import React from 'react';
import UserReview from './UserReview';

const UserReviews = (props) => {

  return (
    <section className="review-section" >
      <div className="review-sec">
        <div className="review-head">
          <h2>Reviews</h2>
          {props.reviews.map(review => {
            return <UserReview
              name={review.name}
              image={review.image}
              rating={review.rating}
              comment={review.comment}
            />
          })}
        </div>
      </div>
    </section>
  );
}

export default UserReviews;