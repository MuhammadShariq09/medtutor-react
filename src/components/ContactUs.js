import React from 'react';
import Navbar from './Navbar';
import axios from 'axios';

class ContactUs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contactForm: {
                email: '',
                subject: '',
                message: ''
            },
            errorMessage: '',
            showSuccessModal: false
        }
    }

    onEnterContactField = (e) => {
        let contactFormFields = { ...this.state.contactForm } ? { ...this.state.contactForm } : {};
        if (e.target.id === 'em') {
            contactFormFields['email'] = e.target.value;
        }
        else if (e.target.id === 'sub') {
            contactFormFields['subject'] = e.target.value;
        }
        else if (e.target.id === 'comment') {
            contactFormFields['message'] = e.target.value;
        }
        this.setState({ errorMessage: "", contactForm: contactFormFields });
    }

    validateEmail = (mail) => {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return (true)
        }
        // alert("You have entered an invalid email address!")
        return (false)
    }

    onSubmit = () => {
        
        if (!this.state.contactForm.email.length || !this.state.contactForm.subject.length || !this.state.contactForm.message.length) {
            this.setState({ errorMessage: 'Any of the above fields cannot be empty.' });
            return;
        }
        if(!this.validateEmail(this.state.contactForm.email))
        {
            this.setState({ errorMessage: 'Invalid email address entered.' });
            return;
        }

        let that = this;
        axios.post('/contact', that.state.contactForm)
            .then(res => {
                that.setState({showSuccessModal: true}, () => {
                    document.getElementById('contactFormClick').click();
                })
                document.getElementById("em").value = "";
                document.getElementById("sub").value = "";
                document.getElementById("comment").value = "";
            })
            .catch(err => {
                that.setState({ errorMessage: 'Error submitting. Please try later..' });
            })
    }

    render() {
        return (
            <div>
               <div className="banner">
                <div className="container">
            <div className="row banner-caption">
                <div className="col-lg-7 col-md-12 col-sm-12">
                  <h1>contact <i>Us</i></h1>
                  <h5>Over 5000+ MCQs, EMQ &amp; VSAQs available</h5> 
                  </div>
              <div className="col-lg-5 col-md-12 col-sm-12">
                <img src={`${process.env.PUBLIC_URL}/images/new-images_07.png`} className="main-top-banner-right-img" alt="" />
              </div> 
            </div>
            </div>
            </div>
                <section className="contact-us-section">
                    <div className="container">
                        <div className="">
                                    <div className="edit-profile-card-body">
                                       
                                        <div className="row">
                                            <div className="col-lg-6">
                                            <div className="edit-pro-information-head">
                                            <h2>get in touch</h2>
                                        </div>
                                                <div className="edit-profile-information">
                                                    <div className="drop-a-message-head">
                                                        <p className="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam. </p>
                                                    </div>
                                                    <form action="#" method="get" acceptCharset="utf-8">
                                                    <div className="row">
                                                            <div className="col-lg-12">
                                                                <div className="form-group">
                                                                    <label>your name </label>
                                                                    <input type="text" id="sub" className="w-100 custom-input-box" placeholder=""
                                                                        onChange={this.onEnterContactField}
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-lg-12">
                                                                <div className="form-group">
                                                                    <label>your  Email  </label>
                                                                    <input type="email" id="em" className="w-100 custom-input-box" placeholder=""
                                                                        onChange={this.onEnterContactField}
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div className="row">
                                                            <div className="col-lg-12">
                                                                <div className="form-group">
                                                                    <label htmlFor="comment">Your Message: </label>
                                                                    <textarea className="form-control message-text-area" rows={5} id="comment"
                                                                        defaultValue={""}
                                                                        onChange={this.onEnterContactField}
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <p>{this.state.errorMessage}</p>
                                                {/* send-button  */}
                                                <div className="send-button">
                                                    <button type="submit" className="button-main" onClick={this.onSubmit}>Send Message<i className="fa fa-chevron-circle-right" /></button>
                                                </div>
                                                {/* send-button- */}
                                            </div>
                                            <div className="col-lg-6">
                                                <div className="contact-information-sec">
                                                    <div className="contact-information-list">
                                                    <img src={`${process.env.PUBLIC_URL}/images/contact.png`} className="img-fluid" alt="" />
                                                        <ul className="list-unstyled">
                                                            <li className="py-4 location-item">
                                                            <a href="https://goo.gl/maps/ecoPhX55FpwKjdo28" target="_blank">
                                                                    <div className="media">
                                                                    <i className="fa fa-map-marker" aria-hidden="true"></i>
                                                                        <div className="media-body">
                                                                        908 Barnes Street,Altamonte Springs, Florida
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li className="pb-3">
                                                            <a href="tel:+123-6654-9874">
                                                                    <div className="media">
                                                                    <i className="fa fa-phone" aria-hidden="true"></i>
                                                                        <div className="media-body">
                                                                        +123-6654-9874
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li className="pb-3">
                                                                <a href="mailto:Contact@mettutor.com">
                                                                    <div className="media">
                                                                    <i className="fa fa-envelope" aria-hidden="true"></i>
                                                                        <div className="media-body">
                                                                            Contact@mettutor.com
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </li> 
                                                        </ul>
                                                          {/* follow-us-sec  */}
                                                <div className="follow-us"> 
                                                        <ul className="list-unstyled">
                                                            <li className="d-inline-block social-icons"><a href="#"><i className="fa fa-facebook" aria-hidden="true" /></a></li>
                                                            <li className="d-inline-block social-icons"><a href="#"><i className="fa fa-twitter" aria-hidden="true" /></a></li>
                                                            <li className="d-inline-block social-icons"><a href="#"><i className="fa fa-instagram" aria-hidden="true" /></a></li>
                                                            <li className="d-inline-block social-icons"><a href="#"><i className="fa fa-linkedin" aria-hidden="true" /></a></li>
                                                            <li className="d-inline-block social-icons"><a href="#"><i className="fa fa-youtube-play" aria-hidden="true" /></a></li>
                                                        </ul>
                                                    </div>
                                                {/* follow--us-sec */}
                                                    </div>
                                                </div>
                                              
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                </section>
                <div className="container"> 
              <div className="subscribe-area about-subs">
                <div className="row">
                  <div className="col-lg-4 col-md-12 col-sm-12">
                    <h4>Subscribe To Get Latest News</h4>
                  </div>
                  <div className="col-lg-8 col-md-12 col-sm-12">
                    <div className="newsletter shadow">
                      <input type="email" placeholder="Enter Your Email Address" onChange={(e) => this.setState({ newsLetterEmail: e.target.value })} />
                      <button onClick={this.onSubscribe}>Notify Me</button>
                      <div className="clearfix" />
                    </div>
                  </div>
                </div>
              </div> 
          </div>
                <a id="contactFormClick" data-toggle="modal" href="#myModal" 
                onClick={() => { this.setState({ showPasswordResetModal: true }) }}
                style={{display: 'none'}}
                ></a>
                <div className="modal" id="myModal">
                    <div className="modal-dialog">
                        {
                            this.state.showSuccessModal ?
                            <div className="modal-content">
                                {/* Modal Header */}
                                <div className="modal-header">
                                    <h4 className="modal-title">Contact Us</h4>
                                    <button type="button" className="close" data-dismiss="modal">×</button>
                                </div>
                                {/* Modal body */}
                                <div className="modal-body">
                                    Thanks for reaching us. Your message has been succesfully sent.
                                            </div>
                                {/* Modal footer */}
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-success" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            :
                            null
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default ContactUs;