import React, { Component } from 'react';
import axios from 'axios';

class SessionPerformance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentQuestions: [],
      performance: {
        total: 1,
        attempted: 0,
        correct: 0,
        incorrect: 0
      },
      visualAnswers: [],
      selectedQues: {}

    }
  }

  computeAnswers = (questions) => {
    // const questions = [...this.state.currentQuestions] ? [...this.state.currentQuestions] : [];
    
    if (questions.length) {
      questions.sort(function (a, b) {
        return a.id - b.id;
      });

      var attempted = 0;
      var correct = 0;
      var incorrect = 0;
      var answers = [];

      questions.forEach((question) => {
        let result = question.pivot;
        if (result.attempted === 1) {
          attempted++;
          if (result.correct === 1) {
            correct++;
          }
          else if (result.correct === 0) {
            incorrect++;
          }
        }
        answers.push({
          attempted: result.attempted,
          correct: result.correct,
          question_id: result.question_id
        })
      });
      var localPerformance = {
        total: questions.length,
        attempted: attempted,
        correct: correct,
        incorrect: incorrect,
      }
      
      this.setState({
        performance: localPerformance,
      },
        this.getAnswersStatus(answers)
      )
    }
  }

  showAttemptedQues = (qID) => {
    let selectedObj = {};
    const { currentQuestions } = this.state;
    const result = currentQuestions.filter(ques => ques.id === qID);
    console.log("currentQuestions: ",currentQuestions , result);
    let selectedOption = result[0].pivot.selected_option;
    let scenario = result[0].options.filter(item => item.id === selectedOption);
    let correct = result[0].options.filter(item => item.correct === 1);
    console.log("scenario: ",scenario);

    
    selectedObj = { ...result[0] };
    selectedObj.scenario = scenario;
    selectedObj.correct = correct;
    

    this.setState({ selectedQues: selectedObj });
    window.$('#exampleModalCenter').modal('show');
  }

  getAnswersStatus = (myAnswers) => {
    var myProcessedAnswers = [];
    if (myAnswers.length) {
      myProcessedAnswers = myAnswers.map((answer, index) => {
        
        if (answer.attempted === 1) {
          if (answer.correct === 1) {
            return (
              <li onClick={() => this.showAttemptedQues(answer.question_id)} key={index} className="result">
                <div className="d-flex justify-content-between">
                  <div className="correct-answers">
                    <span>{index + 1}</span>
                    <span className="pl-2">Correct</span>
                  </div>
                  <div className="correct-ans-icon">
                    <i className="fa fa-check-circle" aria-hidden="true" />
                  </div>
                </div>
              </li>
            );
          } else {
            return (
              <li onClick={() => this.showAttemptedQues(answer.question_id)} key={index} className="result">
                <div className="d-flex justify-content-between">
                  <div className="correct-answers">
                    <span>{index + 1}</span>
                    <span className="pl-2">Wrong</span>
                  </div>
                  <div className="wrong-ans-icon">
                    <i className="fa fa-times-circle" aria-hidden="true" />
                  </div>
                </div>
              </li>
            );
          }
        }
        else if (answer.attempted === 0) {
          return (
            <li key={index} className="result">
              <div className="d-flex justify-content-between">
                <div className="correct-answers">
                  <span>{index + 1}</span>
                  <span className="pl-2" style={{ textAlign: "center" }}> - </span>
                </div>
                {/* <div className="wrong-ans-icon">
                                    <i className="fa fa-times-circle" aria-hidden="true" />
                                </div> */}
              </div>
            </li>
          );
        }
      });
      
      this.setState({ visualAnswers: myProcessedAnswers });
    }
  };

  componentDidMount() {
    console.log("CALLED");
    let that = this;
    axios.get(`/courses/${localStorage.getItem("courseId")}/tests/${this.props.testId}`)
      .then(res => {        
        that.setState({ currentQuestions: res.data.questions },
          that.computeAnswers(res.data.questions)
        )
      });
  }
  
  render() {
    return (
      <div className="session-performance-sec">
        <div className="session-performance-head">
          <h2>Session Performance</h2>
          <h6>Your Score</h6>
        </div>
        <div className="row">
          <div className="col-lg-4 col-md-5">
            <div className="score-img">
              <div className="wrapper">
                <div className={`c100 p${(this.state.performance.correct / this.state.performance.total) * 100} pink`}>
                  <span>{Math.round((this.state.performance.correct / this.state.performance.total) * 100)}%</span>
                  <div className="slice">
                    <div className="bar" />
                    <div className="fill" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-7">
            {/* no-of question-attempted-card */}
            <div className="card">
              <div className="card-body">
                <div className="question-attempted pull-left">
                  <p className="m-0">Question Attempted</p>
                  <h1>{this.state.performance.attempted}</h1>
                </div>
                <div className="icon-first-card pull-right">
                  <img src="/images/noquestion_attempted.png" alt="" className="w-100 align-self-center my-3" />
                </div>
              </div>
            </div>
            {/* no-of question-attempted-card */}
            {/* no-of-correct-question-card */}
            <div className="card">
              <div className="card-body">
                <div className="no-of-correct-question pull-left">
                  <p className="m-0">Correct</p>
                  <h1>{this.state.performance.correct}</h1>
                </div>
                <div className="icon-second-card pull-right">
                  <img src="/images/correct-question-attempted.png" alt="" className="w-100 align-self-center my-3" />
                </div>
              </div>
            </div>
            {/* no-of correct-question-card */}
            {/* no-of-wrong-question-card */}
            <div className="card">
              <div className="card-body">
                <div className="no-of-in-correct-question pull-left">
                  <p className="m-0">Incorrect</p>
                  <h1>{this.state.performance.incorrect}</h1>
                </div>
                <div className="icon-third-card pull-right">
                  <img src="/images/wrongquestion.png" alt="" className="w-100 align-self-center my-3" />
                </div>
              </div>
            </div>
            {/* no-of wrong-question-card */}
          </div>
          <div className="col-lg-4 col-md-4">
            <div className="card correct-answers-card">
              <ul className="list-unstyled">
                <div
                  style={{
                    margin: "5px auto",
                    display: "block",
                    textAlign: "center"
                  }}
                >
                  <h4>Answers</h4>
                </div>
                {this.state.visualAnswers}
              </ul>
            </div>
          </div>
        </div>
        <div className="revise-now-button my-5">
          {localStorage.getItem("memoType") === "memorizer" ? <button onClick={() => this.props.goBackToAll()}>Review Now</button> : null }
        </div>

        {/*modal start here*/}
        <div className="login-fail-main user">
          <div className="featured inner">
            <div className="modal fade bd-example-modal-lg" id="exampleModalCenter" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div className="modal-dialog modal-lgg conf">
                <div className="modal-content">
                  <div className="modal-content">
                    <div className="modal-header">
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <form>
                      <div className="payment-modal-main">
                        <div className="payment-modal-inner">
                          <div className="row">
                            <div className="col-12 text-center">
                              <img src="images/block.png" className="img-fluid" alt="" />
                              <h3>Question#{this.state.selectedQues.id} </h3>
                              <p>{this.state.selectedQues.title}</p>
                              {this.state.selectedQues.type === 'VSAQ' || this.state.selectedQues.type === 'EMQ' ? "" : <p>Your Answer: {this.state.selectedQues.scenario && this.state.selectedQues.scenario[0].scenario} </p>}
                              {this.state.selectedQues.type === 'VSAQ' || this.state.selectedQues.type === 'EMQ' ? 
                                <p>Correct: {this.state.selectedQues.answer}</p> :
                                <p>Correct: {this.state.selectedQues.correct && this.state.selectedQues.correct[0].scenario}</p>
                              }
                              <p>Explanation: { this.state.selectedQues.explanation }</p>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-12 text-center">
                              {/* <button type="button" className="can">yes</button> */}
                              <button type="button" className="con" data-dismiss="modal">Cancel</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/*user profile end here*/}
        </div>
        {/*modal end here*/}

      </div>
    );
  }
}

export default SessionPerformance;