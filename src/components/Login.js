import React, { Component } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { Link, withRouter } from 'react-router-dom';
import { ValidatorComponent } from 'react-form-validator-core';
import ValidatorForm from 'react-form-validator-core/lib/ValidatorForm';


axios.defaults.baseURL = 'http://dev74.onlinetestingserver.com/medtutor/api';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      rememberme: true,
      showPasswordResetModal: false,
      showVerificationModal: false,
      enterNewPasswordModal: false,
      passwordResetSuccessModal: false,
      verificationCode: '',
      checkVerificationCode: '',
      resetPassError: '',
      passwordResetEmail: ''
    }
  }

  onEnterUsername = (e) => {

    this.setState({
      username: e.target.value
    })
  }

  onEnterPassword = (e) => {

    this.setState({
      password: e.target.value
    })
  }

  clickRememberMeCheckbox = (e) => {

    // this.setState({rememberme: e.target.checked})
  }

  onSubmitForm = (e) => {
    let that = this;
    e.preventDefault();
    axios.post('/login', { email: this.state.username, password: this.state.password })
      .then(res => {
        if (res.data.token) {
          const myToken = res.data.token;
          axios.defaults.headers.common['Authorization'] = 'Bearer ' + myToken;
          // setToken(res.data.token);
          axios.get('/users/profile')
            .then(res => {
              // if (that.state.rememberme) {
              localStorage.setItem('token', myToken);
              localStorage.setItem('username', res.data.name);
              localStorage.setItem('userimage', res.data.image);
              localStorage.setItem('userEmail', res.data.email);
              localStorage.setItem('user', JSON.stringify(res.data));
              // }
              this.props.history.push('/');
            })
            .catch(err => {
              console.log("errerr: ", err);
              toast.error("Error fetching user info.");
            })
          // API.headers['Authorization'] = 'Bearer ' + res.data.token;
        }
      })
      .catch(error => {

        Object.keys(error.response.data.errors);
        toast.error(error.response ? error.response.data.message : "Something went wrong.");
        // toast.error("Something went wrong");

      })
  }

  onEnterPasswordResetEmail = (e) => {
    const email = e.target.value;

    this.setState({ username: email });
  }

  submitPasswordResetEmail = () => {

    let that = this;
    if (this.state.username.length) {
      axios.post('/password/create', { email: this.state.username })
        .then(res => {

          if (res.data.message) {
            toast.success(res.data.message);
            // document.getElementById('closeresetModal').click();
            that.setState({ showPasswordResetModal: false, showVerificationModal: true });
            // document.getElementById('myPassRecvryModal').click();
          }
        }).catch(error => {
          error.response.data.errors && Object.values(error.response.data.errors).map((error, index) => {
            toast.error(error[index]);
          });
        });
    }
  }

  onEnterVerificationCode = (e) => {
    this.setState({ verificationCode: e.target.value })
  }

  onSubmitVerificationCode = () => {
    if (this.state.verificationCode.length) {
      axios.get(`/password/toke/validate/${this.state.verificationCode}`)
        .then(res => {
          if (res.data.token) {
            this.setState({
              showPasswordResetModal: false,
              showVerificationModal: false,
              enterNewPasswordModal: true
            })
          }
        })
        .catch(err => {
          this.setState({ checkVerificationCode: 'Invalid verification code entered.' })
        })
    }
  }

  onEnterNewResetPassword = (e) => {
    if (e.target.value.length >= 6) {
      this.setState({ newResetPassword: e.target.value })
    }
  }

  onConfirmNewResetPassword = (e) => {
    if (e.target.value.length >= 6) {
      this.setState({ confirmNewResetPassword: e.target.value })
    }
  }

  onSubmitNewResetPassword = () => {
    const newPass = this.state.newResetPassword;
    const newConfirmPass = this.state.confirmNewResetPassword;

    let that = this;

    if (newPass.length >= 6 && newConfirmPass.length >= 6 && newPass === newConfirmPass) {
      axios.post('/password/reset', {
        email: that.state.username,
        password: newPass,
        password_confirmation: newConfirmPass,
        token: that.state.verificationCode
      })
        .then(res => {

          that.resetModalsState();
          document.getElementById('myPassRecvryModal').click();
          that.setState({ passwordResetSuccessModal: true }, () => {
            document.getElementById('openResetSuccessModal').click();
          })
          // that.props.history.push('/login');
        })
        .catch(err => {

          that.setState({ resetPassError: 'Error resetting password. Please try later.' })
        })
    }
    else {
      let errorMessage = '';
      errorMessage = errorMessage +
        (newPass.length < 6 || newConfirmPass.length < 6) ? 'Passwords should be minimum 6 characters in length. ' : null;
      errorMessage = errorMessage +
        (newPass !== newConfirmPass) ? 'Password do not match. ' : null;
      that.setState({ resetPassError: errorMessage })
    }
  }

  resetModalsState = () => {
    this.setState({
      showPasswordResetModal: false,
      showVerificationModal: false,
      enterNewPasswordModal: false,
      verificationCode: '',
      checkVerificationCode: '',
      resetPassError: '',
      passwordResetEmail: ''
    })
  }
  
  render() {
    const { errorMessages, validators, requiredError, validatorListener, ...rest } = this.props;
    return (
      <div>
        {/* <Navbar history={this.props.history} /> */}
        {/* header-end */}
        {/* login-page */}
        <section className="login-section">
          <div className="container">
            <div className="card border-0 edit-profile-card ">
              <div className="main-sec">
                <div className="card-header edit-profile-header bg-white border-bottom-0">
                </div>
                <div className="card-body pt-0">
                  <div className="row">
                    <div className="col-lg-6">
                      <img src={`${process.env.PUBLIC_URL}/images/logo-image.png`} alt="" className="w-100" />
                      <br />
                      <br />
                      <br />
                      <br />
                      <img src={`${process.env.PUBLIC_URL}/images/new-images_07.png`} alt="" className="w-100" />
                    </div>
                    <div className="col-lg-6">
                      <div className="login-main-sec">
                        <div className="login-head text-center">
                          <h3>Login</h3>
                          <p>Login Account</p>
                        </div>
                        <div className="form-main">
                          <div className="row">
                            <div className="col-md-12">
                              <div className="img-div">
                                <div className="img-div-inner"> </div>
                              </div>
                            </div>
                          </div>
                          {/*fields start here*/}
                          <form onSubmit={this.onSubmitForm}>
                            <div className="fields">
                              <div className="row">
                                <div className="col-md-12 email-input-sec mb-3">
                                  <i className="fa fa-user" />
                                  <input type="email" value={this.state.username} className="form-control" spellCheck="true" placeholder="User Name" onChange={this.onEnterUsername} required />
                                </div>
                                <div className="col-md-12 email-input-sec mb-3">
                                  <i className="fa fa-key" />
                                  <input type="password" spellCheck="true" className="form-control" placeholder="Password" onChange={this.onEnterPassword} required />
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-md-6 col-6">
                                  <label className="check-box-container">Remember me
                                                                          <input type="checkbox" defaultChecked onChange={this.clickRememberMeCheckbox} />
                                    <span className="checkbox-checkmark" />
                                  </label>
                                </div>
                                <div className="col-md-6 col-6 text-right">
                                  {/* <a data-toggle="modal" href="#myPassRecvryModal">Forgot Password?</a> */}
                                  <a data-toggle="modal" href="#myPassRecvryModal" onClick={() => { this.setState({ showPasswordResetModal: true }) }}>Forgot Password?</a>
                                </div>
                              </div>
                              <button type="submit" className="form-control" >Login</button>
                              <div className="new-user text-center">
                                <p>-Not a member? Register Now-</p>
                                <Link to="/register" className="login form-control">Register</Link>
                              </div>
                            </div>
                          </form>
                          {/* form end here */}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div></section>
        {/* Pass--revovery-modal */}
        {/* {this.state.showPasswordResetModal ?  */}
        <button id="closePwModal" type="button" className="close" data-dismiss="myPassRecvryModal" style={{ display: 'none' }}>asd</button>
        <div className="modal" id="myPassRecvryModal">
          <div className="modal-dialog">
            {
              this.state.showPasswordResetModal
                ?
                <div className="modal-content">
                  <div className="modal-header border-bottom-0">
                    <button id="closeresetModal" type="button" className="close" data-dismiss="modal" onClick={() => {
                      this.resetModalsState();
                    }}>×</button>
                  </div>
                  {/* Modal body */}
                  <div className="modal-body">
                    <div className="pass-recovery-modal">
                      <div className="register-sec-main-head text-center">
                        <h2>Password Recovery</h2>
                      </div>
                      <div className="row mt-5">
                        <div className="form-group  col-12 email-input-sec">
                          <i className="fa fa-envelope" />
                          <input type="text" contentEditable="true" className="form-control" spellCheck="true" placeholder="Enter Email Address" onChange={this.onEnterPasswordResetEmail} />
                        </div>
                      </div>
                      <div className="continue text-center">
                        <button onClick={this.submitPasswordResetEmail}>Continue</button>
                      </div>
                      {/* <div className="d-flex justify-content-center my-5 back-to-login-sec">
                                        <div className="pl-2"><i className="fa fa-angle-left" /><a href="#" className="text-dark"> back to login</a></div>
                                    </div> */}
                    </div>
                  </div>
                </div>
                :
                this.state.showVerificationModal
                  ?
                  <div className="modal-content">
                    <div className="modal-header border-bottom-0">
                      <button type="button" className="close" data-dismiss="modal" onClick={this.resetModalsState}>×</button>
                    </div>
                    {/* Modal body */}
                    <div className="modal-body">
                      <div className="pass-recovery-modal">
                        <div className="register-sec-main-head text-center">
                          <h2>Password Recovery</h2>
                        </div>
                        <div className="row mt-5">
                          <div className="form-group  col-12 email-input-sec">
                            <i className="fa fa-pencil" aria-hidden="true" />
                            <input contentEditable="true" value={this.state.verificationCode} className="form-control" spellCheck="true" defaultValue="Enter verification Code" onChange={this.onEnterVerificationCode} />
                          </div>
                        </div>
                        <p>{this.state.checkVerificationCode}</p>
                        <div className="continue text-center">
                          <button onClick={this.onSubmitVerificationCode}>Continue</button>
                        </div>
                        {/* <div className="d-flex justify-content-center my-5 back-to-login-sec">
                                                            <div className="pl-2"><i className="fa fa-angle-left" /><a href="#" className="text-dark"> back to login</a></div>
                                                        </div> */}
                      </div>
                    </div>
                  </div>
                  :
                  null
            }
            {
              this.state.enterNewPasswordModal ?
                <div className="modal-content">
                  <div className="modal-header border-bottom-0">
                    <button type="button" className="close" data-dismiss="modal" onClick={this.resetModalsState}>×</button>
                  </div>
                  {/* Modal body */}
                  <div className="modal-body">
                    <div className="pass-recovery-modal">
                      <div className="register-sec-main-head text-center">
                        <h2>Password Recovery</h2>
                      </div>
                      <div className="row mt-5">
                        <div className="form-group  col-12 email-input-sec">
                          <i className="fa fa-lock" aria-hidden="true" />
                          <input type="password" contentEditable="true" className="form-control" spellCheck="true" placeholder="Enter Password" onChange={this.onEnterNewResetPassword} />
                        </div>
                      </div>
                      <div className="row">
                        <div className="form-group  col-12 email-input-sec">
                          <i className="fa fa-lock" aria-hidden="true" />
                          <input type="password" contentEditable="true" className="form-control" spellCheck="true" placeholder="Re Type Password" onChange={this.onConfirmNewResetPassword} />
                        </div>
                      </div>
                      <p>{this.state.resetPassError}</p>
                      <div className="continue text-center">
                        <button onClick={this.onSubmitNewResetPassword}>Continue</button>
                      </div>
                      {/* <div className="d-flex justify-content-center my-5 back-to-login-sec">
                                        <div className="pl-2"><i className="fa fa-angle-left" /><a href="#" className="text-dark"> back to login</a></div>
                                    </div> */}
                    </div>
                  </div>
                </div>
                :
                null
            }
          </div>
        </div>
        <a id="openResetSuccessModal" data-toggle="modal" href="#resetSuccessModal"
          onClick={() => { this.setState({ passwordResetSuccessModal: true }) }}
          style={{ display: 'none' }}
        >Forgot Password?</a>

        <div className="modal" id="resetSuccessModal">
          <div className="modal-dialog">
            {
              this.state.passwordResetSuccessModal ?
                <div className="modal-content">
                  {/* Modal Header */}
                  <div className="modal-header">
                    <h4 className="modal-title">Password Reset</h4>
                    <button type="button" className="close" data-dismiss="modal">×</button>
                  </div>
                  {/* Modal body */}
                  <div className="modal-body">
                    Your password has been successfully reset. Please log-in with your new credentials.
                                            </div>
                  {/* Modal footer */}
                  <div className="modal-footer">
                    <button type="button" className="btn btn-success" data-dismiss="modal">Close</button>
                  </div>
                </div>
                :
                null
            }
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);