import React, { Component } from 'react';
import axios from 'axios';
import TestHandler from './TestHandler';
import SessionPerformance from './SessionPerformance';
import { withRouter } from 'react-router-dom'
import { toast } from 'react-toastify';

class Test extends Component {
  constructor(props) {
    super(props);
    this.state = {
      previousTests: [],
      testId: null,
      currentStep: 1,
      testMode: "system",
      completed: false,
      selectedQuestions: ["all"],
      questionsMode: 'unanswered',
      numOfQuestions: 10,
      displayTopics: [],
      totalQuestions: 0,
      selectedTopics: ['all'],
      timeComplexity: 3,
      testQuestions: [],
      expiryTime: null,
      deleteTestId: null,
      errorMessage: ''
    }
  }

  onSelectTestMode = (e) => {
    const { id } = e.target;
    
    if (id === 'systemTest') {
      this.setState({ testMode: "system", selectedQuestions: ["all"], errorMessage: '' })
      
      return;
    }
    this.setState({ testMode: "classic", selectedQuestions: ["all"], errorMessage: '' })



    
  }

  onMarkQuestionType = (e) => {
    const { id, checked } = e.target;
    let selections = this.state.selectedQuestions.length ? [...this.state.selectedQuestions] : [];
    console.log(selections)

    var pushId = '';
    if (id === 'mcqTest') {
      pushId = 'mcq';
    }
    else if (id === 'vsaqTest') {
      pushId = 'vsaq';
    }
    else if (id === 'emqTest') {
      pushId = 'emq';
    }
    else if (id === 'allTest') {
      pushId = 'all';
    }
    const index = selections.indexOf(pushId);
    if (checked) {
      if (id !== 'allTest') {
        const allId = selections.indexOf('all');
        if (allId > -1) {
          selections.splice(allId, 1);
        }
      }
      if (index < 0) {
        if (pushId === 'all') {
          selections = [];
        }
        selections.push(pushId);
      }
      if (selections.includes('vsaq') && selections.includes('emq') && selections.includes('mcq')) {
        selections = [];
        selections.push('all');
      }
    }
    else {
      if (index > -1) {
        selections.splice(index, 1);
      }
    }
    // this.updateCheckBoxStatus(id, selections);
    this.setState({ selectedQuestions: selections, errorMessage: '' });
  }

  onSelectQuestionsMode = (e) => {
    this.setState({ questionsMode: e.target.value })
  }

  onSelectTotalQuestions = (e) => {
    const amount = parseInt(e.target.value);
    this.setState({ numOfQuestions: amount });
  }

  handleMarkTopic = (e) => {
    const { id, checked } = e.target;
    var selectedTopics = [...this.state.selectedTopics] ? [...this.state.selectedTopics] : [];
    const index = selectedTopics.indexOf(id);
    if (checked) {
      if (id !== 'all') {
        if ((selectedTopics.indexOf('all')) > -1) {
          selectedTopics.splice((selectedTopics.indexOf('all')), 1);
        }
        selectedTopics.push(id);
      }
      else if (id === 'all') {
        selectedTopics = [];
        selectedTopics.push(id);
      }
      else if (index < 0) {
        selectedTopics.push(id);
      }
    }
    else {
      if (index > -1) {
        selectedTopics.splice(index, 1);
      }
    }
    this.updateCheckBoxStatus(id, selectedTopics);
    this.setState({ selectedTopics: selectedTopics });
  }

  getTopics = async (courseId) => {
    let that = this;
    if (courseId) {
      return await axios.get(`/topics?course=${courseId}`).then(res => {
        var displayTopics = [];
        var tempDisplayArray = [];
        tempDisplayArray.push(
          <li key="all">
            <label className="check-box-container">All
              <input type="checkbox" id="all" onChange={this.handleMarkTopic} />
              <span className="checkbox-checkmark" />
            </label>
          </li>
        );

        res.data.map((topic, index) => {
          tempDisplayArray.push(
            <li key={topic.id}>
              <label className="check-box-container">
                {topic.title}
                <input type="checkbox" id={topic.id} onChange={this.handleMarkTopic} />
                <span className="checkbox-checkmark" />
              </label>
            </li>
          )
          if (tempDisplayArray.length == 4 || (index + 1 == res.data.length)) {
            let myObject = <div key={displayTopics.length} className="col-lg-6 col-md-6">
              <div className="questions-checkbox">
                <ul className="list-unstyled">
                  {tempDisplayArray}
                </ul>
              </div>
            </div>;
            displayTopics.push(myObject);
            tempDisplayArray = [];
          }
        })
        that.setState({ displayTopics: displayTopics, totalQuestions: res.data.length })
        return (displayTopics);
      })
    }
    return [];
  }

  filterQuestions = () => {
    var mode = this.state.selectedQuestions.includes('all') ? "all" : this.state.selectedQuestions;
    var topics = this.state.selectedTopics.includes('all') ? "all" : this.state.selectedTopics;
    const totalTime = this.state.numOfQuestions * this.state.timeComplexity;
    const payload = {
      type: this.state.testMode.toLowerCase(),
      mode: mode,
      topics: topics,
      number_of_questions: this.state.numOfQuestions,
      selections: this.state.questionsMode,
      test_type: 'test',
      duration: totalTime,
      repetition: false
    }

    
    let memoType = localStorage.getItem('memoType');
    let memoId = localStorage.getItem('memoId');
    

    if (memoType === 'system') {
      payload.course_id = localStorage.getItem("courseId");
      axios.post(`/system-memorizers`, payload)
        .then(res => {
          // this.setState({ testQuestions: res.data });
          const myQuestions = res.data.questions;
          if (myQuestions.length < 1) {
            toast.warn("Questions not found in the selected topic");
            return;
          }
          
          if (myQuestions.length) {
            myQuestions.sort(function (a, b) {
              return a.id - b.id;
            });
          }
          
          localStorage.setItem('activeTestId', res.data.id);
          if (localStorage.getItem('myTestAnswers')) {
            localStorage.removeItem('myTestAnswers');
          }
          this.setState({ testQuestions: myQuestions, testId: res.data.id, expiryTime: res.data.expired_at, currentStep: 3 });
          // this.setState({
          //   testQuestions: myQuestions,
          //   revisionId: res.data.id,
          //   completed: false,
          //   isNewTest: true,
          //   currentStep: 3
          // });
          localStorage.setItem('memoType', '');
        });
      return;
    }
    else if (memoType === 'both') {
      payload.course_id = localStorage.getItem("courseId");
      axios.post(`/my-memorizers`, payload)
        .then(res => {
          // this.setState({ testQuestions: res.data });
          const myQuestions = res.data.questions;
          
          if (myQuestions.length) {
            myQuestions.sort(function (a, b) {
              return a.id - b.id;
            });
          }
          
          localStorage.setItem('activeTestId', res.data.id);
          if (localStorage.getItem('myTestAnswers')) {
            localStorage.removeItem('myTestAnswers');
          }
          this.setState({ testQuestions: myQuestions, testId: res.data.id, expiryTime: res.data.expired_at, currentStep: 3 });
          // this.setState({
          //   testQuestions: myQuestions,
          //   revisionId: res.data.id,
          //   completed: false,
          //   isNewTest: true,
          //   currentStep: 3
          // });
          localStorage.setItem('memoType', '');
        });
      return;
    }
    else if (memoType === 'myMemo') {
      
      payload.course_id = localStorage.getItem("courseId");
      payload.memorizer_id = memoId;
      axios.post(`/memorizer/test`, payload)
        .then(res => {
          
          // this.setState({ testQuestions: res.data });
          const myQuestions = res.data.questions;
          
          if (myQuestions.length) {
            myQuestions.sort(function (a, b) {
              return a.id - b.id;
            });
          }
          
          localStorage.setItem('activeTestId', res.data.id);
          if (localStorage.getItem('myTestAnswers')) {
            localStorage.removeItem('myTestAnswers');
          }
          this.setState({ testQuestions: myQuestions, testId: res.data.id, expiryTime: res.data.expired_at, currentStep: 3 });
          // this.setState({
          //   testQuestions: myQuestions,
          //   revisionId: res.data.id,
          //   completed: false,
          //   isNewTest: true,
          //   currentStep: 3
          // });
          localStorage.setItem('memoType', '');
        });
      return;
    }
    axios.post(`${this.props.location.pathname}/create`, payload).then(res => {
        const myQuestions = res.data.questions;
        if(myQuestions.length < 1){
          toast.warn("Questions not found in this topic.");
          return;

        }
        
        if (myQuestions.length) {
          myQuestions.sort(function (a, b) {
            return a.id - b.id;
          });
        }
        
        localStorage.setItem('activeTestId', res.data.id);
        if (localStorage.getItem('myTestAnswers')) {
          localStorage.removeItem('myTestAnswers');
        }
        this.setState({ testQuestions: myQuestions, testId: res.data.id, expiryTime: res.data.expired_at, currentStep: 3 }, () => {
          this.props.preventNavClicks()
        });
      })

  }

  updateCheckBoxStatus = (id, object) => {
    if (id !== 'all') {
      if (!object.includes('all')) {
        document.getElementById('all').checked = false;
      }
    }
    document.getElementById(id).checked = object.includes(id);
  }

  onClickProceed = (e) => {
    const { id } = e.target;
    if (id.includes('step1Proceed')) {
      if (!this.state.testMode.length) {
        this.setState({ errorMessage: "Please select one of the above test modes." });
        return;
      }
      else if (this.state.testMode === 'classic' && !this.state.selectedQuestions.length) {
        this.setState({ errorMessage: "Please select one or more question types." });
        return;
      }
      this.setState({ currentStep: 2 })
    }
    else if (id.includes('step2Proceed')) {
      if (!this.state.testMode.length) {
        this.setState({ errorMessage: "Please select one of the above test modes." });
        return;
      }
      else if (this.state.testMode === 'classic' && !this.state.selectedQuestions.length) {
        this.setState({ errorMessage: "Please select one or more question types." });
        return;
      }
      this.filterQuestions()
    }
  }

  getPreviousTests = (courseId) => {
    let that = this;
    const allMonths = ['January', 'February', 'March',
      'April', 'May', 'June', 'July', 'August',
      'September', 'October', 'November', 'December'];
    axios.get(`/courses/${courseId}/tests`)
      .then(res => {
        let allPreviousRevisions = res.data;
        const processedRevisions = allPreviousRevisions.map((eachRevision) => {
          const totalQuestions = eachRevision.questions.length;
          const totalCorrect = eachRevision.questions.filter(eachQuestion => eachQuestion.pivot.correct === 1).length;
          const totalWrong = eachRevision.questions.filter(eachQuestion => (eachQuestion.pivot.attempted === 1 && eachQuestion.pivot.correct === 0)).length;
          const totalUnAnswered = eachRevision.questions.filter(eachQuestion => eachQuestion.pivot.attempted === 0).length;
          const score = totalCorrect / totalQuestions;
          const progress = (totalCorrect + totalWrong) / totalQuestions;
          var date = eachRevision.updated_at.split(' ')[0].split('-');
          date[1] = allMonths[parseInt(date[1]) - 1];
          date = date.reverse().join(' ');
          const status = {
            id: eachRevision.id,
            date: date,
            completed: 1,
            mode: eachRevision.mode,
            totalQuestions,
            totalCorrect,
            totalWrong,
            totalUnAnswered,
            score: score,
            progress
          }
          return status;
        })
        that.setState({ previousTests: processedRevisions })
      })
  }

  getTotalQuestions = () => {
    axios.get('/questions/count')
      .then(res => {
        this.setState({ totalQuestions: res.data })
        
      })
  }

  showPreviousTests = () => {
    {
      var displayRevs = [];
      var thisDisplayRev = [];
      console.log("previousTests: ",this.state.previousTests);
      this.state.previousTests.map((eachRevision, index) => {
        thisDisplayRev.push(
          <div key={index} className="col-lg-6 col-md-6 col-sm-6 col-12 ">
            <div className="card">
              <div className="card-date text-right">
                <p>{eachRevision.date}</p>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-4 col-lg-4 col-md-4 col-6 border-right">
                    <ul className="list-unstyled">
                      <li>
                        <div className="mode">
                          <label>Mode</label>
                          <p className="m-0">{eachRevision.mode}</p>
                        </div>
                      </li>
                      <li>
                        <div className="unanswered">
                          <label >Unanswered</label>
                          <p className="m-0">{eachRevision.totalUnAnswered}</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div className="col-lg-4 col-lg-4 col-md-4 col-6 border-right">
                    <ul className="list-unstyled">
                      <li>
                        <div className="no-of-question">
                          <label >No of Questions</label>
                          <p className="m-0">{eachRevision.totalQuestions}</p>
                        </div>
                      </li>
                      <li>
                        <div className="incorrect">
                          <label >Incorrect</label>
                          <p className="m-0">{eachRevision.totalWrong}</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div className="col-lg-4 col-lg-4 col-md-4 col-6">
                    <ul className="list-unstyled">
                      <li>
                        <div className="correct">
                          <label >Correct</label>
                          <p className="m-0">{eachRevision.totalCorrect}</p>
                        </div>
                      </li>
                      <li>
                        <div className="score">
                          <label >Score</label>
                          <p className="m-0">{Math.round(eachRevision.score * 100)}%</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="progress my-3">
                  <div className="progress-bar custom-progress-bar"
                    role="progressbar"
                    aria-valuenow={Math.round(eachRevision.progress * 100)}
                    aria-valuemin={0}
                    aria-valuemax={100}
                    style={{ width: `${Math.round(eachRevision.progress * 100)}%` }}>{Math.round(eachRevision.progress * 100)}%</div>
                  <span className="pull-right">
                    {/*  <p class="text-right total-percent">100%</p> */}
                  </span>
                </div>
                <div className="row my-3">
                  <div className="col-lg-6 col-md-6 col-6 text-left">
                    <div className="continue-button">
                      <button id={eachRevision.id} onClick={this.onClickContinueButton}>
                        Review Now <i id={eachRevision.id} className="fa fa-chevron-circle-right" aria-hidden="true" />
                      </button>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-6 trash-icon text-right">
                    <a id="deleteTestIcon"
                      data-toggle="modal"
                      href="#myDeleteTestModal"
                      onClick={() => { this.setState({ deleteTestId: eachRevision.id }) }}
                    ><i className="fa fa-trash-o" aria-hidden="true" /></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
        if (index % 2 === 1 || (index % 2 === 0 && index === this.state.previousTests.length - 1)) {
          displayRevs.push(
            <div key={index} className="row" style={{ marginTop: "8px" }}>
              {thisDisplayRev}
            </div>
          )
          thisDisplayRev = [];
        }
      })
      return displayRevs;
    }
  }

  onClickContinueButton = (e) => {
    let that = this;
    const revisionId = e.target.id;
    
    axios.get(`/courses/${localStorage.getItem("courseId")}/tests/${revisionId}`)
      .then(res => {
        
        if (res.data.is_test === 0) {
          that.props.history.push(`/courses/${localStorage.getItem("courseId")}/tests`);
          that.goBackToAllTests();
          return;
        }

        const myQuestions = res.data.questions;
        if (myQuestions !== undefined && myQuestions.length) {
          myQuestions.sort(function (a, b) {
            return a.id - b.id;
          });
        }
        localStorage.setItem('activeTestId', res.data.id);
        if (localStorage.getItem('myTestAnswers')) {
          localStorage.removeItem('myTestAnswers');
        }
        that.setState({
          testQuestions: myQuestions,
          testId: res.data.id,
          completed: true,
          isNewTest: false,
          currentStep: 3
        }, () => { that.props.preventNavClicks() });
      })
      .catch(err => {
        that.props.history.push(`/courses/${localStorage.getItem("courseId")}/tests`);
        that.goBackToAllTests();
      })
  }

  onClickDeleteTest = () => {
    document.getElementById('deleteTestIcon').click();
    const deleteId = this.state.deleteTestId;
    
    if (deleteId) {
      axios.delete(`courses/${localStorage.getItem("courseId")}/tests/${deleteId}`)
        .then(res => {
          
          if (res.data) {
            this.getPreviousTests(localStorage.getItem("courseId"));
          }
        })
    }
  }

  onClickCancelDeleteTest = () => {
    document.getElementById('deleteTestIcon').click();
    this.setState({ deleteTestId: null })
  }

  testCompleted = () => {
    this.setState({ currentStep: 4 });
    this.props.enableNavClicks();
  }

  goBackToAllTests = () => {
    this.setState({
      previousTests: [],
      testId: null,
      currentStep: 1,
      testMode: "system",
      completed: false,
      selectedQuestions: ["all"],
      questionsMode: 'unanswered',
      numOfQuestions: 10,
      displayTopics: [],
      totalQuestions: 0,
      selectedTopics: ['all'],
      timeComplexity: 3,
      testQuestions: [],
      expiryTime: null,
      deleteTestId: null,
      errorMessage: ''
    }, () => {
      this.getTopics();
      this.getPreviousTests();
    })
  }

  checkURLParams = () => {
    const { courseId, activeTab, testId: reviseId } = this.props.match.params;

    if (activeTab && activeTab == 'tests') {
      if (reviseId && reviseId > -1) {
        this.onClickContinueButton({ target: { id: reviseId } });
        return;
      }
      else {
        this.getTopics(courseId);
        this.getPreviousTests(courseId);
        this.getTotalQuestions();
      }
    }
  }

  componentDidMount() {
    // const [, , courseId, activeTab, activeTabId] = window.location.pathname.split('/');
    // if (activeTabId && activeTabId > -1) {
    //   this.onClickContinueButton({ target: { id: activeTabId } });
    //   return;
    // }
    // if (activeTab == 'tests') {
    //   this.getTopics();
    //   this.getPreviousTests();
    //   this.getTotalQuestions();
    // }
    

    this.checkURLParams();
  }

  onSelectTimeComplexity = (e) => {
    this.setState({ timeComplexity: parseInt(e.target.value) })
  }

  render() {
    if (this.state.currentStep === 1) {
      return (
        <section id="stest1" className="revise-now-sec">
     
          <button type="button"  onClick={()=>
           this.setState({ currentStep: 2 })
          }>Start Test</button>

          {
            this.state.previousTests.length
              ?
              <div className="previous-revision my-4">
                <div className="previous-revision-head my-4">
                  <h3>Previous Tests</h3>
                </div>
                {this.showPreviousTests()}
              </div>
              :
              null
          }


          <div className="modal" id="myDeleteTestModal">
            <div className="modal-dialog rewise-modal-dialog">
              <div className="modal-content delete-memorizer-modal-content">
                {/* Modal body */}
                <div className="modal-body rewise-modal-delete" style={{ width: '500px' }}>
                  <div className="alert-image text-center">
                    <img src={`${process.env.PUBLIC_URL}/images/alert.png`} alt="" />
                  </div>
                  <div className="alert-head text-center">
                    <h2 className="f-w-700">Alert</h2>
                    <p> Are you sure you want to delete this
                    test?</p>
                  </div>
                  <div className="alert-buttons text-center">
                    <button className="no-button"
                      onClick={this.onClickCancelDeleteTest}
                    >No</button>
                    <button className="yes-button" onClick={this.onClickDeleteTest}>Yes</button>
                  </div>
                </div>
                {/* Modal footer */}
              </div>
            </div>
          </div>

        </section>
      );
    }
    else if (this.state.currentStep === 2) {
      return (

        
        <div id="st2">
          <div className="revise-go-back-sec">
            <a href="#" onClick={this.goBackToAllTests}>
              <span style={{ marginRight: "5px" }}>
                <i className="fa fa-angle-left" aria-hidden="true">
                </i>
              </span>
              Go back
                        </a>
          </div>
          <br />


          <div className="revise-now-head">
            <h3>Test Mode</h3>
          </div>
          <div className="revise-now-head">
            <h3>Choose how you would like to be tested</h3>
          </div>
          <div className="revise-now-desc">
            <p>System mode organises the questions according to bodily systems. Classic mode organises the questions.</p>
          </div>
          <div className="row">
            <div className="col-lg-6 col-md-6 col-sm-6 col-12">
              <div className="revise-sec-radio-check-box-sec">
                <ul className="list-unstyled">
                  <li className="d-inline-block" style={{ padding: "5px 20px" }}>
                    <label className="radio-container" >
                      System Mode
                          <input type="checkbox" id='systemTest' checked={this.state.testMode === "system"} name="radio" onChange={this.onSelectTestMode}  />
                      <span className="radio-checkmark" />
                    </label>
                  </li>
                  <li className="d-inline-block" style={{ padding: "5px 20px" }}>
                    <label className="radio-container">
                      Classic Mode
                         <input type="checkbox" id='classicTest' checked={this.state.testMode === "classic"} name="radio" onChange={this.onSelectTestMode} />
                      <span className="radio-checkmark" />
                    </label>
                  </li>
                </ul>
                <div className="row pl-3 pr-3">
                  <div className="col-12">
                    <p>Select your question type(s)</p>
                    <p> MCQ’s: Multiple choice questions, EMQs: Extended Matching Questions, VSAQ’s – Very Short Answer Questions</p>
                    <ul className="list-unstyled">
                      <li className="d-inline-block pull-left w-50">
                        <label className="check-box-container">All
                            <input type="checkbox" id="allTest"
                            onChange={this.onMarkQuestionType}
                            checked={this.state.selectedQuestions.includes('all')}
                          />
                          <span className="checkbox-checkmark" />
                        </label>
                      </li>
                      <li className="d-inline-block pull-right w-50">
                        <label className="check-box-container">EMQS
                          <input type="checkbox" id="emqTest"
                            onChange={this.onMarkQuestionType}
                            checked={this.state.selectedQuestions.includes('emq') && !this.state.selectedQuestions.includes('all')}
                          />
                          <span className="checkbox-checkmark" />
                        </label>
                      </li>
                      <li className="d-inline-block pull-left w-50">
                        <label className="check-box-container">MCQS
                          <input type="checkbox" id="mcqTest"
                            onChange={this.onMarkQuestionType}
                            checked={this.state.selectedQuestions.includes('mcq') && !this.state.selectedQuestions.includes('all')}
                          />
                          <span className="checkbox-checkmark" />
                        </label>
                      </li>
                      <li className="d-inline-block pull-right w-50">
                        <label className="check-box-container">VSAQS
                            <input type="checkbox" id="vsaqTest"
                            onChange={this.onMarkQuestionType}
                            checked={this.state.selectedQuestions.includes('vsaq') && !this.state.selectedQuestions.includes('all')}
                          />
                          <span className="checkbox-checkmark" />
                        </label>
                      </li>
                    </ul>
                  </div>
                </div>
                <p style={{ marginLeft: "20px" }}>{this.state.errorMessage}</p>
                <div className="col-lg-6 col-md-12 col-sm-6 col-12" style={{ margin: "5px 0px" }}>
                  <div className="proceed-button mt-2 mb-3">
                    {/* <button id="step1Proceed" onClick={this.onClickProceed}>Proceed
                      <i id="step1Proceedi" className="fa fa-chevron-circle-right" aria-hidden="true" style={{ marginLeft: '5px' }} />
                    </button> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        
         
          <div className="revise-now-desc">
            {/* <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> */}
          </div>
          <div className="row">
            <div className="col-lg-6 col-md-12">
              <div className="row">
                <div className="col-lg-5 col-md-6">
                  <div className="card h-100">
                    <div className="card-body">
                      <ul className="list-unstyled text-center">
                        <li>
                          <img src={`${process.env.PUBLIC_URL}/images/type.png`} />
                        </li>
                        <li>Type</li>
                        <li>
                          <h4>{this.state.testMode === 'system' ? 'System Mode' : 'Classic Mode'}</h4>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="col-lg-5 col-md-6">
                  <div className="card">
                    <div className="card-body">
                      <ul className="list-unstyled text-center">
                        <li>
                          <img src={`${process.env.PUBLIC_URL}/images/que-type.png`} />
                        </li>
                        <li>Questions Type</li>
                        <li>
                          <h4>{this.state.testMode === 'system' ?
                            'All'
                            :
                            this.state.selectedQuestions.map(question => { return (`${question.toUpperCase()}\n`) })
                          }</h4>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              {/* empty-sec */}
            </div>
          </div>
          <br />
          {/* {
            this.state.testMode === 'classic' ? */}
              <div className="questions-topic-sec my-5">
                <div className="questions-info-first-sec my-3">
                  {/* <h6>Total Questions Found {this.state.totalQuestions}</h6> */}
                </div>
                <div className="row">
                  <div className="col-lg-6">
                    <div className="card question-type-card">
                      <div className="card-header border-bottom-0">
                        <h6>Select the topic(s) you wish to be tested on</h6>
                      </div>
                      <div className="card-body">
                        <div className="questions-type">
                          <h6>Question Type</h6>
                        </div>
                        <div className="row">
                          {this.state.displayTopics || "Topics not found."}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* :
              null
          } */}
          
          <br />
          <div className="question-selection-sec">
            <div className="row">
              <div className="col-lg-6">
                <div className="question-selection-main-sec">
                  <ul className="list-unstyled">
                    <li className="mb-3">
                      <h6>Question Selection</h6>
                    </li>
                    <li>
                      <div className="form-group">
                        <select className="form-control" id="sel1" onChange={this.onSelectQuestionsMode}>
                          <option value="unanswered">New Questions Only</option>
                          <option value="answered">Repeated Questions Only</option>
                          <option value="all">New and Repeated Questions</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-6">
                <div className="no-of-question-sec">
                  <ul className="list-unstyled">
                    <li>
                      <h6>Number of Questions</h6>
                    </li>
                    <li>
                      <div className="slidecontainer">
                        <input data-toggle="tooltip" title={this.state.numOfQuestions} data-placement="bottom" type="range"
                          min="1" max={`${this.state.totalQuestions}`} value={this.state.numOfQuestions} className="slider" id="myRange"
                          onChange={this.onSelectTotalQuestions}
                        />
                        <p>Total questions needed : {this.state.numOfQuestions}</p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="time-complexity-level">
            <div className="row">
              <div className="col-lg-6">
                <div className="time-complexity-head">
                  <h6>Time Complexity</h6>
                </div>
                <div className="form-group">
                  <select className="form-control" id="sel1" onChange={this.onSelectTimeComplexity}>
                    <option value={3}>Beginner</option>
                    <option value={2}>Intermediate</option>
                    <option value={1}>Expert</option>
                  </select>
                </div>
                <div className="note-sec">
                  <ul className="list-unstyled">
                    <li>In Beginner level user will get 3 minutes per question.</li>
                    <li>In Intermediate level user will get 2 minutes per question. </li>
                    <li>In Expert level user will get 1 minute per question.</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-md-12 col-sm-6 col-12">
            <div className="proceed-button mt-2 mb-3" style={{ textAlign: "left" }}>
              <button id="step2Proceed" onClick={this.onClickProceed}>Begin your test!
                                <i id="step2Proceedi" className="fa fa-chevron-circle-right" aria-hidden="true" style={{ marginLeft: '5px' }} />
              </button>
            </div>
          </div>
        </div>
      );
    }
    else if (this.state.currentStep === 3) {
      return (
        <TestHandler
          testQuestions={this.state.testQuestions}
          courseId={localStorage.getItem("courseId")}
          testId={this.state.testId}
          showReference={false}
          completed={this.state.completed}
          isTestComplete={this.state.completed}
          expiryTime={Date.now() + (((this.state.timeComplexity * 60) * this.state.numOfQuestions) * 1000)}
          testCompleted={this.testCompleted}
          memorizers={this.props.memorizers}
          enableNavClicks={this.props.enableNavClicks}
        />
      );
    }
    else if (this.state.currentStep === 4) {
      return (
        <SessionPerformance
          testId={this.state.testId}
          courseId={localStorage.getItem("courseId")}
          goBackToAll={this.goBackToAllTests}
        />
      );
    }
  }
}

export default withRouter(Test);