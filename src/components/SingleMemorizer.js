import React, { Component } from 'react'
import axios from 'axios';

class SingleMemorizer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      deleteMMC: null,
      memo: props.memo,
      newQuestion: { title: null, options: [] },
      colors: [false, false, false, false]
    };
  }

  getMemoryCards = () => {
    const myMemoryCards = this.state.memo.questions;
    return myMemoryCards.map(((eachQuestion, index) => (
      <div className="card memorizer-first-card memory-card-1">
        <div className="card-header bg-memorizer-card-color bg-memorizer-card-color memorizer-card-header">
          <div className="row">
            <div className="col-lg-10 col-md-9 col-7">
              <div className="memory-card-head">
                <h6 className="memorizer-head-color">Memory Card {index + 1}</h6>
              </div>
            </div>
            <div className="col-lg-2 col-md-3 col-5 text-right ">
              <div className="d-flex justify-content-between">
                <div className="type-sec">
                  <span>Type:  </span><span className="mcq-type">{eachQuestion.type === 'MEMORIZER' ? 'MCQ' : eachQuestion.type}</span>
                </div>
                <div className="trash-icon-custom-style">
                  <a data-toggle="modal" href="#myDeleteMemoryCardModal" >
                    <i className="fa fa-trash" aria-hidden="true" id={eachQuestion.id} onClick={this.onClickSetMmcToDelete} />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        {
          eachQuestion.type === 'MCQ' || eachQuestion.type === 'MEMORIZER'
            ?
            <div className="card-body">
              <div className="memory-desription">
                <p>{eachQuestion.title}</p>
              </div>
              <div className="correct-answers">
                <h6>Correct Answers:</h6>
                <p className="m-0">{eachQuestion.answer}</p>
              </div>
            </div>
            :
            null
        }
        {
          eachQuestion.type === 'VSAQ'
            ?

            <div className="card-body">
              <div className="memory-desription">
                <p>{eachQuestion.preview.split('(')[0]}
                  <span style={{ padding: "0 10px" }}><input type="text" placeholder="" disabled={true} /> </span>
                  {eachQuestion.preview.substring(eachQuestion.preview.indexOf(')') + 1)}
                </p>
              </div>
              <div className="correct-answers">
                <h6>Correct Answers:</h6>
                <p className="m-0">{eachQuestion.answer}</p>
              </div>
            </div>
            :
            null
        }
        {
          eachQuestion.type === 'EMQ'
            ?
            <div className="card-body">
              <div className="memory-desription">
                <p>{eachQuestion.preview}</p>
              </div>
              <div className="memory-card-scenario">
                <div className="scenario-head">
                  <p className="mb-0">Scenario</p>
                  <span>1</span>
                </div>
                <div className="scenario-description">
                  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                </div>
              </div>
              <div className="correct-answers">
                <h6>Correct Answers:</h6>
                <p className="m-0">Distracted by the Readable</p>
              </div>
              <div className="memory-card-scenario">
                <div className="scenario-head">
                  <p className="mb-0">Scenario</p>
                  <span>1</span>
                </div>
                <div className="scenario-description">
                  <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                </div>
              </div>
            </div>
            :
            null
        }
      </div>
    )))
  }

  onClickSetMmcToDelete = (e) => {
    const { id } = e.target;
    this.setState({ deleteMMC: parseInt(id) });
  }

  onClickDeleteThisMemoryCard = () => {
    const deleteId = this.state.deleteMMC;
    var tempMemos = { ...this.state.memo } ? { ...this.state.memo } : {};
    if (tempMemos.questions) {
      const thisQuestion = tempMemos.questions.find(question => question.id === deleteId);
      var thisIndex = -1;
      if (thisQuestion !== undefined) {
        thisIndex = tempMemos.questions.indexOf(thisQuestion);
      }
    }
    if (deleteId) {
      axios.delete(`/memorizers/${this.state.memo.id}/questions/${deleteId}`)
        .then(res => {
          if (res.data) {
            if (thisIndex > -1) {
              tempMemos.questions.splice(thisIndex, 1);
              this.setState({ memo: tempMemos })
            }
            this.props.onDeleteMemo();
          }
        })
    }
  }

  onEnterNewQuestion = (e) => {
    const { value } = e.target;
    var newQuestion = this.state.newQuestion;
    newQuestion.title = value;
    this.setState({ newQuestion: newQuestion, error: '' })
  }

  onEnterOption = (e) => {
    const { value, id } = e.target;
    const myObject = {
      title: value,
      id,
      correct: 0
    }
    var newQuestion = this.state.newQuestion;
    if (newQuestion.options.length) {
      const myItem = newQuestion.options.find(option => option.id === id);
      var myIndex = -1;
      if (myItem) {
        myIndex = newQuestion.options.indexOf(myItem);
      }
      if (myIndex > -1) {
        newQuestion.options[myIndex] = myObject;
      }
      else {
        newQuestion.options.push(myObject);
      }
    }
    else {
      newQuestion.options.push(myObject);
    }
    
    this.setState({ newQuestion: newQuestion, error: '' })
  }

  onMarkAgreement = (e) => {
    const { checked } = e.target;
    var myQuestion = this.state.newQuestion;
    myQuestion.agreement = checked;
    this.setState({ newQuestion: myQuestion, error: '' });
  }

  onMarkCorrectOption = (e) => {
    const { id } = e.target;
    const myQuestion = this.state.newQuestion;
    var myColors = this.state.colors;
    if (myQuestion.options.length) {
      const thisOption = myQuestion.options.find(option => option.id === id);
      if (thisOption) {
        const thisIndex = myQuestion.options.indexOf(thisOption);
        for (const i in myQuestion.options) {
          if (i == thisIndex) {
            myQuestion.options[i].correct = 1;
            myColors[i] = true;
          }
          else {
            myQuestion.options[i].correct = 0;
            myColors[i] = 'false';
          }
        }
        
        this.setState({ newQuestion: myQuestion, error: '' })
      }
      else {
        this.props.onShowToast('Please enter an option before marking correct.', 'info');
        this.setState({ error: 'Please enter an option before marking correct.' })
      }
    }
  }

  isOptionCorrect = (id) => {
    const myQuestion = this.state.newQuestion;
    if (myQuestion.options.length) {
      const myOption = myQuestion.options.find(option => option.id == id);
      if (myOption !== undefined) {
        if (myOption.correct == 1) {
          return true;
        }
      }
    }
    return false;
  }

  onClickAddQuestion = () => {
    const myQuestion = this.state.newQuestion;
    if (!myQuestion.title || !myQuestion.title.length) {
      this.setState({ error: 'Please enter a question title.' })
      return;
    }
    var correctFound = false;
    for (let i = 0; i < 4; i++) {
      const thisOption = myQuestion.options[i];
      if (thisOption === undefined || !thisOption.title || !thisOption.title.length) {
        this.setState({ error: 'None of the options field can be empty.' })
        return;
      }
      if (thisOption.correct == 1) {
        correctFound = true;
      }
    }
    if (!correctFound) {
      this.setState({ error: 'One of the options has to be marked correct.' })
      return;
    }
    if (!myQuestion.agreement) {
      this.setState({ error: 'You must agree to the T&C.' });
      return;
    }
    const crossEl = document.getElementById('cross');
    crossEl.click();
    let that = this;
    var tempMemos = { ...this.state.memo } ? { ...this.state.memo } : {};
    axios.post(`/memorizers/${this.state.memo.id}/questions`,
      {
        ...this.state.newQuestion,
        custom: "true"
      })
      .then(res => {
        
        if (res.data) {
          this.props.onDeleteMemo()
        }
      })
  }

  render() {
    
    return (
      <div>
        <div className="tab-inner-sec memory-cards">
          <div className="row">
            <div className="col-lg-6 py-3">
              <ul className="list-unstyled">
                <li>
                  <h1 className="m-0 f-s-22">{this.state.memo.title}</h1>
                </li>
                <li className="custom-subtitle-color">Last updated: {this.state.memo.updated_at}</li>
              </ul>
            </div>
            <div className="col-lg-6 py-3 text-right">
              <button className="custom-theme-button-style mr-2">
                <a href="#"
                  className="addMem"
                  data-toggle="modal"
                  data-target="#mymemoryModal"
                >Add Memory card</a>
              </button>
              {/* <button data-toggle="modal" data-target="#myDeleteModal" className="custom-theme-button-style">
                <a href="#">Delete</a>
              </button> */}
            </div>
          </div>
          <div className="memorizer-inner-part">
            <div className="memorizer-inner-first-part">
              <div className="row">
                <div className="col-lg-12 col-md-12">
                  {this.getMemoryCards()}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal" id="myDeleteMemoryCardModal">
          <div className="modal-dialog delete-memorizer-modal-dialog">
            <div className="modal-content delete-memorizer-modal-content">
              {/* Modal body */}
              <div className="modal-body delete-memorizer-modal" style={{ width: '500px' }}>
                <div className="alert-image text-center">
                  <img src="images/alert.png" alt="" />
                </div>
                <div className="alert-head text-center">
                  <h2 className="f-w-700">Alert</h2>
                  <p> Are you sure you want to delete this
                                    memory card?</p>
                </div>
                <div className="alert-buttons text-center">
                  <button className="no-button"
                    data-toggle="modal" data-target="#myDeleteMemoryCardModal"
                  >No</button>
                  <button
                    className="yes-button"
                    data-toggle="modal" data-target="#myDeleteMemoryCardModal"
                    onClick={this.onClickDeleteThisMemoryCard}
                  >Yes</button>
                </div>
              </div>
              {/* Modal footer */}
            </div>
          </div>
        </div>

        <div className="modal" id="mymemoryModal">
          <div className="modal-dialog">
            <div className="modal-content">
              {/* Modal Header */}
              <div className="modal-header border-0">
                <button id="cross" type="button" className="close" data-dismiss="modal">×</button>
              </div>
              {/* Modal body */}
              <div className="modal-body">
                <div className="d-flex justify-content-between">
                  <div className="add-question-head">
                    <h5> Add Question </h5>
                  </div>
                  <div className="type-mcq">
                    <h5> Type: MCQ </h5>
                  </div>
                </div>
                <div className="type-question-here">
                  <textarea className="form-control" rows={5} id="comment" placeholder="Type Question here" onChange={this.onEnterNewQuestion} />
                </div>
                <ul className="list-unstyled question-lists">
                  <li>
                    <div className="add-question-sec">
                      <label>A:</label>
                      <input id={0} type="text" className="custom-inputt-box" placeholder="Enter Option" onChange={this.onEnterOption} />
                      <button id={0}
                        className={`correct-button ${this.state.colors[0] === true ? 'correct-button-correct' : 'none'}`}
                        onClick={this.onMarkCorrectOption}
                      >Correct</button>
                    </div>
                  </li>
                  <li>
                    <div className="add-question-sec">
                      <label>B:</label>
                      <input id={1} type="text" className="custom-inputt-box" placeholder="Enter Option" onChange={this.onEnterOption} />
                      <button id={1} className={`correct-button ${this.state.colors[1] === true ? 'correct-button-correct' : 'none'}`}
                        onClick={this.onMarkCorrectOption}
                      >Correct</button>
                    </div>
                  </li>
                  <li>
                    <div className="add-question-sec">
                      <label>C:</label>
                      <input id={2} type="text" className="custom-inputt-box" placeholder="Enter Option" onChange={this.onEnterOption} />
                      <button id={2} className={`correct-button ${this.state.colors[2] === true ? 'correct-button-correct' : 'none'}`}
                        onClick={this.onMarkCorrectOption}
                      >Correct</button>
                    </div>
                  </li>
                  <li>
                    <div className="add-question-sec">
                      <label>D:</label>
                      <input id={3} type="text" className="custom-inputt-box" placeholder="Enter Option" onChange={this.onEnterOption} />
                      <button id={3} className={`correct-button ${this.state.colors[3] === true ? 'correct-button-correct' : 'none'}`}
                        onClick={this.onMarkCorrectOption}
                      >Correct</button>
                    </div>
                  </li>
                  <li>
                    <label className="check-box-container checkkboxx px-4">Terms and Condition Check box will be displayed here
                      <input type="checkbox" onChange={this.onMarkAgreement} />
                      <span className="checkbox-checkmark" />
                    </label>
                  </li>
                  <li>{this.state.error}</li>
                </ul>
              </div>
              {/* Modal footer */}
              <div className="modal-footer-wrapper text-center">
                <button className="add-memory-card" onClick={this.onClickAddQuestion}>Add Memory Card</button>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default SingleMemorizer;