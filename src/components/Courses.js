import React, { Component } from 'react';
import axios from 'axios';
import Memorizer from './Memorizer';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Revise from './Revise';
import Test from './Test';
import { Redirect , withRouter  } from 'react-router-dom';

toast.configure({
  autoClose: 8000,
  draggable: false,
  //etc you get the idea
});


class Courses extends Component {
  constructor(props) {
    super(props);
    localStorage.setItem("courseId" , this.props.match.params.courseId );
    
    this.state = {
      currentCourse: {
        title: 'Course Title'
      },
      performances: {
        warning: '',
        attempted_questions: 0,
        average_score: 0,
        course_completion: 0
      },
      activeTab: 'performance',
      memorizers: [],
      isNavClickable: true
    }
  }

  getPerformanceResult = (id) => {
    let that = this;
    axios.get(`/courses/${id}/analysis`)
      .then(res => { that.setState({ performances: res.data }) })
      .catch(err => {
        let data = err.response.data.errors;
        this.onShowToast(data); 
      })
  }

  onShowToast = (msg) => {
    let that = this;
    let message = msg ? Object.values(msg)[0] : "Something went wrong";
    return toast.error(message, {
      position: "top-center",
      autoClose: 3000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: false,
      onClose: () => { that.props.history.push('/profile') }
    });
  }

  updateNavigation = (e) => {
    const { id, fromFunc } = e.target;
    const courseId = this.props.match.params.courseId;
    // const { courseId, activeTab, testId } = this.props.match.params;
    if (id.includes('performance')) {
      this.setState({ activeTab: 'performance' })
      this.props.history.push(`/courses/${courseId}/performance`);
      this.getPerformanceResult(courseId);
    }
    else if (id.includes('test')) {
      this.setState({ activeTab: 'tests' })
      this.props.history.push(`/courses/${courseId}/tests`);
    }
    else if (id.includes('memorizer')) {
      this.setState({ activeTab: 'memorizers' })
      this.props.history.push(`/courses/${courseId}/memorizers`);
    }
    else if (id.includes('revise')) {
      this.setState({ activeTab: 'revise' })
      this.props.history.push(`/courses/${courseId}/revise`);
    }
    else {
      window.location.href = window.location.origin;
    }
  }

  initializeMemos = () => {
    axios.get('/memorizers')
      .then(res => {
        this.setState({ memorizers: res.data });
      })
  }

  getMemorizers = (memos) => {
    this.setState({ memorizers: memos })
  }

  preventNavClicks = () => {
    this.setState({ isNavClickable: false });
  }

  enableNavClicks = () => {
    this.setState({ isNavClickable: true });
  }

  setActiveTab = activeTab => {
    if (activeTab === 'performance') {
      document.getElementById('nav-performance-tab').click();
      // this.setState({ activeTab: activeTab })
    }
    else if (activeTab === 'tests') {
      document.getElementById('nav-tests-tab').click();
      // this.setState({ activeTab: activeTab })
    }
    else if (activeTab === 'revise') {
      document.getElementById('nav-revise-tab').click();
      // this.setState({ activeTab: activeTab })
    }
    else if (activeTab === 'memorizers') {
      document.getElementById('nav-memorizer-tab').click();
      // this.setState({ activeTab: activeTab })
    }
    else {
      document.getElementById('nav-performance-tab').click();
      // this.setState({ activeTab: 'performance' });
    }
  }

  checkURLParams = () => {
    const { courseId, activeTab, testId } = this.props.match.params;

    if (testId || activeTab) {
      let that = this;
      this.getPerformanceResult(courseId);
      axios.get('/courses/' + courseId)
        .then(res => {
          that.setState({ currentCourse: res.data, courseId: courseId })
          that.setActiveTab(activeTab);
        })
        .catch(err => {
          this.onShowToast();
        })
    }
    else if (courseId) {
      let that = this;
      this.getPerformanceResult(courseId);
      axios.get('/courses/' + courseId)
        .then(res => {
          that.setState({ currentCourse: res.data, courseId: courseId })
          that.setActiveTab('performance');
        })
        .catch(err => {
          
          this.onShowToast();
        })
    }
    else {
      toast.error("No match");
      alert("no match");
      
    }

    this.initializeMemos();

    

  }

  componentDidMount() {
    if (localStorage.getItem('token')) {
      this.checkURLParams();
    }
  }

  render() {
    if (localStorage.getItem('token')) {
      return (
        <div>
          <section className="my-profile-section">
            <div className="container">
              <div className="my-profile">
                <h1 className="text-white">My Profile</h1>
              </div>
              <div className="card border-0 profile-card">
                <div className="profile-header bg-purple">
                  <div className="profile-head-button">
                    <button className="pull-right"><a href="#" >{this.state.currentCourse.title}</a>
                    </button>
                  </div>
                </div>
                <div className="card-body pt-0">
                  {/* Tabs */}
                  <section id="tabs">
                    <div className="container">
                      <nav className="border-bottom">
                        <div className="row">
                          <div className="col-lg-2 col-md-2 col-2">
                            <div className="pro-image ml-auto">
                              <img src={(JSON.parse(localStorage.getItem('user'))).image} alt="" className="w-100" style={{ borderRadius: "35px" }} />
                            </div>
                          </div>
                          <div className="col-lg-10 col-sm-10 col-10">
                            <div
                              className="nav nav-tabs ml-auto border-bottom-0"
                              id="nav-tab"
                              role="tablist"
                              style={{
                                pointerEvents: !this.state.isNavClickable ? 'none' : "auto"
                              }}>
                              <a className={`nav-item nav-link`}
                                id="nav-performance-tab" data-toggle="tab" href="#nav-performance"
                                role="tab" aria-controls="nav-performance" aria-selected="true"
                                onClick={this.updateNavigation}> <span id="performance">My Performance</span>
                              </a>
                              <a className="nav-item nav-link" id="nav-revise-tab" data-toggle="tab" href="#nav-revise"
                                role="tab" aria-controls="nav-revise" aria-selected="false"
                                onClick={this.updateNavigation}>
                                <span id="revise">Revise</span>
                              </a>
                              <a className="nav-item nav-link" id="nav-tests-tab" data-toggle="tab" href="#nav-test"
                                role="tab" aria-controls="nav-test" aria-selected="false"
                                onClick={this.updateNavigation}>
                                <span id="tests">Tests</span>
                              </a>
                              <a className="nav-item nav-link" id="nav-memorizer-tab" data-toggle="tab" href="#nav-memorizer"
                                role="tab" aria-controls="nav-memorizer" aria-selected="false"
                                onClick={this.updateNavigation}>
                                <span id="memorizers">Memorisers</span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </nav>
                      <div className="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                        <div className="tab-pane fade performance-tab" id="nav-performance" role="tabpanel" aria-labelledby="nav-home-tab">
                          <div className="performance-head-section">
                            <div className="performance-head">
                              <h2>Performance</h2>
                            </div>
                            <div className="row">
                              <div className="col-lg-6">
                                <div className=" row inner-performance-sec">
                                  <div className="col-lg-12">
                                    <div className="row">
                                      <div className="col-lg-6 col-md-6">
                                        <ul className="list-unstyled">
                                          <li>
                                            <div className="card px-2">
                                              <div className="row">
                                                <div className="col-lg-6 col-md-6 col-6">
                                                  <ul className="list-unstyled my-3">
                                                    <li>Average</li>
                                                    <li>Test Score: {this.state.performances.average_score}%</li>
                                                  </ul>
                                                </div>
                                                <div className="col-lg-6 col-md-6 col-6 average-score-graph">
                                                  <div className="wrapper">
                                                    <div className={`c100 p${this.state.performances.average_score} pink`}>
                                                      <span>{this.state.performances.average_score}%</span>
                                                      <div className="slice">
                                                        <div className="bar" />
                                                        <div className="fill" />
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </li>
                                        </ul>
                                      </div>
                                    </div>
                                    <div className="row">
                                      <div className="col-lg-6 col-md-6">
                                        <ul className="list-unstyled my-3">
                                          <li>
                                            <div className="card px-2">
                                              <div className="row">
                                                <div className="col-lg-6 col-md-6 col-6" style={{ display: 'flex', alignItems: 'center' }}>
                                                  <ul className="list-unstyled my-3">
                                                    <li>Question</li>
                                                    <li>Attempted</li>
                                                  </ul>
                                                </div>
                                                <div className="col-lg-6 col-md-6 col-6">
                                                  <div className="no-of-question-attempted my-2">
                                                    <h1 style={{ margin: "auto", textAlign: 'center' }}>{this.state.performances.attempted_questions}</h1>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </li>
                                        </ul>
                                      </div>
                                    </div>
                                    <div className="progress-bar-sec">
                                      <div className="progress-bar-head">
                                        <ul className="list-unstyled">
                                          <li>
                                            <h6>{this.state.currentCourse.title}</h6>
                                          </li>
                                          <li>
                                            <p className="m-0">Course Completion</p>
                                          </li>
                                        </ul>
                                      </div>
                                      <div className="progress" style={{ height: "45px", borderRadius: "25px" }}>
                                        <div className="progress-bar custom-progress-bar" role="progressbar" aria-valuenow={this.state.performances.course_completion} aria-valuemin={0} aria-valuemax={100} style={{ width: `${this.state.performances.course_completion}%` }}>{this.state.performances.course_completion}%</div>
                                        {/* <span className="pull-right">
                                                                                    <p className="text-right total-percent">100%</p>
                                                                                </span> */}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="col-lg-6">
                                <div className="wrapper">
                                  <div className={`c100 p${this.state.performances.average_score} pink`}>
                                    <span>{this.state.performances.average_score}%</span>
                                    <div className="slice">
                                      <div className="bar" />
                                      <div className="fill" />
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        {/*Home tab-end */}
                        {/* Revise-tab-start */}
                        <div className="tab-pane fade revise-tab" id="nav-revise" role="tabpanel" aria-labelledby="nav-profile-tab">
                          <Revise
                            key={this.state.activeTab.includes('revise')}
                            memorizers={this.state.memorizers}
                            courseId={window.location.pathname.split('/')[2]}
                          />
                        </div>
                        {/* Revise-tab-end-now */}
                        {/* test-tab-start-here */}
                        <div className="tab-pane fade test-tab" id="nav-test"
                          role="tabpanel"
                          aria-labelledby="nav-contact-tab"
                          style={{ color: "black" }}
                        >
                          <Test
                            key={this.state.activeTab.includes('test')}
                            courseId={window.location.pathname.split('/')[2]}
                            memorizers={this.state.memorizers}
                            preventNavClicks={this.preventNavClicks}
                            enableNavClicks={this.enableNavClicks}
                          />
                        </div>
                        {/* test-tab-end-now */}
                        {/* memorizer-start now */}
                        <div className="tab-pane fade test-tab" id="nav-memorizer"
                          role="tabpanel"
                          aria-labelledby="nav-about-tab"
                        >
                          <Memorizer
                            key={this.state.activeTab.includes('memo')}
                            setMemorizers={this.getMemorizers}
                          />
                        </div>
                      </div>
                      {/* tab-content-end */}
                    </div>
                    {/* container-end */}
                  </section>
                </div>
                {/* card-body-end */}
              </div>
            </div>
          </section>
        </div >
      );
    }
    else {
      return <h1>Please login before accessing this.</h1>
    }
  }
}

export default withRouter(Courses);