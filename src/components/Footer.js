import React from 'react';
import ReactWOW from 'react-wow'
import {Link} from 'react-router-dom';

const Footer = () => {
    return (
        <div className="footer" style={{marginTop: "10px"}}>
            <div className="container">
                <div className="row">
                    <ReactWOW animation='fadeInUp animated' duration="1s" delay="0.3s">
                        <div className="col-md-3 col-sm-6 col-xs-12 ">
                            <h4>ABOUT</h4>
                            <ul>
                                <li><Link to="/aboutus">About Us</Link></li>
                                <li><Link to="/faq">FAQ</Link></li>
                                <li><Link to="/contact">Contact us</Link></li>
                            </ul>
                        </div>
                    </ReactWOW>
                    <ReactWOW animation='fadeInUp animated' duration="1s" delay="0.3s">
                        <div className="col-md-3 col-sm-6 col-xs-12 ">
                            <h4>HELP</h4>
                            <ul>
                                <li><Link to="/advertise" className="MRCP1">Advertise</Link></li>
                                <li><Link to="/guidelines" className="MRCP1">Content Guidelines</Link></li>
                                <li><Link to="/Support" className="MRCP1">Support</Link></li>
                            </ul>
                        </div>
                    </ReactWOW>
                    <ReactWOW animation='fadeInUp animated' duration="1s" delay="0.3s">
                        <div className="col-md-3 col-sm-6 col-xs-12 ">
                            <h4>BUSINESS</h4>
                            <ul>
                                <li><Link to="/mostrecent">Most Recent</Link></li>
                                <li><Link to="/featured">Featured</Link></li>
                                <li><Link to="/mostviewed">Most Viewed</Link></li>
                            </ul>
                        </div>
                    </ReactWOW>
                    <ReactWOW animation='fadeInUp animated' duration="1s" delay="0.3s">
                        <div className="col-md-3 col-sm-6 col-xs-12 ">
                            <h4>FOLLOW US ON</h4>
                            <a href="#" target="_blank">
                            <i className="fa fa-facebook" aria-hidden="true" />
                            </a> 
                            <a href="#" target="_blank">
                                <i className="fa fa-instagram" aria-hidden="true" />
                            </a> 
                            <a href="#" target="_blank">
                                <i className="fa fa-google-plus" aria-hidden="true" />
                            </a> 
                            <a href="#" target="_blank">
                                <i className="fa fa-vine" aria-hidden="true" />
                            </a>
                        </div>
                    </ReactWOW>
                </div>

                <ReactWOW animation='fadeInUp animated' duration="1s" delay="0.3s">
                    <div className="bottom">
                        <div className="row">
                            <div className="col-md-6 col-sm-6"> 
                                <Link to="/terms">Terms of Use</Link> 
                                <Link to="/privacypolicy">Privacy Policy</Link> 
                            </div>
                            <div className="col-md-6 col-sm-6">
                                <p>Copyright 2019 Medtutor.co.uk</p>
                            </div>
                        </div>
                    </div>
                </ReactWOW>
            </div>
        </div>
    );
}

export default Footer;