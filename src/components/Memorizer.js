import React, { Component, Fragment } from "react";
import axios from "axios";
import { toast } from "react-toastify";
import RevisionHandler from "./RevisionHandler";
import SessionPerformance from "./SessionPerformance";
import "react-toastify/dist/ReactToastify.css";
import SingleMemorizer from "./SingleMemorizer";
import { withRouter } from "react-router-dom";

class Memorizer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      memorizers: [],
      SystemMemorizers: null,
      displayMemorizers: null,
      newMemoName: "",
      memoNameError: "",
      deleteMemoId: null,
      showSingleMemo: null,
      systemMemo: [],
      memoType: "system",
      currentStep: 1,
      revisionMode: "system",
      selectedQuestions: ["mcq"],
      questionsMode: "unanswered",
      numOfQuestions: 10,
      totalQuestions: 0,
      spaceRepitition: true,
      displayTopics: [],
      selectedTopics: ["all"],
      testQuestions: [],
      deleteRevisionId: null,
      errorMessage: "",
      previousRevisions: [],
      memoId: 0,
      memoTitle: "",
    };
  }

  onSelectRevisionMode = (e) => {
    const { id } = e.target;
    if (id === "system") {
      this.setState({
        revisionMode: id,
        selectedQuestions: ["all"],
        errorMessage: "",
      });
      return;
    }
    this.setState({
      revisionMode: id,
      selectedQuestions: ["all"],
      errorMessage: "",
    });
  };

  onMarkQuestionType = (e) => {
    const { id, checked } = e.target;
    let selections = this.state.selectedQuestions.length
      ? [...this.state.selectedQuestions]
      : [];
    var pushId = id;
    const index = selections.indexOf(pushId);
    if (checked) {
      if (id !== "all") {
        const allId = selections.indexOf("all");
        if (allId > -1) {
          selections.splice(allId, 1);
        }
      }
      if (index < 0) {
        if (pushId === "all") {
          selections = [];
        }
        selections.push(pushId);
      }
      if (
        selections.includes("vsaq") &&
        selections.includes("emq") &&
        selections.includes("mcq")
      ) {
        selections = [];
        selections.push("all");
      }
    } else {
      if (index > -1) {
        selections.splice(index, 1);
      }
    }

    this.setState({ selectedQuestions: selections, errorMessage: "" });
  };

  onMarkQuestionType = (e) => {
    const { id, checked } = e.target;
    let selections = this.state.selectedQuestions.length
      ? [...this.state.selectedQuestions]
      : [];
    var pushId = id;
    const index = selections.indexOf(pushId);
    if (checked) {
      if (id !== "all") {
        const allId = selections.indexOf("all");
        if (allId > -1) {
          selections.splice(allId, 1);
        }
      }
      if (index < 0) {
        if (pushId === "all") {
          selections = [];
        }
        selections.push(pushId);
      }
      if (
        selections.includes("vsaq") &&
        selections.includes("emq") &&
        selections.includes("mcq")
      ) {
        selections = [];
        selections.push("all");
      }
    } else {
      if (index > -1) {
        selections.splice(index, 1);
      }
    }
    // this.updateCheckBoxStatus(id, selections);

    this.setState({ selectedQuestions: ["mcq"], errorMessage: "" });
    // this.setState({ selectedQuestions: selections, errorMessage: '' });
  };

  onClickProceed = (e) => {
    const { id } = e.target;
    localStorage.setItem("testType", "memorizer");
    console.log("id.includes: ", id);
    if (id.includes("step1Proceed")) {
      if (!this.state.revisionMode.length) {
        this.setState({
          errorMessage: "Please select one of the above test modes.",
        });
        return;
      } else if (
        this.state.revisionMode === "classic" &&
        !this.state.selectedQuestions.length
      ) {
        this.setState({
          errorMessage: "Please select one or more question types.",
        });
        return;
      }
      this.setState({ currentStep: 3 });
    } else if (id.includes("step2Proceed")) {
      this.filterQuestions();
    }
  };

  onSelectQuestionsMode = (e) => {
    this.setState({ questionsMode: e.target.value });
  };

  onSelectTotalQuestions = (e) => {
    const amount = parseInt(e.target.value);
    this.setState({ numOfQuestions: amount });
  };

  onSelectSpaceRepititionMode = (e) => {
    const { id } = e.target;

    if (id === "Yes") {
      this.setState({ spaceRepitition: true });
    } else if (id === "No") {
      this.setState({ spaceRepitition: false });
    }
  };

  testCompleted = () => {
    this.setState({ currentStep: 5 });
  };

  filterQuestions = () => {
    var mode = this.state.selectedQuestions.includes("all")
      ? "all"
      : this.state.selectedQuestions;
    var topics = this.state.selectedTopics.includes("all")
      ? "all"
      : this.state.selectedTopics;
    const payload = {
      type: this.state.revisionMode.toLowerCase(),
      mode: mode,
      topics: topics,
      number_of_questions: this.state.numOfQuestions,
      selections: this.state.questionsMode,
      test_type: "revise",
      duration: 0,
      repetition: this.state.spaceRepitition,
    };

    let memoType = localStorage.getItem("memoType");
    // this.props.location.state.memoType

    if (this.state.memoType === "system") {
      payload.course_id = localStorage.getItem("courseId");
      axios.post(`/system-memorizers`, payload).then((res) => {
        // this.setState({ testQuestions: res.data });
        const myQuestions = res.data.questions;

        if (myQuestions.length) {
          myQuestions.sort(function (a, b) {
            return a.id - b.id;
          });
        }

        localStorage.setItem("activeTestId", res.data.id);
        if (localStorage.getItem("myTestAnswers")) {
          localStorage.removeItem("myTestAnswers");
        }
        this.setState({
          testQuestions: myQuestions,
          revisionId: res.data.id,
          completed: false,
          isNewTest: true,
          currentStep: 4,
        });
        localStorage.setItem("memoType", "");
      });
      return;
    } else if (this.state.memoType === "both") {
      payload.course_id = localStorage.getItem("courseId");
      axios.post(`/my-memorizers`, payload).then((res) => {
        // this.setState({ testQuestions: res.data });
        const myQuestions = res.data.questions;

        if (myQuestions.length) {
          myQuestions.sort(function (a, b) {
            return a.id - b.id;
          });
        }

        localStorage.setItem("activeTestId", res.data.id);
        if (localStorage.getItem("myTestAnswers")) {
          localStorage.removeItem("myTestAnswers");
        }
        this.setState({
          testQuestions: myQuestions,
          revisionId: res.data.id,
          completed: false,
          isNewTest: true,
          currentStep: 4,
        });
        localStorage.setItem("memoType", "");
      });
      return;
    } else if (this.state.memoType === "myMemo") {
      payload.course_id = localStorage.getItem("courseId");
      payload.memorizer_id = this.state.memoId;
      // payload.title = this.state.memoTitle;
      axios.post(`/memorizer/test`, payload).then((res) => {
        // this.setState({ testQuestions: res.data });
        const myQuestions = res.data.questions;

        if (myQuestions.length) {
          myQuestions.sort(function (a, b) {
            return a.id - b.id;
          });
        }

        localStorage.setItem("activeTestId", res.data.id);
        if (localStorage.getItem("myTestAnswers")) {
          localStorage.removeItem("myTestAnswers");
        }
        this.setState({
          testQuestions: myQuestions,
          revisionId: res.data.id,
          completed: false,
          isNewTest: true,
          currentStep: 4,
        });
        localStorage.setItem("memoType", "");
      });
      return;
    }
  };

  populateSystemMemorizers = (type) => {
    const memo = this.state.SystemMemorizers;
    // if (mems.length) {
    //   return mems.map((memo, index) => (
    return (
      <div className="col-lg-4 col-md-6 py-2">
        <div className="card">
          <div className="card-header bg-memorizer-card-color bg-memorizer-card-color">
            <div className="row">
              <div className="col-lg-10 col-md-10 col-10">
                <ul className="list-unstyled mb-0">
                  <li>
                    <h4 className="memorizer-head-color">
                      {memo && memo.title}
                    </h4>
                  </li>
                  <li className="memorizer-list-item-color">
                    Last updated:{" "}
                    <span className="f-w-500">{memo && memo.created_at}</span>
                  </li>
                </ul>
              </div>
              <div className="col-lg-2 col-md-2 col-2">
                <ul className="list-unstyled">
                  {/* <li className="trash-icon-custom-style">
                      <i
                        id={ memo && memo.id}
                        className="fa fa-trash"
                        aria-hidden="true"
                        onClick={this.onSelectDeleteMemo}
                        style={{ cursor: 'pointer' }}
                      />
                    </li> */}
                </ul>
              </div>
            </div>
          </div>
          <div className="card-body">
            <div className="row">
              <div className="col-lg-6 col-md-6 col-6">
                <div className="row">
                  <div className="col-lg-12 col-md-12 ">
                    <div className="row">
                      <div className="col-lg-9 col-md-8 col-8">
                        <p className="memorizer-para-color">Memory Cards</p>
                      </div>
                      <div className="col-lg-3 col-md-3 col-3 my-3 angle-right">
                        {" "}
                        <i className="fa fa-angle-right" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-md-4 col-5">
                <div className="amount-memorizer pull-right">
                  <h1 className="mt-0">{memo && memo.questions_count}</h1>
                </div>
              </div>
            </div>
          </div>
          <div className="memorizer-footer-card text-center">
            <a
              className="font-weight-bold"
              // id={ memo && memo.id}
              // onClick={() => this.setState({ currentStep: 2})}
              onClick={() => {
                if (memo && memo.questions_count === 0) {
                  toast.warn("Please add some questions to start the test.");
                } else {
                  this.setState({ currentStep: 2, memoType: "system" });
                }
              }}
              // onClick={() => { if(memo && memo.questions_count === 0){ toast.warn("Please add some questions to start the test.");} else { this.onClickProceed(null , type)} }}
            >
              Proceed
              {/* <span id={ memo && memo.id} className="pull-right pull-right pr-3">
                  <i id={ memo && memo.id} className="fa fa-angle-right" aria-hidden="true" onClick={this.onClickViewMemorizerDetails} />
                </span> */}
            </a>
          </div>
        </div>
      </div>
    );
    //   )
    //   )
    // }
    return null;
  };

  populateMemorizers = (type) => {
    const mems = this.state.memorizers;
    if (mems.length) {
      return mems.map((memo, index) => (
        <div key={index} className="col-lg-4 col-md-6 py-2">
          <div className="card">
            <div className="card-header bg-memorizer-card-color bg-memorizer-card-color">
              <div className="row">
                <div className="col-lg-10 col-md-10 col-10">
                  <ul className="list-unstyled mb-0">
                    <li>
                      <h4 className="memorizer-head-color">{memo.title}</h4>
                    </li>
                    <li className="memorizer-list-item-color">
                      Last updated:{" "}
                      <span className="f-w-500">{memo.updated_at}</span>
                    </li>
                  </ul>
                </div>
                <div className="col-lg-2 col-md-2 col-2">
                  <ul className="list-unstyled">
                    <li className="trash-icon-custom-style">
                      <i
                        id={memo.id}
                        className="fa fa-trash"
                        aria-hidden="true"
                        onClick={this.onSelectDeleteMemo}
                        style={{ cursor: "pointer" }}
                      />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="card-body">
              <div className="row">
                <div className="col-lg-6 col-md-6 col-6">
                  <div className="row">
                    <div className="col-lg-12 col-md-12 ">
                      <div className="row">
                        <div className="col-lg-9 col-md-8 col-8">
                          <p className="memorizer-para-color">Memory Cards</p>
                        </div>
                        <div className="col-lg-3 col-md-3 col-3 my-3 angle-right">
                          {" "}
                          <i className="fa fa-angle-right" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6 col-md-4 col-5">
                  <div className="amount-memorizer pull-right">
                    <h1 className="mt-0">{memo.questions_count}</h1>
                  </div>
                </div>
              </div>
            </div>
            <div className="memorizer-footer-card text-center">
              <a
                className="font-weight-bold"
                // href="#/"
                // id={memo.id}
                // onClick={() => this.setState({ currentStep: 2})}
                onClick={() => {
                  if (memo.questions_count === 0) {
                    toast.warn("Please add some questions to start the test.");
                  } else {
                    this.setState({
                      currentStep: 2,
                      memoId: memo.id,
                      memoTitle: memo.title,
                      memoType: "myMemo",
                    });
                    // this.onClickProceed(memo.id , type);
                  }
                }}
                // onClick={() => { if(memo.questions_count === 0){ toast.warn("Please add some questions to start the test.");} else {this.onClickProceed(memo.id , type)}}}
              >
                Proceed
                {/* <span id={memo.id} className="pull-right pull-right pr-3">
                  <i id={memo.id} className="fa fa-angle-right" aria-hidden="true" onClick={this.onClickViewMemorizerDetails} />
                </span> */}
              </a>
            </div>
          </div>
        </div>
      ));
    }
    return null;
  };

  onSelectDeleteMemo = (e) => {
    let that = this;
    this.setState({ deleteMemoId: e.target.id });
    const el = document.getElementById("deleteMemoButton");
    el.click();
  };

  onConfirmDeleteMemorizer = () => {
    let that = this;
    const myDeleteId = this.state.deleteMemoId;
    if (myDeleteId) {
      this.onShowToast("Memorizer delete prompt issued", "info");
      axios
        .delete(`/memorizers/${myDeleteId}`)
        .then((res) => {
          if (res.data.message) {
            that.onShowToast("Memorizer has been deleted...", "success");
          }
        })
        .catch((err) =>
          that.onShowToast("Error deleting memorizer...", "warning")
        );
    }
  };

  onClickViewMemorizerDetails = (e) => {
    const { id } = e.target;
    this.getSingleMemo(id);
  };

  onShowToast = (content, type) => {
    let that = this;
    this.getMemorizers();
    if (type === "success") {
      return toast.success(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
      });
    } else if (type === "info") {
      return toast.info(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
      });
    } else if (type === "warning") {
      return toast.warning(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
      });
    } else if (type === "danger") {
      return toast.danger(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
      });
    }
  };

  onCreateMemo = () => {
    if (!this.state.newMemoName.length) {
      this.setState({ memoNameError: "Memo name cannot be empty!" });
      return;
    }
    document.getElementById("myNewMemoModal").click();
    axios
      .post("/memorizers", { title: this.state.newMemoName })
      .then((res) => {
        if (res.data) {
          this.onShowToast("Memorizer has been saved!", "success");
        } else {
          this.onShowToast(`Memorizer couldn't be saved!`, "warning");
        }
      })
      .catch((err) =>
        this.onShowToast(`Memorizer couldn't be saved!`, "warning")
      );
  };

  getMemorizers = () => {
    if (localStorage.getItem("token")) {
      let that = this;
      axios.get("/memorizers").then((res) => {
        that.setState(
          {
            memorizers: res.data,
          },
          () => {
            // that.populateMemorizers()
            that.props.setMemorizers(res.data);
          }
        );
      });
    }
  };

  onClickDeleteThisModal = () => {};

  getSingleMemo = (memoId) => {
    let that = this;
    // const [, , courseId, activeTab, activeTabId] = window.location.pathname.split('/');
    const { courseId, activeTab, testId } = this.props.match.params;
    if (testId) {
      memoId = testId;
    }
    if (memoId > -1) {
      let that = this;
      axios.get(`/memorizers/${memoId}`).then((res) => {
        that.setState({ showSingleMemo: res.data }, () => {
          if (!testId) {
            that.props.history.push(
              `/courses/${courseId}/memorizers/${memoId}`
            );
          }
        });
      });
    }
  };

  componentDidMount() {
    // const [, , courseId, activeTab, memoId] = window.location.pathname.split('/');
    const { courseId, activeTab, testId } = this.props.match.params;
    const memoId = testId;
    if (activeTab == "memorizers") {
      if (memoId && parseInt(memoId) > -1) {
        this.getSingleMemo(parseInt(memoId));
      } else {
        this.getMemorizers();
      }
    }
    this.onSystemMemorizer();
  }
  onSystemMemorizer = async () => {
    try {
      const data = await axios.get("/system-memorizers");

      this.setState({
        systemMemo: data.data.questions,
        SystemMemorizers: data.data,
      });
    } catch (error) {
      toast.error("Something went wrong");
    }
  };
  getMemoryCards = () => {
    const myMemoryCards = this.state.systemMemo;
    return myMemoryCards.map((eachQuestion, index) => (
      <div className="card memorizer-first-card memory-card-1">
        <div className="card-header bg-memorizer-card-color bg-memorizer-card-color memorizer-card-header">
          <div className="row">
            <div className="col-lg-10 col-md-9 col-7">
              <div className="memory-card-head">
                <h6 className="memorizer-head-color">Question {index + 1}</h6>
              </div>
            </div>
            <div className="col-lg-2 col-md-3 col-5 text-right ">
              <div className="d-flex justify-content-between">
                <div className="type-sec">
                  <span>Type: </span>
                  <span className="mcq-type">
                    {eachQuestion.type === "MEMORIZER"
                      ? "MCQ"
                      : eachQuestion.type}
                  </span>
                </div>
                {/* <div className="trash-icon-custom-style">
                  <a data-toggle="modal" href="#myDeleteMemoryCardModal" >
                    <i className="fa fa-trash" aria-hidden="true" id={eachQuestion.id} onClick={this.onClickSetMmcToDelete} />
                  </a>
                </div> */}
              </div>
            </div>
          </div>
        </div>
        {eachQuestion.type === "MCQ" || eachQuestion.type === "MEMORIZER" ? (
          <div className="card-body">
            <div className="memory-desription">
              <p>{eachQuestion.title}</p>
            </div>
            {/* <div className="correct-answers">
                <h6>Correct Answers:</h6>
                <p className="m-0">{eachQuestion.answer}</p>
              </div> */}
          </div>
        ) : null}
        {eachQuestion.type === "VSAQ" ? (
          <div className="card-body">
            <div className="memory-desription">
              <p>
                {eachQuestion.preview.split("(")[0]}
                <span style={{ padding: "0 10px" }}>
                  <input type="text" placeholder="" disabled={true} />{" "}
                </span>
                {eachQuestion.preview.substring(
                  eachQuestion.preview.indexOf(")") + 1
                )}
              </p>
            </div>
            <div className="correct-answers">
              <h6>Correct Answers:</h6>
              <p className="m-0">{eachQuestion.answer}</p>
            </div>
          </div>
        ) : null}
        {eachQuestion.type === "EMQ" ? (
          <div className="card-body">
            <div className="memory-desription">
              <p>{eachQuestion.preview}</p>
            </div>
            <div className="memory-card-scenario">
              <div className="scenario-head">
                <p className="mb-0">Scenario</p>
                <span>1</span>
              </div>
              <div className="scenario-description">
                <p>
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem Ipsum is that it has a more-or-less
                  normal distribution of letters, as opposed to using 'Content
                  here, content here', making it look like readable English.
                </p>
              </div>
            </div>
            <div className="correct-answers">
              <h6>Correct Answers:</h6>
              <p className="m-0">Distracted by the Readable</p>
            </div>
            <div className="memory-card-scenario">
              <div className="scenario-head">
                <p className="mb-0">Scenario</p>
                <span>1</span>
              </div>
              <div className="scenario-description">
                <p>
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem Ipsum is that it has a more-or-less
                  normal distribution of letters, as opposed to using 'Content
                  here, content here', making it look like readable English.
                </p>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    ));
  };

  // onClickProceed = (id , type) => {
  //   const { courseId, activeTab, testId } = this.props.match.params;
  //   if(type === "system"){
  //     localStorage.setItem('memoType', type);
  //     this.props.history.push({
  //       pathname: `/courses/${courseId}/tests`,
  //     });
  //     window.location.reload();
  //     return;
  //   }
  //   else if(type === "myMemo"){
  //     localStorage.setItem('memoType', type);
  //     localStorage.setItem('memoId' , id);
  //     this.props.history.push({
  //       pathname: `/courses/${courseId}/tests`,
  //     });
  //     window.location.reload();
  //     return;
  //   }
  //   localStorage.setItem('memoType', this.state.memoType);
  //   localStorage.setItem('memoId' , id);
  //   this.props.history.push({
  //     pathname: `/courses/${courseId}/tests`,
  //   });
  //   window.location.reload();

  // }

  render() {
    if (this.state.showSingleMemo) {
      return (
        <SingleMemorizer
          memo={this.state.showSingleMemo}
          onDeleteMemo={this.getMemorizers}
          onShowToast={this.onShowToast}
        />
      );
    }
    if (this.state.currentStep === 2) {
      return (
        <section id="st1" className="revise-now-sec">
          <div className="revise-now-head">
            <h3>Memoriser</h3>
          </div>
          <div className="revise-now-desc">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
          </div>
          <div className="row">
            <div className="col-lg-6 col-md-6 col-sm-6 col-12">
              <div className="revise-sec-radio-check-box-sec">
                <ul className="list-unstyled">
                  <li
                    className="d-inline-block"
                    style={{ padding: "5px 20px" }}
                  >
                    <label className="radio-container">
                      System Mode
                      <input
                        type="radio"
                        checked={this.state.revisionMode === "system"}
                        id="system"
                        name="radio"
                        onChange={this.onSelectRevisionMode}
                      />
                      <span className="radio-checkmark" />
                    </label>
                  </li>
                  <li
                    className="d-inline-block"
                    style={{ padding: "5px 20px" }}
                  >
                    <label className="radio-container">
                      Classic Mode
                      <input
                        type="radio"
                        id="classic"
                        name="radio"
                        onChange={this.onSelectRevisionMode}
                      />
                      <span className="radio-checkmark" />
                    </label>
                  </li>
                </ul>
                <div className="row pl-3 pr-3">
                  <div className="col-lg-6 col-md-12 col-sm-6 col-12">
                    <p>Question type</p>
                    <ul className="list-unstyled">
                      {/* <li className="d-inline-block pull-left w-50">
                        <label className="check-box-container">All
                          <input type="checkbox" id="all"
                            onChange={this.onMarkQuestionType}
                            checked={this.state.selectedQuestions.includes('all')}
                          />
                          <span className="checkbox-checkmark" />
                        </label>
                      </li> */}
                      {/* <li className="d-inline-block pull-right w-50">
                        <label className="check-box-container">EMQS
                          <input type="checkbox" id="emq"
                            onChange={this.onMarkQuestionType}
                            checked={this.state.selectedQuestions.includes('emq') && !this.state.selectedQuestions.includes('all')}
                          />
                          <span className="checkbox-checkmark" />
                        </label>
                      </li> */}
                      <li className="d-inline-block pull-left w-50">
                        <label className="check-box-container">
                          MCQS
                          <input
                            type="checkbox"
                            id="mcq"
                            onChange={this.onMarkQuestionType}
                            checked={
                              this.state.selectedQuestions.includes("mcq") &&
                              !this.state.selectedQuestions.includes("all")
                            }
                          />
                          <span className="checkbox-checkmark" />
                        </label>
                      </li>
                      {/* <li className="d-inline-block pull-right w-50">
                        <label className="check-box-container">VSAQS
                          <input type="checkbox" id="vsaq"
                            onChange={this.onMarkQuestionType}
                            checked={this.state.selectedQuestions.includes('vsaq') && !this.state.selectedQuestions.includes('all')}
                          />
                          <span className="checkbox-checkmark" />
                        </label>
                      </li> */}
                    </ul>
                  </div>
                </div>
                <p style={{ marginLeft: "20px" }}>{this.state.errorMessage}</p>
                <div
                  className="col-lg-6 col-md-12 col-sm-6 col-12"
                  style={{ margin: "5px 0px" }}
                >
                  <div className="proceed-button mt-2 mb-3">
                    <button id="step1Proceed" onClick={this.onClickProceed}>
                      Proceed
                      <i
                        id="step1Proceedi"
                        className="fa fa-chevron-circle-right"
                        aria-hidden="true"
                        style={{ marginLeft: "5px" }}
                      />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {this.state.previousRevisions.length ? (
            <div className="previous-revision my-4">
              <div className="previous-revision-head my-4">
                <h3>Previous Revision</h3>
              </div>
              {this.showPreviousRevisions()}
            </div>
          ) : null}

          <div className="modal" id="myModal">
            <div className="modal-dialog rewise-modal-dialog">
              <div className="modal-content delete-memorizer-modal-content">
                {/* Modal body */}
                <div
                  className="modal-body rewise-modal-delete"
                  style={{ width: "500px" }}
                >
                  <div className="alert-image text-center">
                    <img
                      src={`${process.env.PUBLIC_URL}/images/alert.png`}
                      alt=""
                    />
                  </div>
                  <div className="alert-head text-center">
                    <h2 className="f-w-700">Alert</h2>
                    <p> Are you sure you want to delete this revision?</p>
                  </div>
                  <div className="alert-buttons text-center">
                    <button
                      className="no-button"
                      onClick={this.onClickCancelDeleteRevision}
                    >
                      No
                    </button>
                    <button
                      className="yes-button"
                      onClick={this.onClickDeleteRevision}
                    >
                      Yes
                    </button>
                  </div>
                </div>
                {/* Modal footer */}
              </div>
            </div>
          </div>
        </section>
      );
    } else if (this.state.currentStep === 3) {
      return (
        <div id="st2">
          <div className="revise-go-back-sec">
            {/* <a href="#/" onClick={this.goBackToAllRevisions}> */}
            <a href="#/" onClick={() => this.setState({ currentStep: 1 })}>
              <span style={{ marginRight: "5px" }}>
                <i className="fa fa-angle-left" aria-hidden="true"></i>
              </span>
              Go back
            </a>
          </div>
          <br />
          <div className="revise-now-head">
            <h3>Memoriser</h3>
          </div>
          <div className="revise-now-desc">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
          </div>
          <div className="row">
            <div className="col-lg-6 col-md-12">
              <div className="row">
                <div className="col-lg-5 col-md-6">
                  <div className="card h-100">
                    <div className="card-body">
                      <ul className="list-unstyled text-center">
                        <li>
                          <img
                            src={`${process.env.PUBLIC_URL}/images/type.png`}
                          />
                        </li>
                        <li>Type</li>
                        <li>
                          <h4>
                            {this.state.revisionMode === "system"
                              ? "System Mode"
                              : "Classic Mode"}
                          </h4>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="col-lg-5 col-md-6">
                  <div className="card">
                    <div className="card-body">
                      <ul className="list-unstyled text-center">
                        <li>
                          <img
                            src={`${process.env.PUBLIC_URL}/images/que-type.png`}
                          />
                        </li>
                        <li>Questions Type</li>
                        <li>
                          <h4>
                            {this.state.selectedQuestions.map((question) => {
                              return `${question.toUpperCase()}\n`;
                            })}
                          </h4>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6">{/* empty-sec */}</div>
          </div>
          <br />
          {this.state.revisionMode === "classic" ? (
            <div className="questions-topic-sec my-5">
              <div className="questions-info-first-sec my-3">
                <h6>Total Questions Found {this.state.totalQuestions}</h6>
              </div>
              <div className="row">
                <div className="col-lg-6">
                  <div className="card question-type-card">
                    <div className="card-header border-bottom-0">
                      <h6>Topics</h6>
                    </div>
                    <div className="card-body">
                      <div className="questions-type">
                        <h6>Question Type</h6>
                      </div>
                      <div className="row">{this.state.displayTopics}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : null}
          <br />
          <div className="question-selection-sec">
            <div className="row">
              <div className="col-lg-6">
                <div className="question-selection-main-sec">
                  <ul className="list-unstyled">
                    <li className="mb-3">
                      <h6>Question Selection</h6>
                    </li>
                    <li>
                      <div className="form-group">
                        <select
                          className="form-control"
                          id="sel1"
                          onChange={this.onSelectQuestionsMode}
                        >
                          <option value="unanswered">New Questions Only</option>
                          <option value="answered">
                            Repeated Questions Only
                          </option>
                          <option value="all">
                            New and Repeated Questions
                          </option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-6">
                <div className="no-of-question-sec">
                  <ul className="list-unstyled">
                    <li>
                      <h6>Number of Questions</h6>
                    </li>
                    <li>
                      <div className="slidecontainer">
                        <input
                          data-toggle="tooltip"
                          title={this.state.numOfQuestions}
                          data-placement="bottom"
                          type="range"
                          min={1}
                          max={100}
                          defaultValue={this.state.numOfQuestions}
                          className="slider"
                          id="myRange"
                          onChange={this.onSelectTotalQuestions}
                        />
                        <p>
                          Total questions needed : {this.state.numOfQuestions}
                        </p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="apply-space-repetition-sec my-4">
            <div className="apply-space-repetition-sec-head">
              <h6>Apply Space Repetition ?</h6>
            </div>
            <div className="apply-space-repetition-sec-desc">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </p>
            </div>
            <div className="apply-space-repetition-check-box">
              <div className="row">
                <div className="col-lg-4">
                  <ul className="list-unstyled">
                    <li className="d-inline-block">
                      <label className="radio-container">
                        Yes
                        <input
                          id="Yes"
                          checked={this.state.spaceRepitition}
                          onChange={this.onSelectSpaceRepititionMode}
                          type="radio"
                          name="radio"
                        />
                        <span className="radio-checkmark" />
                      </label>
                    </li>
                  </ul>
                </div>
                <div className="col-lg-4">
                  <ul className="list-unstyled">
                    <li className="d-inline-block">
                      <label className="radio-container">
                        No
                        <input
                          id="No"
                          onChange={this.onSelectSpaceRepititionMode}
                          type="radio"
                          name="radio"
                        />
                        <span className="radio-checkmark" />
                      </label>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-md-12 col-sm-6 col-12">
            <div
              className="proceed-button mt-2 mb-3"
              style={{ textAlign: "left" }}
            >
              <button id="step2Proceed" onClick={this.onClickProceed}>
                Start
                <i
                  id="step2Proceedi"
                  className="fa fa-chevron-circle-right"
                  aria-hidden="true"
                  style={{ marginLeft: "5px" }}
                />
              </button>
            </div>
          </div>
        </div>
      );
    } else if (this.state.currentStep === 4) {
      return (
        <RevisionHandler
          testQuestions={this.state.testQuestions}
          courseId={localStorage.getItem("courseId")}
          testId={this.state.revisionId}
          isNewTest={this.state.isNewTest}
          isTestComplete={this.state.completed}
          repitition={this.state.spaceRepitition}
          memorizers={this.props.memorizers}
          goBackToAllRevisions={this.goBackToAllRevisions}
          testCompleted={this.testCompleted}
        />
      );
    } else if (this.state.currentStep === 5) {
      return (
        <SessionPerformance
          testId={this.state.revisionId}
          courseId={localStorage.getItem("courseId")}
          goBackToAll={this.goBackToAllRevisions}
        />
      );
    } else if (this.state.currentStep === 1) {
      return (
        // onChange={e => this.setState({ memoType: e.target.value })}
        <div>
          <nav className="memo-tabs">
            <div className="nav nav-tabs" id="nav-tab" role="tablist">
              <a
                onClick={(e) => this.setState({ memoType: "system" })}
                className="nav-item nav-link active"
                id="nav-home-tab"
                data-toggle="tab"
                href="#nav-home"
                role="tab"
                aria-controls="nav-home"
                aria-selected="true"
              >
                <div className="form-check">
                  {/* <input className="form-check-input" value='system' onChange={e => this.setState({ memoType: e.target.value })} type="radio" name="exampleRadios" id="exampleRadios1" defaultChecked /> */}
                  <label className="form-check-label" htmlFor="exampleRadios1">
                    {" "}
                    Pre-Set Questions{" "}
                  </label>
                </div>
              </a>
              <a
                onClick={(e) => this.setState({ memoType: "myMemo" })}
                className="nav-item nav-link"
                id="nav-profile-tab"
                data-toggle="tab"
                href="#nav-profile"
                role="tab"
                aria-controls="nav-profile"
                aria-selected="false"
              >
                <div className="form-check">
                  {/* <input className="form-check-input" value='myMemo' onChange={e => this.setState({ memoType: e.target.value })} type="radio" name="exampleRadios" id="exampleRadios2" /> */}
                  <label className="form-check-label" htmlFor="exampleRadios2">
                    {" "}
                    User Added Questions
                  </label>
                </div>
              </a>
              <a
                onClick={(e) => this.setState({ memoType: "both" })}
                className="nav-item nav-link"
                id="nav-contact-tab"
                data-toggle="tab"
                href="#nav-contact"
                role="tab"
                aria-controls="nav-contact"
                aria-selected="false"
              >
                <div className="form-check">
                  {/* <input className="form-check-input" value='both' onChange={e => this.setState({ memoType: e.target.value })} type="radio" name="exampleRadios" id="exampleRadios2" /> */}
                  <label className="form-check-label" htmlFor="exampleRadios2">
                    {" "}
                    Use Both{" "}
                  </label>
                </div>
              </a>
            </div>
          </nav>
          <div className="tab-content memo-content" id="nav-tabContent">
            <div
              className="tab-pane fade show active"
              id="nav-home"
              role="tabpanel"
              aria-labelledby="nav-home-tab"
            >
              {this.state.memoType === "system" ? (
                <div>
                  <h1>Pre-Set Questions</h1>
                  <div className="memorizer-inner-part">
                    <div className="memorizer-inner-first-part">
                      <div className="row">
                        {this.populateSystemMemorizers()}
                      </div>
                    </div>
                  </div>
                  {/* <div className="col-lg-6 col-md-12 col-sm-6 col-12" style={{ margin: "5px 0px" }}>
              <div className="proceed-button mt-2 mb-3">
                <button onClick={() => this.onClickProceed()} id="step1Proceed">Proceed
                      <i id="step1Proceedi" className="fa fa-chevron-circle-right" aria-hidden="true" style={{ marginLeft: '5px' }} />
                </button>
              </div>
            </div> */}
                </div>
              ) : null}
            </div>
            <div
              className="tab-pane fade"
              id="nav-profile"
              role="tabpanel"
              aria-labelledby="nav-profile-tab"
            >
              {this.state.memoType === "myMemo" ? (
                <div className="tab-inner-sec">
                  <div className="row">
                    <div className="col-lg-6 py-3">
                      <ul className="list-unstyled">
                        <li>
                          <h1 className="m-0 f-s-22">User Added Questions</h1>
                        </li>
                        <li className="custom-subtitle-color">
                          Total {this.state.memorizers.length}
                        </li>
                      </ul>
                    </div>
                    <div className="col-lg-6 py-3 text-right">
                      <button
                        data-toggle="modal"
                        data-target="#myNewMemoModal"
                        className="custom-theme-button-style"
                      >
                        <a href="#/">Create New Memoriser</a>
                      </button>
                    </div>
                  </div>
                  <div className="memorizer-inner-part">
                    <div className="memorizer-inner-first-part">
                      <div className="row">
                        {this.populateMemorizers("myMemo")}
                      </div>
                    </div>
                  </div>
                  {/* <div className="col-lg-6 col-md-12 col-sm-6 col-12" style={{ margin: "5px 0px" }}>
                <div className="proceed-button mt-2 mb-3">
                  <button onClick={() => this.onClickProceed()} id="step1Proceed">Proceed
                      <i id="step1Proceedi" className="fa fa-chevron-circle-right" aria-hidden="true" style={{ marginLeft: '5px' }} />
                  </button>
                </div>
              </div> */}
                </div>
              ) : null}
            </div>
            <div
              className="tab-pane fade"
              id="nav-contact"
              role="tabpanel"
              aria-labelledby="nav-contact-tab"
            >
              {this.state.memoType === "both" ? (
                <Fragment>
                  <h1>Pre-Set Questions</h1>
                  <div className="memorizer-inner-part">
                    <div className="memorizer-inner-first-part">
                      <div className="row">
                        {this.populateSystemMemorizers("system")}
                        {/* <div className="col-lg-12 col-md-12">
                      {this.getMemoryCards()}
                    </div> */}
                      </div>
                    </div>
                  </div>
                  <div className="tab-inner-sec">
                    <div className="row">
                      <div className="col-lg-6 py-3">
                        <ul className="list-unstyled">
                          <li>
                            <h1 className="m-0 f-s-22">User Added Questions</h1>
                          </li>
                          <li className="custom-subtitle-color">
                            Total {this.state.memorizers.length}
                          </li>
                        </ul>
                      </div>
                      <div className="col-lg-6 py-3 text-right">
                        <button
                          data-toggle="modal"
                          data-target="#myNewMemoModal"
                          className="custom-theme-button-style"
                        >
                          <a href="#/">Create New Memoriser</a>
                        </button>
                      </div>
                    </div>
                    <div className="memorizer-inner-part">
                      <div className="memorizer-inner-first-part">
                        <div className="row">{this.populateMemorizers("")}</div>
                      </div>
                    </div>
                    {/* <div className="col-lg-6 col-md-12 col-sm-6 col-12" style={{ margin: "5px 0px" }}>
                  <div className="proceed-button mt-2 mb-3">
                    <button onClick={() => this.onClickProceed()} id="step1Proceed">Proceed
                    <i id="step1Proceedi" className="fa fa-chevron-circle-right" aria-hidden="true" style={{ marginLeft: '5px' }} />
                    </button>
                  </div>
                </div> */}
                  </div>
                </Fragment>
              ) : null}
            </div>
          </div>

          <button
            id="deleteMemoButton"
            data-toggle="modal"
            data-target="#myDeleteMemorizerModal"
            style={{ display: "none" }}
          ></button>

          <div className="modal" id="myNewMemoModal">
            <div className="modal-dialog">
              <div className="modal-content">
                {/* Modal body */}
                <div className="modal-body text-center new-memorizer">
                  <img src="/images/new-memorizer.png" alt="" />
                  <h2>New Memoriser</h2>
                  <input
                    type="text"
                    className="w-100 custom-input-box"
                    placeholder="Enter Name"
                    onChange={(e) =>
                      this.setState({
                        memoNameError: "",
                        newMemoName: e.target.value,
                      })
                    }
                  />
                  <p>{this.state.memoNameError}</p>
                  <div className="submit-button-sec my-2">
                    <button
                      onClick={this.onCreateMemo}
                      className="w-100 py-3 custom-theme-button-style text-white font-weight-bold"
                    >
                      Create
                    </button>
                  </div>
                </div>
                {/* Modal footer */}
              </div>
            </div>
          </div>

          <div className="modal" id="myDeleteMemorizerModal">
            <div className="modal-dialog delete-memorizer-modal-dialog">
              <div className="modal-content delete-memorizer-modal-content">
                {/* Modal body */}
                <div
                  className="modal-body delete-memorizer-modal"
                  style={{ width: "500px" }}
                >
                  <div className="alert-image text-center">
                    <img src="images/alert.png" alt="" />
                  </div>
                  <div className="alert-head text-center">
                    <h2 className="f-w-700">Alert</h2>
                    <p> Are you sure you want to delete this memoriser?</p>
                  </div>
                  <div className="alert-buttons text-center">
                    <button
                      className="no-button"
                      data-toggle="modal"
                      data-target="#myDeleteMemorizerModal"
                    >
                      No
                    </button>
                    <button
                      className="yes-button"
                      data-toggle="modal"
                      data-target="#myDeleteMemorizerModal"
                      onClick={this.onConfirmDeleteMemorizer}
                    >
                      Yes
                    </button>
                  </div>
                </div>
                {/* Modal footer */}
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default withRouter(Memorizer);
