import React, { Component } from 'react';
import ReactWOW from 'react-wow'
import axios from 'axios';
import { Link, withRouter, Redirect } from 'react-router-dom';

class Navbar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showDropdown: false,
    }
  }

  onClickLogout = () => {
    axios.get('/logout')
      .then(res => {
        localStorage.clear();
        this.props.history.push('/login');
        // window.location.href = window.location.origin + '/login';
      })
    // return <Redirect to="/login" />
  }

  computeCourses = () => {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user && user.packages) {
      return user.packages.map((thisCourse, index) => {
        return <li key={index} className="my-portal-sub-menu">
          {/* <span>{thisCourse.course.title}</span> */}
          <Link to={`/courses/${thisCourse.course_id}/performance`} onClick={() => { this.handleDropDownClick() }}>
            {/* <a href={`/courses/${thisCourse.course_id}/performance`} onClick={() => { this.handleDropDownClick() }}> */}
            {thisCourse.course.title}
          </Link>
        </li>
      })
    }
    return null;
  }

  handleDropDownClick = () => {
    this.setState({ showDropdown: !this.state.showDropdown })
  }
  // componentDidMount() {
  //   let that = this;
  //   axios.get('/users/profile')
  //     .then(res => {
  //       localStorage.setItem('user', JSON.stringify(res.data));
  //       // that.setState({
  //       //   user: res.data
  //       // })
  //     })
  // }

  async componentDidMount() {
    const res = await axios.get('/users/profile');
    localStorage.setItem('user', JSON.stringify(res.data));
    // .then(res => {
    //   localStorage.setItem('user', JSON.stringify(res.data));

    // })
    const myUser = JSON.parse(localStorage.getItem('user'));
    this.setState({ user: myUser })


    // if (!myUser) {
    //   axios.get(`/users/profile`)
    //     .then(res => {
    //       localStorage.setItem('user', JSON.stringify(res.data));
    //       this.setState({ user: res.data })
    //     })
    // }
    // else {
    //   this.setState({ user: myUser })
    // }
  }

  render() {
    return (
      <header>
        <div className="container">
          <div className="row">
            <ReactWOW animation='fadeInDown' duration="0.3s" delay="0.9s">
              <div className="col-md-3">
                <a href="#" onClick={() => this.props.history.push('/')}>
                  <img src={`${process.env.PUBLIC_URL}/images/new-images_03.png`} className="logo" alt="" />
                </a>
              </div>
            </ReactWOW>
            <div className="col-md-9">
              <div id="menu">
                <ul>
                  <li className={`${window.location.pathname === '/' ? 'active' : null}`}>
                    <Link to='/'
                    // onClick={() => this.props.history.push('/revise')}
                    >Home</Link>
                  </li>
                  {/* <li className={`${window.location.pathname === '/revise' ? 'active' : null}`}>
                    <Link to='/revise'
                    // onClick={() => this.props.history.push('/revise')}
                    >Revise Now</Link>
                  </li> */}
                  <li className={`${window.location.pathname === '/aboutus' ? 'active' : null}`}>
                    <Link to='/aboutus'
                    // onClick={() => this.props.history.push('/revise')}
                    >About Us</Link>
                  </li>
                  <li className={`${window.location.pathname === '/contact' ? 'active' : null}`}>
                    <Link to='/contact'
                    // onClick={() => this.props.history.push('/revise')}
                    >Contact</Link>
                  </li>
                  <span onClick={this.handleDropDownClick}>
                    {
                      this.state.user ?
                        <li className="pro-image-icon">
                          <Link to="#" >
                            <img src={this.state.user.image
                              ? this.state.user.image
                              : "https://www.cappmea.com/uploads/speaker/images/160x160/facebook-default-no-profile-pic-14.jpg"}
                              alt=""
                              style={{ margin: "0 10px" }} /></Link>
                          <Link to="#" className="dropdown">My Revision Profile<i className="fa fa-caret-down" aria-hidden="true" style={{ margin: "0 10px" }} /></Link>
                          <ul className="dropdown-menus p-0 bg-white" style={{ display: `${this.state.showDropdown ? 'block' : 'none'}` }} onClick={this.setDropdown}>
                            <li> <a href="#1"><span className="pull-left w-20"><i className="fa fa-area-chart" aria-hidden="true" /></span>
                              {/* <span className=" pull-left text-left" onClick={() => this.props.history.push('myportal')}>My Portal</span> */}
                              <Link className="pull-left text-left" to="#">My Portal</Link>
                            </a>
                            </li>
                            {/* <li><Link to="#" >MCRP part 1</Link></li>
                                                    <li><Link to="#" >Medical Final</Link></li>
                                                    <li><Link to="#" >MCRP part 2</Link></li> */}
                            {this.computeCourses()}
                            <li> <a href="#"><span className="pull-left w-20"><i className="fa fa-user-circle" aria-hidden="true" /></span>
                              {/* <span className=" pull-left text-left" onClick={() => this.props.history.push('/profile')}>Profile</span>  */}
                              <Link className="pull-left text-left" to="/profile">Profile</Link>
                            </a>
                            </li>
                            <li>
                              <a href="#"> <span className="pull-left w-20"><i className="fa fa-phone" aria-hidden="true" /></span>
                                {/* <span className=" pull-left text-left" onClick={() => this.props.history.push('/contact')}>Contact us</span> */}
                                <Link className="pull-left text-left" to="/contact">Contact us</Link>
                              </a>
                            </li>
                            <li>
                              <a href="#"> <span className="pull-left w-20"><i className="fa fa-power-off" aria-hidden="true" /></span>
                                <span className=" pull-left text-left" onClick={this.onClickLogout}>Logout</span>
                              </a>
                            </li>
                          </ul>
                        </li>
                        :
                        <span>
                          <Link to="/login" className="new-top-btn" onClick={() => this.props.history.push("/login")}>Login</Link>
                          <Link to="/register" className="new-top-btn" onClick={() => this.props.history.push("/register")}>Signup</Link>
                        </span>
                    }
                  </span>
                </ul>
              </div>
            </div>
            {/*right col end*/}
          </div>
        </div>
        {/*container end*/}
      </header>
    );
  }
}

export default withRouter(Navbar);