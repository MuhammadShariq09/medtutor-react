import React, { Component } from 'react';
import Rating from 'react-rating';
import axios from 'axios';
import UserReviews from "./UserReviews";
import { toast } from 'react-toastify';

class Reference extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: '',
      rating: null,
      reviews: [],
      message: '',
      path: '',
      imagePath: ""
    }
  }

  onClickRating = (rate) => {
    this.setState({ rating: rate })
  }

  onEnterComment = (e) => {
    this.setState({ comment: e.target.value });
  }

  onSubmitReview = (e) => {
    e.preventDefault();
    const { rating, comment } = this.state;
    if (comment === '') {
      this.setState({ message: "Comment field is required." });
      return;
    }
    this.setState({ message: "" });

    axios.post(`/courses/${this.props.courseId}/question/${this.props.questionId}/reviews`, { rating, comment }).then(res => {
      this.setState({ comment: '', rating: null })
      this.getAllReviews();
    }).catch(error => {
      console.log("DATA: ", error.response.data);
      error.response.data.errors && Object.values(error.response.data.errors).map((error, index) => {
        toast.error(error[index]);
      });
    });
  }

  getAllReviews = () => {
    let that = this;
    axios.get(`/courses/${this.props.courseId}/question/${this.props.questionId}/reviews`)
      .then(res => that.setState({ reviews: res.data }))
  }

  componentDidMount() {
    this.getAllReviews();
    console.log("this.props.activeQuestion: ", this.props.activeQuestion);
  }

  render() {
    // const { description, videos, rating } = this.props;
    return (
      <div>
        <section className="reference-sec">
          <div className="reference-sec-head">
            <h2>Reference</h2>
          </div>
          <div className="reference-sec-desc">
            <p>{this.props.activeQuestion && this.props.activeQuestion.explanation}</p>
          </div>
          <div className="reference-videos">
            <div className="row">
              {this.props.activeQuestion && this.props.activeQuestion.medias.map((item, index) => {
                if (item.type === "image") {
                  return (<div key={index} className="col-xl-2">
                    <a onClick={() => this.setState({ imagePath: `http://dev74.onlinetestingserver.com/medtutor/storage/media/${item.file_name}` })} data-toggle="modal" href="#myimageModal">
                      <img src={`http://dev74.onlinetestingserver.com/medtutor/storage/media/${item.file_name}` || `${process.env.PUBLIC_URL}/images/ref_video.jpg`} alt="" className="w-100" />
                    </a>
                  </div>)
                } else if (item.type === "video") {
                  return (<div key={index} className="col-xl-2">
                    <a data-toggle="modal" onClick={() => this.setState({ path: `http://dev74.onlinetestingserver.com/medtutor/storage/media/${item.file_name}` })} href="#myVideoModal">
                      <img src={`${process.env.PUBLIC_URL}/images/videoIcon.jpg`} alt="" className="w-100" />
                    </a>
                  </div>)
                } else if (item.type === "file") {
                  return (
                    <div key={index} className="col-xl-2">
                      <a href={`http://dev74.onlinetestingserver.com/medtutor/storage/media/${item.file_name}`}>{`Download`}
                        <img src={`${process.env.PUBLIC_URL}/images/powerpoint.png`} alt="" className="w-100" />
                      </a>
                    </div>)
                }
              })}
            </div>
          </div>
        </section>

        <div className="modal fade" id="myimageModal">
          <div className="modal-dialog">
            <div className="modal-content">
              {/* Modal body */}
              <div className="modal-body" style={{ width: '500px' }}>
                <button type="button" className="close" data-dismiss="modal">×</button>
                <img src={this.state.imagePath || `${process.env.PUBLIC_URL}/images/ref_video.jpg`} alt="" className="w-100" />
              </div>
            </div>
          </div>
        </div>
        <div className="modal" id="myVideoModal">
          <div className="modal-dialog">
            <div className="modal-content">
              {/* Modal body */}
              <div className="modal-body" style={{ width: '500px' }}>
                <button type="button" className="close" data-dismiss="modal">×</button>
                {/* <img src={`${process.env.PUBLIC_URL}/images/sec_vdeo_ref.jpg`} alt="" className="w-100" /> */}
                <iframe src={this.state.path || "http://www.youtube.com/embed/W7qWa52k-nE"}
                  width="450" height="315" frameborder="0" allowfullscreen={false} style={{ margin: "10px" }}>
                </iframe>
              </div>
              {/* Modal footer */}
            </div>
          </div>
        </div>

        <div className="overall-rating my-5">
          <div className="overall-rating-head">
            <h4>Overall Rating</h4>
          </div>
          <div>
            <Rating
              emptySymbol="fa fa-star-o fa-2x"
              fullSymbol="fa fa-star fa-2x"
              fractions={2}
              initialRating={this.state.rating}
              onClick={this.onClickRating}
              style={{ color: "orange" }}
            />
            {
              this.state.rating ?
                <span>{this.state.rating} / 5</span>
                :
                null
            }
          </div>
          <div className="give-a-review">
            <form action method="get" acceptCharset="utf-8">
              <div className="form-group">
                <textarea className="form-control" rows={5} id="comment"
                  placeholder="Give a review"
                  onChange={this.onEnterComment}
                  value={this.state.comment}
                />
                <span className="text-danger">{this.state.message}</span>
              </div>
              <div className="submit-button-sec pull-right">
                <button type="submit" onClick={this.onSubmitReview}>Submit</button>
              </div>
            </form>
          </div>
        </div>
        <UserReviews reviews={this.state.reviews} />
      </div>
    );
  }
}

export default Reference;