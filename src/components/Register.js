import React, { Component } from 'react';
import axios from 'axios';
import Navbar from './Navbar';
import { toast } from 'react-toastify';
import InputMask from 'react-input-mask';
import OwlCarousel from "react-owl-carousel";
import { withRouter } from 'react-router-dom';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayCourses: [],
      responseData: null,

      fullName: '',
      email: '',
      phone: null,
      password: '',
      password_confirmation: '',
      // country: '',
      course: '',
      package: null,
      agreement: false,
      paymentInfo: {
        expMonth: "01",
        expYear: "2020"
      },

      activeFormPart: 1,
      showPasswordMismatch: '',
      showCourseError: '',
      paymentErrors: null,
    }
  }

  onEnterName = (e) => {
    
    this.setState({ fullName: e.target.value });
  }

  onEnterEmail = (e) => {
    
    this.setState({ email: e.target.value });
  }

  onEnterPhone = (e) => {
    // this.setState({
    //   showPasswordMismatch: "phone must be at least 12 characters in length!"
    // });
    this.setState({ phone: e.target.value ,  showPasswordMismatch: '' });
  }

  onEnterPassword = (e) => {
    
    this.setState({
      showPasswordMismatch: '',
      password: e.target.value
    });
  }

  onConfirmPassword = (e) => {
    
    this.setState({ password_confirmation: e.target.value });
  }

  onSelectCountry = (e) => {
    
    this.setState({ country: e.target.value });
  }

  onSelectCourse = (e) => {
    var selectedCourse = e.target.id;
    if (selectedCourse.length) {
      this.setState({
        course: selectedCourse,
        showCourseError: ''
      })
    }
  }

  onSelectPackage = (e) => {
    var myCourse = this.state.responseData.find(course => course.title === this.state.course);
    var myPackage = null;
    if (myCourse) {
      myPackage = myCourse.packages.find(newPackage => newPackage.title === e.target.value);
    }
    var shortPackage = {
      id: myPackage.id,
      name: myPackage.title,
      price: myPackage.amount,
      months: myPackage.duration,
      description: myPackage.description
    }

    this.setState({ package: shortPackage }, () => {
      if (!this.state.course) {
        this.setState({ showCourseError: 'Please select one of these courses.' })
        return;
      }
      if (this.state.course && this.state.package) {
        this.setActiveFormPart();
      }
    })
  }

  onEnterCardNumber = (e) => {
    let paymentInfo = this.state.paymentInfo ? { ...this.state.paymentInfo } : {};
    paymentInfo.cardNumber = e.target.value;
    this.setState({ paymentInfo: paymentInfo })
  }

  onSelectExpiryMonth = (e) => {
    let paymentInfo = this.state.paymentInfo ? { ...this.state.paymentInfo } : {};
    console.log("e.target.value: ",e.target.value);
    paymentInfo.expMonth = e.target.value ? e.target.value : "01" ;
    // e.target.value ?  paymentInfo.expMonth = e.target.value :  paymentInfo.expMonth = 01 ;
    
    if (e.target.value) {
      this.setState({ paymentInfo: paymentInfo });
    }
  }

  onSelectExpiryYear = (e) => {
    let paymentInfo = this.state.paymentInfo ? { ...this.state.paymentInfo } : {};
    paymentInfo.expYear = e.target.value ? e.target.value : "2020";

    if (e.target.value) {
      this.setState({ paymentInfo: paymentInfo });
    }
  }

  onEnterCVC = (e) => {
    let paymentInfo = this.state.paymentInfo ? { ...this.state.paymentInfo } : {};
    paymentInfo.cvc = e.target.value;
    
    this.setState({ paymentInfo: paymentInfo });
  }

  onCheckAgreement = (e) => {
    
    if ((e.target.checked === true || e.target.checked === false)) {
      this.setState({ agreement: e.target.checked });
    }
  }

  onClickFormPart = (e) => {
    
  }

  setActiveFormPart = () => {
    this.setState({ activeFormPart: this.state.activeFormPart + 1 }, () => { window.scrollTo(0, 120) });
  }

  getExpiryMonths = () => {
    var months = [];
    for (let i = 0; i < 12; i++) {
      months.push(<option key={i + 1}>{i + 1 < 10 ? '0'.concat(i + 1) : i + 1}</option>)
    }
    return months;
  }

  getExpiryYears = () => {
    var years = [];
    const currentYear = new Date().getFullYear();
    for (let i = currentYear; i < (currentYear + 6); i++) {
      years.push(<option key={i}>{i}</option>);
    }
    return years;
  }

  onSubmitPart = (e) => {
    e.preventDefault();
    if (this.state.activeFormPart === 1) {
      if ((this.state.password.length < 6)) {
        this.setState({
          password: '',
          password_confirmation: '',
          showPasswordMismatch: "Passwords must be at least 6 characters in length!"
        })
        return;
      }
      else if ((this.state.password !== this.state.password_confirmation)) {
        this.setState({
          password: '',
          password_confirmation: '',
          showPasswordMismatch: "Passwords do not match!"
        })
        return;
      }
      else if (this.state.phone.length < 11 ) {
        this.setState({
          showPasswordMismatch: "phone must be at least 11 characters in length!"
        });
        return;
      }

      else if (this.state.email === '') {
        this.setState({
          email: '',
          showPasswordMismatch: "Email is required!"
        });
        return;
      }
    }
    else if (this.state.activeFormPart === 3) {
      if (!this.state.agreement) {
        this.setState({ paymentErrors: 'You must agree to the T&C' });
        return;
      }
      const paymentInfo = { ...this.state.paymentInfo};
      console.log("paymentInfo: ",paymentInfo);
      if (paymentInfo) {
        if (!paymentInfo.cvc || paymentInfo.cvc.length !== 3) {
          this.setState({ paymentErrors: 'CVC must be 3 digits.' });
          return;
        }
        else if (!paymentInfo.expMonth) {
          this.setState({ paymentErrors: 'Please enter card expiry month' });
          return;
        }
        else if (!paymentInfo.expYear) {
          this.setState({ paymentErrors: 'Please enter card expiry year' });
          return;
        }
        else if (!paymentInfo.cardNumber) {
          this.setState({ paymentErrors: 'Please enter valid card number' });
          return;
        }
        else if (paymentInfo.cardNumber.length < 16) {
          this.setState({ paymentErrors: 'Card number should be 16 digits.' });
          return;
        }
      }
    }
    this.setActiveFormPart();
  }

  onSubmitRegistrationForm = (e) => {
    let that = this;
    e.preventDefault();
    console.log("this.state.paymentInfo: ",this.state.paymentInfo);
    let registrationData = {
      email: this.state.email,
      password: this.state.password,
      password_confirmation: this.state.password_confirmation,
      name: this.state.fullName,
      package_id: this.state.package && this.state.package.id,
      card_number: this.state.paymentInfo.cardNumber.replace(/-/g, ""),
      exp_month: this.state.paymentInfo.expMonth,
      exp_year: this.state.paymentInfo.expYear,
      cvc: this.state.paymentInfo.cvc,
      holder_name: this.state.fullName
    }

    axios.post('/register', registrationData)
      .then(res => {
        
        if (res.data.token) {
          toast.success("Your Account has been Successfully Registered");
          that.props.history.push('/login');
        }
      })
      .catch(error => {
        error.response.data.errors && Object.values(error.response.data.errors).map((error, index) => {
          toast.error(error[index]); 
      });
        
        // const data = error.response.data.errors;
        // let keys = Object.keys(error.response.data.errors);
        // toast.error(data[keys[0]][0]);
      });
  }

  getCourses = async () => {
    var response = null;
    let displayCourses = await axios.get('/courses')
      .then(res => {
        response = res.data;
        return res.data.map((course, index) => {
          if ((index % 3) === 0) {
            return (
              <div key={index} className="col-lg-4 col-md-4 col-sm-4 col-12 text-sm-center text-lg-left text-md-left text-left">
                <label className="radio-container" id={course.title} onClick={this.onSelectCourse}>{course.title}
                  <input type="radio" name="radio" id={course.title} />
                  <span className="radio-checkmark" />
                </label>
              </div>
            )
          }
          else if ((index % 3) === 1) {
            return (
              <div key={index} className="col-lg-4 col-md-4 col-sm-4 col-12 text-sm-center  text-md-center text-left">
                <div className="form-check-inline">
                  <label className="radio-container" id={course.title} onClick={this.onSelectCourse}>{course.title}
                    <input type="radio" name="radio" id={course.title} />
                    <span className="radio-checkmark" />
                  </label>
                </div>
              </div>
            )
          }
          else if ((index % 3) === 2) {
            return (
              <div key={index} className="col-lg-4 col-md-4 col-sm-4  text-sm-center text-md-right text-left">
                <div className="form-check-inline">
                  <label className="radio-container" id={course.title} onClick={this.onSelectCourse}>{course.title}
                    <input type="radio" name="radio" id={course.title} />
                    <span className="radio-checkmark" />
                  </label>
                </div>
              </div>
            )
          }
        })
      })

    this.setState({ responseData: response, displayCourses: await displayCourses })
  }

  getPackages = () => {
    if (this.state.course.length) {
      return this.state.responseData.find(newCourse => newCourse.title === this.state.course).packages.map((newPackage, index) => {
        // return course.packages.map((newPackage, index) => {
        const Points = newPackage.description.split('\n').map((point, i) => {
          return (
            <li key={i} className="d-block py-2">
              <div className="row">
                <div className="col-lg-2 col-md-2 col-2 text-center pr-0 text-center pr-0">
                  <span className="check-icon w-20"><i className="fa fa-check" /></span>
                </div>
                <div className="col-lg-10 col-md-10 col-10 p-0">
                  <span >{point}</span>
                </div>
              </div>
            </li>
          )
        });
        return (
          <div key={index} className="col-lg-4 col-md-6 my-4 d-flex">
            <div className="card border-0 profile-card">
              <div className="card-header header-card">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-6">
                    <div className="price-sec">
                      <ul className="list-unstyled">
                        <li>{newPackage.title}</li>
                        <li>
                          <h2 className="m-0" style={{ fontSize: '1.6rem' }}>£{newPackage.amount}</h2>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-6">
                    <div className="package-duration-sec my-4 text-center">
                      <button className="font-weight-bold">{newPackage.duration} Months</button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="details-of-membership-card">
                  <ul className="list-unstyled">
                    {Points}
                  </ul>
                </div>
              </div>
              <div className="card-footer footer-card">
                <div className="card-select-button text-center">
                  <button className="text-white font-weight-bold" value={newPackage.title} onClick={this.onSelectPackage}>Select</button>
                </div>
              </div>
            </div>
          </div>
        )
        // })
      })
    }
    return null;
  }

  getPackagePoints = (myPackage) => {
    
    return myPackage.description.split('\n').map((point, i) => {
      return <li key={i} className="d-block py-2">
        <div className="row">
          <div className="col-lg-2 col-md-2 col-2 text-center pr-0 text-center pr-0">
            <span className="check-icon w-20"><i className="fa fa-check" /></span>
          </div>
          <div className="col-lg-10 col-md-10 col-10 p-0">
            <span >{point}</span>
          </div>
        </div>
      </li>
    })
  }

  componentDidMount() {
    this.getCourses();
  }

  render() {
    return (
      // <div className="profile-page">
      <div>
        {/* <Navbar /> */}
        {/* header-end */}
        {/* registration-sec-start */}
        <section className="registration-sec">
          <div className="container">
            <div className="registration-main-head text-center">
              <h1 className="text-white">Registration</h1>
            </div>
            <div className="card border-0 registration-main-part">
              <div className="registration-main-div">
                <div className="card-body">
                  <div className="registration-main-progress-bar">
                    <div className="add-your-personal-info">
                      {/* Circles which indicates the steps of the form: */}
                      <div className="row">
                        <div className="col-12">
                          {/*steps start here*/}
                          <div className="steps">
                            <ul className="nav nav-tabs border-bottom-0" id="myTab" role="tablist">

                              <li className="nav-item" id="1" disabled style={{ cursor: "pointer" }} onClick={() => this.setState({ activeFormPart: 1 })}>
                                <a className="nav-link border-0" id="1 home-tab">
                                  <div className={`step-ciricle ${this.state.activeFormPart >= 1 ? 'active' : null}`} id="1">
                                    <span className="border-bottom-0" id="1">1</span>
                                  </div>
                                  <p id="1" disabled>Personal Info</p>
                                </a>
                              </li>

                              <li className="nav-item" id="2" style={{ cursor: "pointer" }} onClick={() => this.setState({ activeFormPart: 2}) }>
                                <a className="nav-link border-0" id="2 home-tab">
                                  <div className={`step-ciricle ${this.state.activeFormPart >= 2 ? 'active' : null}`} id="2"><span id="2" >2</span></div>
                                  <p id="2">Select Package</p>
                                </a>
                              </li>
                              <li className="nav-item" id="3" style={{ cursor: "pointer" }} onClick={() => this.setState({ activeFormPart: 3}) }>
                                <a className="nav-link border-0" id="3 payment-info-tab">
                                  <div className={`step-ciricle ${this.state.activeFormPart >= 3 ? 'active' : null}`} id="3"><span id="3" >3</span>
                                  </div>
                                  <p id="3">Payment Info</p>
                                </a>
                              </li>
                              <li className="nav-item" id="4" onClick={this.onClickFormPart}>
                                <a className="nav-link border-0" id="4 home-tab">
                                  <div className={`step-ciricle ${this.state.activeFormPart >= 4 ? 'active' : null}`} id="4"><span id="4" >4</span></div>
                                  <p id="4">Review info</p>
                                </a>
                              </li>

                            </ul>
                          </div>
                          {/*steps start here*/}
                        </div>
                      </div>
                    </div>
                    <div className="tab-content" id="myTabContent">
                      {
                        this.state.activeFormPart === 1 ?
                          <div className="tab-pane fade show active add-your-personal-info-sec" role="tabpanel" aria-labelledby="home-tab" id="step1">
                            <div className="add-your-personal-info-head text-center">
                              <h3 className="font-weight-bold">Add your Personal Info</h3>
                            </div>
                            <div className="add-your-personal-info-use">
                              <form action="" onSubmit={this.onSubmitPart} method="get" acceptCharset="utf-8">
                                <ul className="list-unstyled">
                                  <li className="pt-3">
                                    <label>Full Name</label>
                                    <input type="text" value={this.state.fullName} required placeholder="Enter Name" className="custom-input-box w-100" onChange={this.onEnterName} />
                                  </li>
                                  <li className="pt-3">
                                    <label>Email</label>
                                    <input type="email" value={this.state.email} placeholder="Enter Email" className="custom-input-box w-100" onChange={this.onEnterEmail} required />
                                  </li>
                                  <li className="pt-3">
                                    <label>Phone</label>
                                    <input type="number" value={this.state.phone} required placeholder="Enter Phone Number" className="custom-input-box w-100" onChange={this.onEnterPhone} />
                                  </li>
                                  <li className="pt-3">
                                    <label>Password</label>
                                    <input type="password" value={this.state.password} required className="custom-input-box w-100" value={this.state.password} onChange={this.onEnterPassword} />
                                  </li>
                                  <li className="pt-3">
                                    <label>Confirm Password</label>
                                    <input type="password" value={this.state.password_confirmation} required className="custom-input-box w-100" value={this.state.password_confirmation} onChange={this.onConfirmPassword} />
                                    {this.state.showPasswordMismatch.length ?
                                      <p>
                                        {this.state.showPasswordMismatch}
                                      </p>
                                      :
                                      null
                                    }
                                  </li>
                                  {/* <li className="pt-3">
                                                                        <label htmlFor="country">Country</label>
                                                                        <select className="form-control" id="sel1" onChange={this.onSelectCountry}>
                                                                            <option>1</option>
                                                                            <option>2</option>
                                                                            <option>3</option>
                                                                            <option>4</option>
                                                                        </select>
                                                                    </li> */}
                                </ul>
                                {/* continue-button-sec-start */}
                                <div className="submit-button-sec py-3">
                                  <button className="custom-theme-button-style text-white font-weight-bold w-100" type="submit">Continue</button>
                                </div>
                                {/* continue-button-sec-end  */}
                              </form>
                            </div>
                          </div>
                          :
                          null
                      }
                      {
                        this.state.activeFormPart === 2 ?
                          <div className="tab-pane fade show active" role="tabpanel" aria-labelledby="home-tab" id="step2">
                            <div className="add-your-personal-info">
                              <div className="add-your-personal-info-head text-center">
                                <h3 className="font-weight-bold">Choose Membership Package</h3>
                              </div>
                              <div className="select-package-sec">
                                <div className="select-package-div text-center">
                                  <p>Select your course First then Select Your Package</p>
                                </div>
                                <div className="checkboxes-for-select-packages">
                                  <div className="row">
                                    {this.state.displayCourses}
                                  </div>
                                  {
                                    this.state.showCourseError.length ?
                                      <p>
                                        {this.state.showCourseError}
                                      </p>
                                      :
                                      null
                                  }
                                </div>

                                <div className="select-member-ship-card">
                                {/* <OwlCarousel items={4} id="buy-slider-sfa" className="corona-thumb-slide" loop
                                      margin={10}
                                      autoplay={true}
                                    > */}
                                  <div className="row my-5">
                                    
                                      {this.getPackages()}
                                    
                                  </div>
                                  {/* </OwlCarousel> */}
                                </div>

                              </div>
                              {/* select-package-sec-end */}
                            </div>
                          </div>
                          :
                          null
                      }
                      {
                        this.state.activeFormPart === 3 ?
                          <div className="tab-pane fade show active" role="tabpanel" aria-labelledby="payment-info-tab" id="step3">
                            <div className="add-your-payment-info">
                              <div className="add-your-personal-info-head text-center">
                                <h3 className="font-weight-bold">Add your Payment Info</h3>
                              </div>
                              <div className="add-your-payment-info">
                                <form action="" onSubmit={this.onSubmitPart} method="get" acceptCharset="utf-8">
                                  <ul className="list-unstyled">
                                    {/* <li className="pt-3">
                                                                            <label>Card Holder Name <span className="text-danger">*</span></label>
                                                                            <input type="text" defaultValue placeholder="Enter Your Card Name" className="custom-input-box w-100" required />
                                                                        </li> */}
                                    <li className="pt-3">
                                      <label>Card Holder Number <span className="text-danger">*</span></label>
                                      <InputMask mask="9999-9999-9999-9999" value={this.state.cardNumber} onChange={this.onEnterCardNumber}>
                                        {(inputProps) => <input {...inputProps} type="text" className="custom-input-box w-100" required />}
                                      </InputMask>
                                      {/* <input type="number" defaultValue
                                        placeholder="Enter Card Number"
                                        className="custom-input-box w-100"
                                        onChange={this.onEnterCardNumber}
                                        required
                                      /> */}
                                      <div className="social-card-image">
                                        <i className="fa fa-cc-visa" aria-hidden="true" />
                                        <i className="fa fa-cc-mastercard" aria-hidden="true" />
                                      </div>
                                    </li>
                                    <li className="pt-3">
                                      <div className="row">
                                        <div className="col-lg-8 col-xl-8">
                                          <div className="row expiry-date-sec">
                                            <div className="col-lg-4 col-xl-6">
                                              <label>Expiry Date <span className="text-danger">*</span></label>
                                              <select className="form-control" id="sel1" required onChange={this.onSelectExpiryMonth}>
                                                {this.getExpiryMonths()}
                                              </select>
                                            </div>
                                            <div className="col-lg-4 col-xl-6">
                                              <div className="year-sec">
                                                <select className="form-control" id="sel1" required onChange={this.onSelectExpiryYear}>
                                                  {this.getExpiryYears()}
                                                </select>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="col-lg-4 col-xl-4">
                                          <label>CW <span className="text-danger">*</span></label>
                                          <InputMask mask="999" onChange={this.onEnterCVC}>
                                        {(inputProps) => <input {...inputProps} type="text" className="custom-input-box w-100" required />}
                                      </InputMask>
                                          {/* <input type="number" placeholder="" className="cw-custom-input-box w-100" required onChange={this.onEnterCVC} /> */}
                                        </div>
                                      </div>
                                    </li>
                                    <li>
                                      <div className="row">
                                        <div className="col-lg-12">
                                          <div className="form-check-inline py-4">
                                            <label className="check-box-container" onClick={this.onCheckAgreement}>I have read and accept the term of use, rules of flight and privacy policy
                                                <input type="checkbox" required />
                                              <span className="checkbox-checkmark" />
                                            </label>
                                          </div>
                                        </div>
                                      </div>
                                    </li>
                                  </ul>
                                  {/* continue-button-sec-start */}
                                  <p>{this.state.paymentErrors}</p>
                                  <div className="submit-button-sec mt-5">
                                    <button className="custom-theme-button-style text-white font-weight-bold w-100" type="submit">Review Details</button>
                                  </div>
                                  {/* continue-button-sec-end  */}
                                </form>
                              </div>
                            </div>
                          </div>
                          :
                          null
                      }
                      {
                        this.state.activeFormPart === 4 ?
                          <div className="tab-pane fade show active" role="tabpanel" aria-labelledby="home-tab" id="step4">
                            <div className="review-your-registration-info">
                              <div className="review-your-registration-head text-center">
                                <h3 className="font-weight-bold">Review Your Info</h3>
                              </div>
                              <div className="review-your-registration-info">
                                <div className="personal-info-sec">
                                  <div className="personal-info-head border-bottom">
                                    <h2 className="mb-0">Personal Info</h2>
                                    <hr />
                                  </div>
                                  <div className="personal-info-details">
                                    <div className="personal-info-name pt-4">
                                      <ul className="list-unstyled">
                                        <li>Full Name</li>
                                        <li className="font-weight-bold">{this.state.fullName}</li>
                                      </ul>
                                    </div>
                                    <div className="personal-info-mail">
                                      <ul className="list-unstyled">
                                        <li>Email</li>
                                        <li className="font-weight-bold">{this.state.email}</li>
                                      </ul>
                                    </div>
                                    <div className="personal-info-phone-details">
                                      <ul className="list-unstyled">
                                        <li>Phone</li>
                                        <li className="font-weight-bold">{this.state.phone}</li>
                                      </ul>
                                    </div>
                                    {/* <div className="personal-info-country-details">
                                                                            <ul className="list-unstyled">
                                                                                <li>Country</li>
                                                                                <li className="font-weight-bold">{this.state.country}</li>
                                                                            </ul>
                                                                        </div> */}
                                  </div>
                                </div>
                                {/* personal-info-sec-end */}
                                {/* your-package-sec-start */}
                                <div className="your-package-sec">
                                  <div className="your-package-head border-bottom">
                                    <h2 className="mb-0">Your Package</h2>
                                    <hr />
                                  </div>
                                  <div className="your-package-details pt-4">
                                    <div className="row">
                                      <div className="col-xl-6">
                                        <div className="row">
                                          <div className="col-xl-4 col-md-4 col-6">
                                            <div className="price-sec">
                                              <ul className="list-unstyled">
                                                <li>{this.state.package && this.state.package.name}</li>
                                                <li>
                                                  <h2 className="m-0" style={{ fontSize: '1.6rem' }}>£{this.state.package && this.state.package.price}</h2>
                                                </li>
                                              </ul>
                                            </div>
                                          </div>
                                          <div className="col-xl-5 col-md-5 col-6">
                                            <div className="package-duration-sec my-4 text-center">
                                              <button className="font-weight-bold">{this.state.package && this.state.package.months} Months</button>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="details-of-membership-card">
                                          <ul className="list-unstyled">
                                            {this.state.package && this.getPackagePoints(this.state.package)}
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                {/* your-package-sec-end */}
                                {/* payment-details-sec-start */}
                                <div className="payment-details-sec">
                                  <div className="payment-details-head border-bottom">
                                    <h2 >Payment Details</h2>
                                    <hr />
                                  </div>
                                  <div className="payment-details-details">
                                    {/* <div className="payment-details-name pt-4">
                                                                            <ul className="list-unstyled">
                                                                                <li>Card Holder Name*</li>
                                                                                <li className="font-weight-bold">{this.state.paymentInfo.}</li>
                                                                            </ul>
                                                                        </div> */}
                                    <div className="payment-details-mail">
                                      <ul className="list-unstyled">
                                        <li>Card Holder Number*</li>
                                        <li className="font-weight-bold">{this.state.paymentInfo.cardNumber}</li>
                                      </ul>
                                    </div>
                                    <div className="payment-details-phone-details">
                                      <ul className="list-unstyled">
                                        <li>Expiry Date *</li>
                                        <li className="font-weight-bold">{this.state.paymentInfo.expMonth}, {this.state.paymentInfo.expYear}</li>
                                      </ul>
                                    </div>
                                    <div className="payment-details-country-details">
                                      <ul className="list-unstyled">
                                        <li>CWW*</li>
                                        <li className="font-weight-bold">{this.state.paymentInfo.cvc}</li>
                                        <li>
                                          <div className="form-check-inline">
                                            <label className="check-box-container">I have read and accept the term of use,rules of flight and privacy policy
                                                                                            <input type="checkbox" defaultChecked={this.state.agreement} />
                                              <span className="checkbox-checkmark" />
                                            </label>
                                          </div>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                                {/* personal-info-sec-end */}
                                {/* payment-details-sec-end */}
                              </div>
                              {/* Register-button-sec-start */}
                              <div className="Register-button-sec">
                                <button className="custom-theme-button-style text-white font-weight-bold w-100" onClick={this.onSubmitRegistrationForm}>Register</button>
                              </div>
                              {/* Register-button-sec-end  */}
                            </div>
                          </div>
                          :
                          null
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* registration-sec-end */}
        {/* modal-new-memorizer-start*/}
        {/* The Modal */}
        <div className="modal" id="myModal">
          <div className="modal-dialog">
            <div className="modal-content">
              {/* Modal Header */}
              <div className="modal-header">
                <h4 className="modal-title">Modal Heading</h4>
                <button type="button" className="close" data-dismiss="modal">×</button>
              </div>
              {/* Modal body */}
              <div className="modal-body">
                Modal body..
                            </div>
              {/* Modal footer */}
              <div className="modal-footer">
                <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Register);