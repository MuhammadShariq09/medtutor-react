import React, { Component } from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom'
import { toast } from 'react-toastify';
import InputMask from 'react-input-mask';

class RegisterCourse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      course: '',
      displayCourses: [],
      responseData: null,
      package: null,
      showCourseError: '',
      name: "",
      card_number: "",
      cvc: "",
      expiry_date: "",
      courceId: "",
      shortPackage: "",
    }
  }

  onSelectCourse = (e) => {
    var selectedCourse = e.target.id;
    if (selectedCourse.length) {
      this.setState({
        course: selectedCourse,
        showCourseError: ''
      })
    }
  }

  getCourses = async () => {
    var response = null;
    let displayCourses = await axios.get('/courses')
      .then(res => {
        response = res.data;
        return res.data.map((course, index) => {
          if ((index % 3) === 0) {
            return (
              <div key={index} className="col-lg-4 col-md-4 col-sm-4 col-12 text-sm-center text-lg-left text-md-left text-left">
                <label className="radio-container" id={course.title} onClick={this.onSelectCourse}>{course.title}
                  <input type="radio" name="radio" id={course.title} />
                  <span className="radio-checkmark" />
                </label>
              </div>
            )
          }
          else if ((index % 3) === 1) {
            return (
              <div key={index} className="col-lg-4 col-md-4 col-sm-4 col-12 text-sm-center  text-md-center text-left">
                <div className="form-check-inline">
                  <label className="radio-container" id={course.title} onClick={this.onSelectCourse}>{course.title}
                    <input type="radio" name="radio" id={course.title} />
                    <span className="radio-checkmark" />
                  </label>
                </div>
              </div>
            )
          }
          else if ((index % 3) === 2) {
            return (
              <div key={index} className="col-lg-4 col-md-4 col-sm-4  text-sm-center text-md-right text-left">
                <div className="form-check-inline">
                  <label className="radio-container" id={course.title} onClick={this.onSelectCourse}>{course.title}
                    <input type="radio" name="radio" id={course.title} />
                    <span className="radio-checkmark" />
                  </label>
                </div>
              </div>
            )
          }
        })
      })

    this.setState({ responseData: response, displayCourses: await displayCourses })
  }

  getPackages = () => {
    if (this.state.course.length) {
      return this.state.responseData.find(newCourse => newCourse.title === this.state.course).packages.map((newPackage, index) => {
        // return course.packages.map((newPackage, index) => {
        const Points = newPackage.description.split('\n').map((point, i) => {
          return (
            <li key={i} className="d-block py-2">
              <div className="row">
                <div className="col-lg-2 col-md-2 col-2 text-center pr-0 text-center pr-0">
                  <span className="check-icon w-20"><i className="fa fa-check" /></span>
                </div>
                <div className="col-lg-10 col-md-10 col-10 p-0">
                  <span >{point}</span>
                </div>
              </div>
            </li>
          )
        });
        return (
          <div key={index} className="col-lg-4 col-md-6 my-4 d-flex">
            <div className="card border-0 profile-card">
              <div className="card-header header-card">
                <div className="row">
                  <div className="col-lg-6 col-md-6 col-6">
                    <div className="price-sec">
                      <ul className="list-unstyled">
                        <li>{newPackage.title}</li>
                        <li>
                          <h2 className="m-0" style={{ fontSize: '1.6rem' }}>£{newPackage.amount}</h2>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-6">
                    <div className="package-duration-sec my-4 text-center">
                      <button className="font-weight-bold">{newPackage.duration} Months</button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="details-of-membership-card">
                  <ul className="list-unstyled">
                    {Points}
                  </ul>
                </div>
              </div>
              <div className="card-footer footer-card">
                <div className="card-select-button text-center">
                  <button className="text-white font-weight-bold" value={newPackage.title} onClick={this.onSelectPackage}>Select</button>
                </div>
              </div>
            </div>
          </div>
        )
        // })
      })
    }
    return null;
  }
  onSelectPackage = (e) => {
    if(!localStorage.getItem('token')){
        toast.warn("Please login to subscribe in the course");
        this.props.history.push("/login");
        return;
    }
    var myCourse = this.state.responseData.find(course => course.title === this.state.course);
    var myPackage = null;
    if (myCourse) {
      myPackage = myCourse.packages.find(newPackage => newPackage.title === e.target.value);
    }
    var shortPackage = {
      package_id: myPackage.id,
    }
    this.setState({ package: shortPackage }, () => {
      if (!this.state.course) {
        this.setState({ showCourseError: 'Please select one of these courses.' })
        return;
      }
      if (this.state.course && this.state.package) {
        this.setState({
          courceId: myCourse.id,
          shortPackage: shortPackage
        });
        window.$("#myNewMemoModal").modal("show");
        // this.subscribePackage(myCourse.id, shortPackage);
      }
    })
  }

  onSubscribePkg = (e) => {
    e.preventDefault();
    const number = this.state.card_number.replaceAll("-", "");
    const eDate = this.state.expiry_date.split("/");
    axios.post(`courses/${this.state.courseId}/subscribe`,
      {
        ...this.state.shortPackage,
        card_number: number,
        exp_month: eDate[0],
        exp_year: eDate[1],
        cvc: this.state.cvc
      })
      .then(res => {
        console.log(res);
        if (res.data.message) {
          window.$("#myNewMemoModal").modal("hide");
          // toast.success(res.data.message);
          this.props.history.push('/profile')
          // setTimeout(() => {
          window.location.reload();
          // }, 3000);

        }
      })
      .catch(error => {
        console.log(error, error.response);
        error.response.data.errors ? Object.values(error.response.data.errors).map((error, index) => {
          toast.error(error || error[index]);
        }) : toast.error(error.response.data.message);
      })
  }

  subscribePackage = (courseId, myPackage) => {
    // axios hit here
    axios.post(`courses/${courseId}/subscribe`,
      {
        ...myPackage,
        card_number: "4242424242424242",
        exp_month: "12",
        exp_year: "2021",
        cvc: "123"
      })
      .then(res => {
        if (res.data.message) {
          this.props.history.push('/profile')
          window.location.reload();
        }
      })
      .catch(err => toast.error("Something went wrong."))
  }

  componentDidMount() {
    this.getCourses();
  }

  render() {
    return (
      <section className="registration-sec" style={{ marginTop: "70px" }}>
        <div className="container">
          <div className="card border-0 registration-main-part">
            <div className="registration-main-div">
              <div className="card-body">
                <div className="tab-content" id="myTabContent">
                  <div className="tab-pane fade show active" role="tabpanel" aria-labelledby="home-tab" id="step2">
                    <div className="add-your-personal-info">
                      <div className="add-your-personal-info-head text-center">
                        <h3 className="font-weight-bold">Choose Membership Package</h3>
                      </div>
                      <div className="select-package-sec">
                        <div className="select-package-div text-center">
                          <p>Select your course First then Select Your Package</p>
                        </div>
                        <div className="checkboxes-for-select-packages">
                          <div className="row" style={{ marginTop: "50px" }}>
                            {this.state.displayCourses}
                          </div>
                          {
                            this.state.showCourseError.length ?
                              <p>
                                {this.state.showCourseError}
                              </p>
                              :
                              null
                          }
                        </div>
                        <div className="select-member-ship-card">
                          <div className="row my-5">
                            {this.getPackages()}
                          </div>
                        </div>
                      </div>
                      {/* select-package-sec-end */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* PAYMENT MODAL START */}
        <div className="modal" id="myNewMemoModal">
          <div className="modal-dialog">
            <div className="modal-content">
              {/* Modal body */}
              <form onSubmit={this.onSubscribePkg}>
                <div className="modal-body text-center new-memorizer">
                  <img src={`${process.env.PUBLIC_URL}/images/new-memorizer.png`} alt="" />
                  <h2>Payment</h2>
                  <input type="text" className="w-100 custom-input-box" placeholder="Enter Name" onChange={(e) => this.setState({ newMemoName: e.target.value })} required />
                  <InputMask mask="9999-9999-9999-9999" value={this.state.card_number} onChange={(e) => this.setState({ card_number: e.target.value })}>
                    {(inputProps) => <input type="text" {...inputProps} className="w-100 custom-input-box" placeholder="Enter Card Number" required />}
                  </InputMask>
                  {/* <input type="text" className="w-100 custom-input-box" placeholder="Enter Card Number" onChange={(e) => this.setState({ newMemoName: e.target.value })} required /> */}

                  {/* <input type="text" className="w-100 custom-input-box" placeholder="Enter CVV" onChange={(e) => this.setState({ newMemoName: e.target.value })} required /> */}
                  <InputMask mask="999" value={this.state.cvc} onChange={(e) => this.setState({ cvc: e.target.value })}>
                    {(inputProps) => <input type="text" {...inputProps} className="w-100 custom-input-box" placeholder="Enter CVV" required />}
                  </InputMask>
                  {/* <input type="text" className="w-100 custom-input-box" placeholder="Enter Expiry" onChange={(e) => this.setState({ newMemoName: e.target.value })} required /> */}
                  <InputMask mask="99/9999" value={this.state.expiry_date} onChange={(e) => this.setState({ expiry_date: e.target.value })}>
                    {(inputProps) => <input type="text" {...inputProps} className="w-100 custom-input-box" placeholder="Enter Expiry" required />}
                  </InputMask>
                  <div className="submit-button-sec my-2">
                    <button type="submit" className="w-100 py-3 custom-theme-button-style text-white font-weight-bold">Subscribe</button>
                  </div>
                </div>
              </form>
              {/* Modal footer */}
            </div>
          </div>
        </div>
        {/* PAYMENT MODAL END */}
      </section>
    );
  }
}

export default withRouter(RegisterCourse);