import React from 'react';
import { withRouter } from 'react-router-dom';

const Notfound = (props) => {
  return (
    <div>
      <section className="contact-us-section">
        <div className="container">
          <div className="card border-0 edit-profile-card ">
            <div className="main-sec">
              <img src={`${process.env.PUBLIC_URL}/images/404-notfound.png`} alt="404" style={{ width: "100%" , cursor: "pointer" }} onClick={ () => props.history.push('/')} />
              {/* () => window.location.href = window.location.origin */}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default withRouter(Notfound);