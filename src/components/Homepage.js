import React, { Component } from 'react';
import ReactWOW from 'react-wow'
import axios from 'axios';
import Navbar from './Navbar';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class Homepage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newsLetterEmail: ''
    }
  }

  onShowToast = (content, type) => {
    let that = this;
    if (type === 'success') {
      return toast.success(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
      });
    }
    else if (type === 'info') {
      return toast.info(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
      });
    }
    else if (type === 'warning') {
      return toast.warning(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
      });
    }
    else if (type === 'danger') {
      return toast.danger(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
      });
    }
    else {
      return toast.default(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
      });
    }
  }

  onSubscribe = () => {
    let that = this;
    if (this.state.newsLetterEmail.length) {
      axios.post('/newsletters', {
        email: this.state.newsLetterEmail
      })
        .then(res => {
          if (res.data.email) {
            that.onShowToast('Successfully subscribed to newsletters!', 'success');
          }
        })
        .catch(err => { that.onShowToast('Error occured while subscribing to newsletters.', 'warning') })
    }
  }

  componentDidMount() {
    window.scrollTo(0,0);
    let currentUser = localStorage.getItem('user');

    if (currentUser) {
      this.setState({ user: currentUser })
    }
    else {
      if (localStorage.getItem('token')) {
        axios.get('/users/profile', { headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') } })
          .then(resp => {
            
            localStorage.setItem('username', resp.data.name);
            localStorage.setItem('userimage', resp.data.image);
            // this.props.history.push('/');
          })
          .catch(err => toast.error("User profile fetch error."));
      }
    }
  }

  render() {
    return (
      <div>
        <div className="main-top-banner clearfix">
          <Navbar history={this.props.history} />
          <div className="container">
            <div className="row banner-caption">
              <ReactWOW animation='fadeInDown' duration="1.5s" delay="0.9s">
                <div className="col-lg-7 col-md-12 col-sm-12">
                  <h1>Innovative <i>Learning</i><span>Can Change Your World</span> </h1>
                  <h5>Over 5000+ MCQs, EMQ &amp; VSAQs available</h5>
                  <Link to="/registercourse" className="button-main">View Courses <i className="fa fa-chevron-circle-right" /></Link>
                </div>
              </ReactWOW>
              <div className="col-lg-5 col-md-12 col-sm-12">
                <img src={`${process.env.PUBLIC_URL}/images/new-images_07.png`} className="main-top-banner-right-img" alt="" />
              </div>
            </div>
            <div className="row">
              <div className="col-md-6 col-sm-12">
                <div className="row">
                  <ReactWOW animation='fadeInDown' duration="2.5s" delay="1s">
                    <div className="col-md-6 col-sm-12">
                      <div className="banner-boxs">
                        <img src={`${process.env.PUBLIC_URL}/images/Medtutor-1.0_10.jpg`} className="img-fluid" alt="" />
                        <h6>Gain Success in Medical Exams</h6>
                      </div>
                      <div className="banner-boxs">
                        <img src={`${process.env.PUBLIC_URL}/images/Medtutor-1.0_18.jpg`} className="img-fluid" alt="" />
                        <h6>Effortlessly memorise
                        with Spaced Repetition
                                              Learning</h6>
                      </div>
                    </div>
                  </ReactWOW>
                  <ReactWOW animation='fadeInDown' duration="4.5s" delay="1s">
                    <div className="col-md-6 col-sm-12 VSAQs">
                      <div className="banner-boxs">
                        <img src={`${process.env.PUBLIC_URL}/images/Medtutor-1.0_14.jpg`} className="img-fluid" alt="" />
                        <h6>Over 5000 MCQs,
                                              EMQ &amp; VSAQs available!</h6>
                      </div>
                      <div className="banner-boxs">
                        <img src={`${process.env.PUBLIC_URL}/images/Medtutor-1.0_20.jpg`} className="img-fluid" alt="" />
                        <h6>Monitor
                                              Your Progress</h6>
                      </div>
                    </div>
                  </ReactWOW>
                </div>
              </div>
              <div className="col-md-6 col-sm-12">
                <ReactWOW animation='fadeInDown' duration="0.3s" delay="0.9s">
                  <div className="banner-bottom-right">
                    <h1>Study Any Time,  Anywhere</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                    commodo consequat. Duis aute irure dolor in
                                      reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                    <Link to="/aboutus" className="button-main">Learn More <i className="fa fa-chevron-circle-right" /></Link>
                  </div>
                </ReactWOW>
              </div>
            </div>
          </div>
        </div>
        <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
          <section className="chose-sec">
            <div className="container">
              <h1>Choose Your Exam</h1>
              <p>Our revision resources ensure you have the best chance of passing your exam.</p>
              <div className="chose-srch">
                <input type="search" placeholder="Find My Exams" />
                <button> <i className="fa fa-search" aria-hidden="true" /></button>
              </div>
            </div>
          </section>
        </ReactWOW>
        <section className="popular-sec-centr MRCP">
          <div className="container">
            <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
              <h1 className="text-center">Popular Revision Resources</h1>
            </ReactWOW>
            <ReactWOW animation='fadeInDown' duration="1.5s" delay="0.9s">
              <p className="p-new">Select from our range of exam preparation question banks.</p>
            </ReactWOW>
            <div className="row">
              <div className="col-md-4 col-sm-12">
                <div className="popular-sec-top-boxs">
                  <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
                    <img src={`${process.env.PUBLIC_URL}/images/home_04.png`} className="img-full" alt="" />
                  </ReactWOW>
                  <div className="popular-sec-top-boxs-inner">
                    <ReactWOW animation='fadeInDown' duration="3.5s" delay="1s">
                      <img src={`${process.env.PUBLIC_URL}/images/new-images_11.png`} className="iconss" alt="" />
                    </ReactWOW>
                    <a href="#">MRCP Part 1 <i className="fa fa-chevron-circle-right" aria-hidden="true" /></a>
                  </div>
                </div>
              </div>{/*col end*/}
              <div className="col-md-4 col-sm-12">
                <div className="popular-sec-top-boxs">
                  <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
                    <img src={`${process.env.PUBLIC_URL}/images/home_06.png`} className="img-full" alt="" />
                  </ReactWOW>
                  <div className="popular-sec-top-boxs-inner">
                    <ReactWOW animation='fadeInDown' duration="3.5s" delay="1s">
                      <img src={`${process.env.PUBLIC_URL}/images/new-images_13.png`} className="iconss" alt="" />
                    </ReactWOW>
                    <a href="#">MRCP Part 2 <i className="fa fa-chevron-circle-right" aria-hidden="true" /></a>
                  </div>
                </div>
              </div>{/*col end*/}
              <div className="col-md-4 col-sm-12">
                <div className="popular-sec-top-boxs">
                  <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
                    <img src={`${process.env.PUBLIC_URL}/images/home_08.png`} className="img-full" alt="" />
                  </ReactWOW>
                  <div className="popular-sec-top-boxs-inner">
                    <ReactWOW animation='fadeInDown' duration="3.5s" delay="1s">
                      <img src={`${process.env.PUBLIC_URL}/images/new-images_15.png`} className="iconss" alt="" />
                    </ReactWOW>
                    <a href="#">Medical Final <i className="fa fa-chevron-circle-right" aria-hidden="true" /></a>
                  </div>
                </div>
              </div>{/*col end*/}
            </div>{/*row end*/}
          </div>{/*container end*/}
          <div className="container mt-5">
            <div className="row align-items-center">
              <ReactWOW animation='fadeInDown' duration="2s" delay="0.9s">
                <div className="col-md-6 col-sm-12">
                  <h2>Use Our Unique <img src={`${process.env.PUBLIC_URL}/images/new-images_24.png`} className="imgg" alt="" /> to Key Learn Facts Quickly &amp; Retain them Long Term</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                  <a href="#" className="button-main">Learn More <i className="fa fa-chevron-circle-right" /></a>
                </div>
              </ReactWOW>
              <ReactWOW animation='fadeInDown' duration="2s" delay="0.9s">
                <div className="col-md-6 col-sm-12 p-0">
                  <img src={`${process.env.PUBLIC_URL}/images/new-images_21.png`} className="img-full" alt="" />
                </div>
              </ReactWOW>
            </div>{/*row end*/}
            <div className="row mt-5">
              <div className="col-md-3 col-sm-12">
                <div className="box">
                  <div className="popular-sec-bottom-boxs">
                    <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
                      <img src={`${process.env.PUBLIC_URL}/images/Medtutor-1.0_44.jpg`} alt="" />
                      <p>Access Evidence-based, Interactive Learning  Models</p>
                    </ReactWOW>
                  </div>
                </div>
              </div>{/*col end*/}
              <div className="col-md-3 col-sm-12">
                <div className="box">
                  <div className="popular-sec-bottom-boxs">
                    <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
                      <img src={`${process.env.PUBLIC_URL}/images/Medtutor-1.0_47.jpg`} alt="" />
                      <p>View Multimedia Models to Help You Learn in Simulated Enviroments</p>
                    </ReactWOW>
                  </div>
                </div>
              </div>{/*col end*/}
              <div className="col-md-3 col-sm-12">
                <div className="box">
                  <div className="popular-sec-bottom-boxs">
                    <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
                      <img src={`${process.env.PUBLIC_URL}/images/Medtutor-1.0_50.jpg`} alt="" />
                      <p>Plan &amp; Record Your Learning Online with Med Tutor Portfolio</p>
                    </ReactWOW>
                  </div>
                </div>
              </div>{/*col end*/}
              <div className="col-md-3 col-sm-12">
                <div className="box">
                  <div className="popular-sec-bottom-boxs">
                    <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
                      <img src={`${process.env.PUBLIC_URL}/images/Medtutor-1.0_52.jpg`} alt="" />
                      <p>Resources for Appraisal
                                        &amp; Revalidation</p>
                    </ReactWOW>
                  </div>
                </div>
              </div>{/*col end*/}
            </div>{/*row end*/}
          </div>{/*containr end*/}
        </section>{/*chose sec end*/}
        <section className="track-pass-sec">
          <div className="container">
            <div className="row align-items-center">
              <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
                <div className="col-md-6 col-sm-12">
                  <img src={`${process.env.PUBLIC_URL}/images/new-images_28.png`} className="img-full" alt="" />
                </div>
              </ReactWOW>
              <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
                <div className="col-md-6 col-sm-12">
                  <h2>Am I on Track to Pass? <br /> How do I Compare to Others</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                  <a href="#" className="button-main">Learn More <i className="fa fa-chevron-circle-right" /></a>
                </div>
              </ReactWOW>
            </div>
          </div>{/*container end*/}
        </section>{/*track pass sec end*/}
        <section className="about-bottom">
          <div className="container">
            <div className="row align-items-center">
              <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
                <div className="col-md-6 col-sm-12">
                  <h2>About MedTutor</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                  <a href="#" className="button-main"><img src={`${process.env.PUBLIC_URL}/images/new-images_34.png`} alt="" /> Video Overview</a>
                </div>
              </ReactWOW>
              <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
                <div className="col-md-6 col-sm-12">
                  <img src={`${process.env.PUBLIC_URL}/images/new-images_32.png`} className="img-full" alt="" />
                </div>
              </ReactWOW>
            </div>
          </div>{/*container end*/}
        </section>{/*track pass sec end*/}
        <section className="sec-4">
          <div className="container">
            <div className="row">
              <div className="col-lg-4 col-md-12 col-sm-12">
                <div className="white-testi-head">
                  <h3>customer</h3>
                  <h2 className="glitch" data-text="testimonials">testimonials</h2>
                  <p>It is a long established fact that a reader will be distracted by the readable content of a page
                  when looking at its layout.
            </p>
                </div>
              </div>
              <div className="col-lg-8 col-md-12 col-sm-12">
                {/* <Carousel className="owl-carousel3 owl-theme"> */}
                <div className="owl-carousel3 owl-theme">
                  <div className="item shadow">
                    <p>“Lorem Ipsum is simply dumamy text of the printing and typesetting to make a type specimen
                book.Lorem Ipsum is simply dummy text of thstan dard dummy text ever since the 1500s.</p>
                    <img src={`${process.env.PUBLIC_URL}/images/client.png`} />
                    <h5>jake smith</h5>
                    <h6>Member</h6>
                  </div>
                  <div className="item shadow">
                    <p>“Lorem Ipsum is simply dumamy text of the printing and typesetting to make a type specimen
                book.Lorem Ipsum is simply dummy text of thstan dard dummy text ever since the 1500s.</p>
                    <img src={`${process.env.PUBLIC_URL}/images/client.png`} />
                    <h5>jake smith</h5>
                    <h6>Member</h6></div>
                  <div className="item shadow">
                    <p>“Lorem Ipsum is simply dumamy text of the printing and typesetting to make a type specimen
                book.Lorem Ipsum is simply dummy text of thstan dard dummy text ever since the 1500s.</p>
                    <img src={`${process.env.PUBLIC_URL}/images/client.png`} />
                    <h5>jake smith</h5>
                    <h6>Member</h6></div>
                  <div className="item shadows">
                    <p>“Lorem Ipsum is simply dumamy text of the printing and typesetting to make a type specimen
                book.Lorem Ipsum is simply dummy text of thstan dard dummy text ever since the 1500s.</p>
                    <img src={`${process.env.PUBLIC_URL}/images/client.png`} />
                    <h5>jake smith</h5>
                    <h6>Member</h6></div>
                </div>
                {/* </Carousel> */}
              </div>
            </div>
          </div>
        </section>
        <ReactWOW animation='fadeInDown' duration="1s" delay="0.9s">
          <div className="container">
            <ReactWOW animation='fadeInUp animated' duration="1s" delay="0.3s">
              <div className="subscribe-area wow fadeInUp animated animated" data-wow-duration="1s" data-wow-delay="0.3s" style={{ visibility: 'visible', animationDuration: '1s', animationDelay: '0.3s', animationName: 'fadeInUp' }}>
                <div className="row">
                  <div className="col-lg-4 col-md-12 col-sm-12">
                    <h4>Subscribe To Get
                                Latest News</h4>
                  </div>
                  <div className="col-lg-8 col-md-12 col-sm-12">
                    <div className="newsletter shadow">
                      <input type="email" placeholder="Enter Your Email Address" onChange={(e) => this.setState({ newsLetterEmail: e.target.value })} />
                      <button onClick={this.onSubscribe}>Notify Me</button>
                      <div className="clearfix" />
                    </div>
                  </div>
                </div>
              </div>
            </ReactWOW>
          </div>
        </ReactWOW>

      </div >
    );
  }
}
export default Homepage;