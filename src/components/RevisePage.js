import React, { Component } from 'react';
import Navbar from './Navbar';

class RevisePage extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div >
                <div className="banner">
                <div className="container">
            <div className="row banner-caption">
                <div className="col-lg-7 col-md-12 col-sm-12">
                  <h1>Revise <i>Now</i></h1>
                  <h5>Over 5000+ MCQs, EMQ &amp; VSAQs available</h5> 
                  </div>
              <div className="col-lg-5 col-md-12 col-sm-12">
                <img src={`${process.env.PUBLIC_URL}/images/new-images_07.png`} className="main-top-banner-right-img" alt="" />
              </div> 
            </div>
            </div>
            </div>
            <div className="study">
            <div className="container">
            <div className="row">
                <div className="col-12 text-center">
                    <h1>Revise Now </h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                </div> 
            </div> 
            </div>
            <div className="about-uniq revise-modes">
                <div className="container">  
            <div className="row mt-5">
              <div className="col-md-3 col-sm-12">
                <div className="box">
                  <div className="popular-sec-bottom-boxs">
                      <img src={`${process.env.PUBLIC_URL}/images/revise-1.png`} alt="" />
                      <p>Questions type</p>
                      <h6>Classic Mode</h6>
                  </div>
                </div>
              </div>{/*col end*/}
              <div className="col-md-3 col-sm-12">
                <div className="box">
                  <div className="popular-sec-bottom-boxs"> 
                      <img src={`${process.env.PUBLIC_URL}/images/revise-2.png`} alt="" />
                      <p>Questions type</p> 
                      <h6>MCQs, EMQs </h6>
                  </div>
                </div>
              </div>{/*col end*/}
              <div className="col-md-3 col-sm-12">
                <div className="box">
                  <div className="popular-sec-bottom-boxs"> 
                      <img src={`${process.env.PUBLIC_URL}/images/revise-1.png`} alt="" />
                      <p>Questions type</p> 
                      <h6>System Mode </h6>
                  </div>
                </div>
              </div>{/*col end*/} 
            </div>{/*row end*/}
                </div> 
            </div>
            
        
        <div className="container"> 
              <div className="subscribe-area about-subs">
                <div className="row">
                  <div className="col-lg-4 col-md-12 col-sm-12">
                    <h4>Subscribe To Get Latest News</h4>
                  </div>
                  <div className="col-lg-8 col-md-12 col-sm-12">
                    <div className="newsletter shadow">
                      <input type="email" placeholder="Enter Your Email Address" onChange={(e) => this.setState({ newsLetterEmail: e.target.value })} />
                      <button onClick={this.onSubscribe}>Notify Me</button>
                      <div className="clearfix" />
                    </div>
                  </div>
                </div>
              </div> 
          </div>
            </div>
        );
    }
}

export default RevisePage;