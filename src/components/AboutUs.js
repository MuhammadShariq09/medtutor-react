import React, { Component } from 'react';
import Navbar from './Navbar';

class AboutUs extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div >
                <div className="banner">
                <div className="container">
            <div className="row banner-caption">
                <div className="col-lg-7 col-md-12 col-sm-12">
                  <h1>About <i>Us</i></h1>
                  <h5>Over 5000+ MCQs, EMQ &amp; VSAQs available</h5> 
                  </div>
              <div className="col-lg-5 col-md-12 col-sm-12">
                <img src={`${process.env.PUBLIC_URL}/images/new-images_07.png`} className="main-top-banner-right-img" alt="" />
              </div> 
            </div>
            </div>
            </div>
            <div className="study">
            <div className="container">
            <div className="row">
                <div className="col-12 text-center">
                    <h1>Study Any Time, <br/> Anywhere</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                </div>
                </div> 
            </div> 
            </div>
            <div className="about-uniq">
                <div className="container">
                <div className="row align-items-center"> 
                <div className="col-md-6 col-sm-12">
                  <h2>Use Our Unique <img src={`${process.env.PUBLIC_URL}/images/new-images_24.png`} className="imgg" alt="" /> to Key Learn Facts Quickly &amp; Retain them Long Term</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                </div> 
                <div className="col-md-6 col-sm-12 p-0">
                  <img src={`${process.env.PUBLIC_URL}/images/new-images_21.png`} className="img-full" alt="" />
                </div> 
            </div>{/*row end*/}
            
            <div className="row mt-5">
              <div className="col-md-3 col-sm-12">
                <div className="box">
                  <div className="popular-sec-bottom-boxs">
                      <img src={`${process.env.PUBLIC_URL}/images/Medtutor-1.0_44.jpg`} alt="" />
                      <p>Access Evidence-based, Interactive Learning  Models</p>
                  </div>
                </div>
              </div>{/*col end*/}
              <div className="col-md-3 col-sm-12">
                <div className="box">
                  <div className="popular-sec-bottom-boxs"> 
                      <img src={`${process.env.PUBLIC_URL}/images/Medtutor-1.0_47.jpg`} alt="" />
                      <p>View Multimedia Models to Help You Learn in Simulated Enviroments</p> 
                  </div>
                </div>
              </div>{/*col end*/}
              <div className="col-md-3 col-sm-12">
                <div className="box">
                  <div className="popular-sec-bottom-boxs"> 
                      <img src={`${process.env.PUBLIC_URL}/images/Medtutor-1.0_50.jpg`} alt="" />
                      <p>Plan &amp; Record Your Learning Online with Med Tutor Portfolio</p> 
                  </div>
                </div>
              </div>{/*col end*/}
              <div className="col-md-3 col-sm-12">
                <div className="box">
                  <div className="popular-sec-bottom-boxs">
                      <img src={`${process.env.PUBLIC_URL}/images/Medtutor-1.0_52.jpg`} alt="" />
                      <p>Resources for Appraisal &amp; Revalidation</p>
                  </div>
                </div>
              </div>{/*col end*/}
            </div>{/*row end*/}
                </div> 
            </div>
            <section className="about-bottom">
          <div className="container">
            <div className="row align-items-center"> 
                <div className="col-md-6 col-sm-12">
                <img src={`${process.env.PUBLIC_URL}/images/new-images_32.png`} className="img-full" alt="" />

                </div> 
                <div className="col-md-6 col-sm-12">
                  <h2>About MedTutor</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                </div> 
            </div>
          </div>{/*container end*/}
        </section>{/*track pass sec end*/}
        <div className="container"> 
              <div className="subscribe-area about-subs">
                <div className="row">
                  <div className="col-lg-4 col-md-12 col-sm-12">
                    <h4>Subscribe To Get Latest News</h4>
                  </div>
                  <div className="col-lg-8 col-md-12 col-sm-12">
                    <div className="newsletter shadow">
                      <input type="email" placeholder="Enter Your Email Address" onChange={(e) => this.setState({ newsLetterEmail: e.target.value })} />
                      <button onClick={this.onSubscribe}>Notify Me</button>
                      <div className="clearfix" />
                    </div>
                  </div>
                </div>
              </div> 
          </div>
            </div>
        );
    }
}

export default AboutUs;