import React from 'react';
import Navbar from './Navbar';
import { Link , withRouter } from 'react-router-dom';
import axios from 'axios';
import { toast } from 'react-toastify';

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        packages: [
          {
            duration: 0,
            course: {
              title: ''
            }
          }
        ]
      },
      password: {},
      updateUser: {},

      showSaveModal: false
    }
  }

  onChangeDetail = (e) => {
    const value = e.target.value;
    const tempUser = { ...this.state.user } ? { ...this.state.user } : {};
    if (e.target.id === "name") {
      tempUser.name = value;
    }
    else if (e.target.id === "email") {
      tempUser.email = value;
    }

    this.setState({ user: tempUser })
  }
  onChangePassword = (e) => {

    const value = e.target.value;
    const tempPass = { ...this.state.password } ? { ...this.state.password } : {};
    if (e.target.id === "currPass") {
      tempPass.currentPassword = value;
    }
    else if (e.target.id === "newPass") {
      tempPass.newPassword = value;
    }
    else if (e.target.id === "confirmNewPass") {
      tempPass.confirmNewPassword = value;
    }

    this.setState({ password: tempPass, showPasswordMismatch: '' });
  }

  onSaveChanges = (e) => {
    this.setState({ showPasswordMismatch: '' })
    e.preventDefault();
    if (this.state.password.currentPassword && this.state.password.newPassword && this.state.password.confirmNewPassword) {
    if(this.state.password.currentPassword.length < 6 || this.state.password.newPassword.length < 6 || this.state.password.confirmNewPassword.length < 6){
      this.setState({ showPasswordMismatch: 'Passwords must be at least 6 characters in length!' })
        return;
    }
  }

    var updateObject = { email: this.state.user.email, name: this.state.user.name };
    if (this.state.password.currentPassword && this.state.password.newPassword && this.state.password.confirmNewPassword) {
      if (this.state.password.newPassword === this.state.password.confirmNewPassword) {
        updateObject = {
          ...updateObject,
          current_password: this.state.password.currentPassword,
          password: this.state.password.newPassword,
          password_confirmation: this.state.password.confirmNewPassword
        }
      }
      else {
        this.setState({ showPasswordMismatch: 'Passwords do not match!' })
        return;
      }
    }
    else if (this.state.updateUser.email) {
      updateObject = {
        ...updateObject,
        email: this.state.updateUser.email
      }
    }
    else if (this.state.updateUser.name) {
      updateObject = {
        ...updateObject,
        name: this.state.updateUser.name
      }
    }
    else if (this.state.updateUser.image) {
      updateObject = {
        ...updateObject,
        image: this.state.updateUser.image
      }
    }



    // if ((updateObject.password)) {

    const token = localStorage.getItem('token');
    //     axios.post('/login', { email: this.state.user.email, password: this.state.password.currentPassword },
    //         { headers: { Authorization: 'Bearer ' + token } }
    //     )
    //         .then(res => {

    //             if (res.data.token) {
    updateObject.current_password = this.state.password.currentPassword;
    console.log("updateObject: ", updateObject);
    axios.put('/users/profile', updateObject,
      { headers: { Authorization: 'Bearer ' + token } })
      .then(resp => {
        if (resp.status === 200) {
          this.setState({ showSaveModal: true })
          document.getElementById('successModal').click();
        }
        else {
          this.setState({ showPasswordMismatch: 'Error updating info, please try later.' })
        }
      }).catch(error => {
        console.log("LLL: ",error.response);
        error.response.data.errors && Object.values(error.response.data.errors).map((err, index) => {
          toast.error(err[index]);
        })
      })
        // }
        //     else {
        //         this.setState({ showPasswordMismatch: 'Error accesing info, please try later.' })
        //         return;
        //     }
        // })
        // .catch(err => this.setState({ showPasswordMismatch: 'Please enter correct current password!' }));
        // }

      }

  handleFileSelect = (evt) => {
          let that = this;
          var f = evt.target.files[0]; // FileList object
          var reader = new FileReader();
          // Closure to capture the file information.
          reader.onload = (function (theFile) {
            return function (e) {
              var binaryData = e.target.result;
              const fileType = f.type.split('/');
              //Converting Binary Data to base 64
              var base64String = window.btoa(binaryData);

              let tempUser = { ...that.state.updateUser } ? { ...that.state.updateUser } : {};
              tempUser.image = `data:image/${fileType[1]};base64,` + base64String;

              let myStateUser = { ...that.state.user } ? { ...that.state.user } : {};
              myStateUser.image = tempUser.image;
              let localStUser = JSON.parse(localStorage.getItem('user'));
              if (localStUser) {
                localStUser.image = tempUser.image;
              }
              localStorage.setItem('user', JSON.stringify(localStUser));
              that.setState({ user: myStateUser, updateUser: tempUser })
              // that.uploadFileToServer(base64String, f.type);

              //showing file converted to base64
              // document.getElementById('base64').value = base64String;
              // alert('File converted to base64 successfuly!\nCheck in Textarea');
            }
          })(f)
          // Read in the image file as a data URL.
          reader.readAsBinaryString(f);
        }

  componentDidMount() {
        let that = this;
        if(localStorage.getItem('token') && localStorage.getItem('token').length) {
      axios.get('/users/profile')
        .then(res => {
          that.setState({
            user: res.data
          })
        })
    }
  }

  render() {
    if (localStorage.getItem('token')) {
      return (
        <div>
          {/* <Navbar /> */}
          <section className="edit-profile-section">
            <div className="container">
              <div className="card border-0 edit-profile-card ">
                <div className="main-sec">
                  <div className="card-header edit-profile-header bg-white border-bottom-0">
                    <div className="edit-profile-head-button">
                      <Link to="/" ><span><i className="fa fa-angle-left font-weight-bold" aria-hidden="true" /></span> Go back</Link>
                    </div>
                  </div>
                  <div className="card-body pt-0">
                    <div className="edit-profile-card-body">
                      <div className="edit-pro-information-head" style={{ paddingBottom: "15px !important" }}>
                        <h4>Information</h4>
                      </div>
                      <div className="edit-profile-information">
                        <ul className="list-unstyled">
                          <li>
                            <h6>Your subscribed package(s): {this.state.user.packages.map(eachPackage => {
                              return (
                                <div>
                                  <li>{eachPackage.course.title} ({eachPackage.duration}) months</li>
                                </div>
                              )
                            })}
                              {/* {this.state.user.packages[0].course.title}  ({this.state.user.packages[0].duration} months) */}
                            </h6>
                          </li>
                          <li className="subscribe-for-another-courses">
                            <button className="custom-theme-button-style"><Link to="/registercourse" >Subscribe for another course</Link></button>
                          </li>
                          <li>
                            <p className="mt-0">(if user will click on this link user will get redirect to buy subscribtion package screen)</p>
                          </li>
                        </ul>
                      </div>
                      <div className="edit-profile-image-sec py-3">
                        <div className="row">
                          <div className="col-lg-2">
                            <div className="edit-pro-image ml-auto">
                              <img src={this.state.user.image} alt="" className="w-100" />
                            </div>
                          </div>
                          <div className="col-lg-10">
                            <div className="change-profile-image-button">
                              <button  >
                                <span className="icon-camera"><i className="fa fa-camera" /></span>
                                <span className="text-on-button" onClick={() => document.getElementById('ppUpload').click()}>
                                  <input type="file"
                                    id="ppUpload"
                                    name="ppUp"
                                    onChange={this.handleFileSelect}
                                    style={{ display: "none" }}
                                  />
                                  Change Picture</span>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      {/* row-end */}
                      <div className="edit-user-name-sec">
                        <form action="#" method="get" acceptCharset="utf-8">
                          <div className="form-group">
                            <ul className="list-unstyled">
                              <li>
                                <label>Full Name</label>
                                <input type="text" id="name" defaultValue={this.state.user.name} className="w-100 custom-input-box" onChange={this.onChangeDetail} />
                              </li>
                              <li className="pt-3">
                                <label>Email</label>
                                <input type="email" id="email" defaultValue={this.state.user.email} readOnly className="w-100 custom-input-box" />
                              </li>
                            </ul>
                          </div>
                        </form></div>
                      {/* change-password-sec-start */}
                      <div className="change-pass-sec">
                        <h4 className="py-4 font-weight-bold">Change Password</h4>
                        <div className="form-group">
                          <ul className="list-unstyled">
                            <li>
                              <label>Current password</label>
                              <input type="password" id="currPass" className="w-100 custom-input-box" onChange={this.onChangePassword} />
                            </li>
                            <li className="pt-3">
                              <label>New Password</label>
                              <input type="password" id="newPass" className="w-100 custom-input-box" onChange={this.onChangePassword} />
                            </li>
                            <li className="pt-3">
                              <label>Re-type Password</label>
                              <input type="password" id="confirmNewPass" className="w-100 custom-input-box" onChange={this.onChangePassword} />
                            </li>
                          </ul>
                          <p>{this.state.showPasswordMismatch}</p>
                        </div>
                      </div>
                      {/* change-password-sec-end */}
                      {/* submit-button-sec-start */}
                      <div className="submit-button-sec mt-5">
                        <button className="custom-theme-button-style text-white font-weight-bold" onClick={this.onSaveChanges}>Save Changes</button>
                      </div>
                      {/* submit-button-sec-end  */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* container-end */}
          </section>
          {/* edit-pro-sec-end */}
          {/* modal-new-memorizer-start*/}
          {/* The Modal */}
          <a id="successModal" data-toggle="modal" href="#myModal" style={{ display: 'none' }}></a>
          {
            <div className="modal" id="myModal">
              <div className="modal-dialog">
                <div className="modal-content">
                  {/* Modal Header */}
                  <div className="modal-header">
                    <h4 className="modal-title">Profile Update</h4>
                    <button type="button" className="close" data-dismiss="modal">×</button>
                  </div>
                  {/* Modal body */}
                  <div className="modal-body">
                    Your profile has been successfully updated.
                                </div>
                  {/* Modal footer */}
                  <div className="modal-footer">
                    <button type="button" className="btn btn-success" data-dismiss="modal" onClick={() => this.props.history.push('/')}>Close</button>
                  </div>
                </div>
              </div>
            </div>
          }
        </div>
      );
    }
    else {
      this.props.history.push('/login');
      return null;
    }
    // return <h1>Please login before accessing this.</h1>
  }
}

export default withRouter(Profile);