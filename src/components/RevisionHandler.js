import React, { Component, Fragment } from "react";
import axios from "axios";
import { toast } from 'react-toastify';
import { withRouter } from 'react-router-dom';
import Reference from "./Reference";
var random = require('lodash.random');
var uniqBy = require('lodash.uniqby');

class RevisionHandler extends Component {
  constructor(props) {
    super(props);
    this.state = {
      testQuestions: props.testQuestions,
      isNewTest: props.isNewTest,
      isTestComplete: props.isTestComplete,
      isPreviousQuestionAvailable: false,
      isNextQuestionAvailable: true,
      activeQuestionIndex: 0,
      activeQuestion: props.testQuestions[0],
      toAddMemos: props.memorizers,
      selectedMemorizers: [],
      testAnswers: [],
      sessionEnded: false,
      answersProcessed: false,
      isSubmitted: false,
      showReference: false,
      answerErrorMessage: '',
      newMemoName: "",
      memorizers: this.props.memorizers
    };
  }

  getCurrentQuestionAnswer = thisQuestionId => {
    var currentAnswers = [...this.state.testAnswers]
      ? [...this.state.testAnswers]
      : [];
    const foundQuestion = currentAnswers.find(answer => answer.questionId === thisQuestionId)

    return foundQuestion;
  };

  isPreviousQuestionAvailable = (activeIndex) => {
    console.log("isPreviousQuestionAvailable: ", activeIndex);
    var previousQuestion = this.state.activeQuestion;
    if (activeIndex > 0 && this.state.isTestComplete) {
      activeIndex--;
      return this.state.testQuestions[activeIndex]
    }
    while (activeIndex > 0) {
      if (this.state.testQuestions[activeIndex].repeatedIndex !== undefined) {
        return this.state.testQuestions[this.state.activeQuestion.repeatedIndex];
      }
      activeIndex--;
      const found = this.getCurrentQuestionAnswer(this.state.testQuestions[activeIndex].id);
      console.log("found: ", found);
      if ((found === undefined)) {
        previousQuestion = this.state.testQuestions[activeIndex];

        return previousQuestion;
      }
      else if ((found.correct === undefined) || (found.correct === false && this.props.repitition === true)) {
        previousQuestion = this.state.testQuestions[activeIndex];

        return previousQuestion;
      }
    }
    return null;
  }

  isNextQuestionAvailable = (activeIndex) => {
    console.log("isNextQuestionAvailable: ", activeIndex);
    var nextQuestion = this.state.activeQuestion;
    if (activeIndex < (this.state.testQuestions.length - 1) && this.state.isTestComplete) {
      console.log("IF CALLED: ", activeIndex < this.state.testQuestions.length - 1, this.state.isTestComplete);
      activeIndex++;
      return this.state.testQuestions[activeIndex]
    }
    while (activeIndex < (this.state.testQuestions.length - 1)) {
      activeIndex++;
      const found = this.getCurrentQuestionAnswer(this.state.testQuestions[activeIndex].id);
      console.log("found: ", found, this.state.testQuestions);
      if (this.state.testQuestions[activeIndex].repeatedIndex !== undefined) {
        return this.state.testQuestions[this.state.testQuestions[activeIndex].repeatedIndex];
      }
      else if ((found === undefined)) {
        nextQuestion = this.state.testQuestions[activeIndex];
        return nextQuestion;
      }
      else if ((found.correct === undefined) || (found.correct === false && this.props.repitition === true)) {
        nextQuestion = this.state.testQuestions[activeIndex];
        return nextQuestion;
      }
    }
    return null;
  }

  onClickPreviousQuestion = () => {
    var activeIndex = this.state.activeQuestionIndex;
    var isPrevAvail = false;
    var isNextAvail = this.isNextQuestionAvailable(activeIndex);
    var previousQuestion = this.isPreviousQuestionAvailable(activeIndex);
    if (previousQuestion !== null) {
      var prevIndex = this.state.testQuestions.indexOf(previousQuestion);
      isPrevAvail = this.isPreviousQuestionAvailable(prevIndex) !== null ? true : false;
      isNextAvail = this.isNextQuestionAvailable(prevIndex) !== null ? true : false;
      activeIndex = this.state.testQuestions.indexOf(previousQuestion);
      this.setState({
        isPreviousQuestionAvailable: isPrevAvail,
        isNextQuestionAvailable: isNextAvail,
        activeQuestionIndex: activeIndex,
        activeQuestion: previousQuestion,
        showReference: this.state.isTestComplete,
        answerErrorMessage: '',
        isSubmitted: false
      });
    }
    else {
      this.setState({
        isPreviousQuestionAvailable: false,
        isNextQuestionAvailable: isNextAvail,
        showReference: this.state.isTestComplete,
        answerErrorMessage: '',
        isSubmitted: false
      });
    }
    const el = document.getElementById('vsaqInput');
    if (el) {
      el.value = '';
    }
  }

  onClickNextQuestion = () => {
    if (this.state.isTestComplete) {

    }
    var activeIndex = this.state.activeQuestionIndex;
    console.log("activeIndex: ", activeIndex);
    var isPrevAvail = false;
    isPrevAvail = this.isPreviousQuestionAvailable(activeIndex) !== null ? true : false;
    var isNextAvail = false;
    var nextQuestion = this.isNextQuestionAvailable(activeIndex);
    if (nextQuestion !== null) {
      var nextIndex = this.state.testQuestions.indexOf(nextQuestion);
      isNextAvail = this.isNextQuestionAvailable(nextIndex) !== null ? true : false;
      console.log("isNextAvail: ", isNextAvail);
      isPrevAvail = this.isPreviousQuestionAvailable(nextIndex) !== null ? true : false;
      activeIndex = this.state.testQuestions.indexOf(nextQuestion);
      this.setState({
        isPreviousQuestionAvailable: isPrevAvail,
        isNextQuestionAvailable: isNextAvail,
        activeQuestionIndex: activeIndex,
        activeQuestion: nextQuestion,
        showReference: this.state.isTestComplete,
        answerErrorMessage: '',
        isSubmitted: false
      });
    }
    else {
      console.log("isNextAvailELSE: ", isNextAvail);
      this.setState({
        isPreviousQuestionAvailable: isPrevAvail,
        isNextQuestionAvailable: false,
        showReference: this.state.isTestComplete,
        answerErrorMessage: '',
        isSubmitted: false
      });
    }
    const el = document.getElementById('vsaqInput');
    if (el) {
      el.value = '';
    }
  }

  onClickMCQOption = e => {
    const { id } = e.target;
    this.setState({ answerErrorMessage: '' })
    this.updateLocalAnswers({
      type: "MCQ",
      questionId: this.state.activeQuestion.id,
      sequenceIndex: this.state.activeQuestionIndex,
      answer: id,
      correct: undefined
    });
  };

  onEnterVSAQAnswer = (e) => {
    var { value } = e.target;
    this.setState({
      answerErrorMessage: ''
    })
    if (value.length) {
      value = value.toLowerCase();
      this.updateLocalAnswers({
        type: "VSAQ",
        questionId: this.state.activeQuestion.id,
        sequenceIndex: this.state.activeQuestionIndex,
        answer: value,
        text: value,
        correct: undefined
      });
    }
  }

  onClickSubmitAnswer = () => {
    const myAnswer = this.getCurrentQuestionAnswer(this.state.activeQuestion.id);
    if (myAnswer !== undefined) {
      const answerIndex = myAnswer.questionId;
      axios.post(`/courses/${this.props.courseId}/question/${answerIndex}/check`, { answer: myAnswer.answer, test_id: this.props.testId })
        .then(res => {
          var allQuestions = [...this.state.testQuestions];
          console.log("allQuestions: ", allQuestions, this.state.testQuestions);
          for (let i = 0; i < allQuestions.length; i++) {
            console.log("allQuestions.length: ", allQuestions.length);
            const thisQuestion = allQuestions[i];
            if (thisQuestion.repeatedIndex !== undefined && (thisQuestion.repeatedIndex === this.state.activeQuestionIndex)) {
              allQuestions.splice(i, 1);
            }
          }
          if (allQuestions !== this.state.testQuestions) {
            this.setState({ testQuestions: allQuestions });
          }
          if (res.data.answer === false && this.props.repitition === true) {
            this.applySpaceRepitition();
          }
          var updatedAnswer = { ...myAnswer, correct: res.data.answer };
          this.updateLocalAnswers({ ...updatedAnswer });
          // this.onClickNextQuestion();
          this.setState({ showReference: true, isSubmitted: true })
        });
    } else {
      this.setState({ answerErrorMessage: 'Please give an answer before submitting.' });
    }
  };

  updateLocalAnswers = myAnswer => {
    const { type, questionId, sequenceIndex, answer, correct } = myAnswer;
    var myTestAnswers = [...this.state.testAnswers] ? [...this.state.testAnswers] : [];
    const answerExist = this.getCurrentQuestionAnswer(questionId);

    if (answerExist === undefined) {
      myTestAnswers.push({ ...myAnswer });
    } else {
      const answerIndex = myTestAnswers.indexOf(myTestAnswers.find(answer => answer.questionId === questionId));
      if (type === 'EMQ') {
        var currentAnswer = myTestAnswers[answerIndex];
        var answeredScenarios = [...currentAnswer.answer] ? [...currentAnswer.answer] : [];
        if (correct !== undefined) {
          currentAnswer.correct = correct;
        }
        else if (answeredScenarios.length) {
          if ((answeredScenarios.find(myanswers => myanswers.scenario_id === answer[0].scenario_id)) !== undefined) {
            const scenarioIndex = answeredScenarios.indexOf(
              answeredScenarios.find(answers => answers.scenario_id === answer[0].scenario_id)
            );
            answeredScenarios[scenarioIndex] = { ...answer[0], correct: myAnswer.correct };
          }
          else {
            answeredScenarios.push({ ...answer[0], correct: myAnswer.correct });
          }
        }
        else {
          answeredScenarios.push({ ...answer[0], correct: myAnswer.correct });
        }

        currentAnswer.answer = answeredScenarios;
        myTestAnswers[answerIndex] = currentAnswer;
      }
      else {
        myTestAnswers[answerIndex] = { ...myAnswer };
      }
    }

    if (myTestAnswers.length) {
      myTestAnswers.sort(function (a, b) {
        return a.sequenceIndex - b.sequenceIndex;
      });
    }
    var isTestCompleted = myTestAnswers.length === this.state.testQuestions.length;

    this.setState({ testAnswers: myTestAnswers, isTestComplete: isTestCompleted }, () => {
      localStorage.setItem("myTestAnswers", JSON.stringify(myTestAnswers));
      this.getAnswersStatus();
    });
  };

  processCurrentTestStatus = () => {
    const myQuestions = [...this.state.testQuestions] ? [...this.state.testQuestions] : [];
    var myTestAnswers = [...this.state.testAnswers]
      ? [...this.state.testAnswers]
      : [];
    var assigned = false;
    var activeIndex = 0;
    var activeQuestion = myQuestions[0];

    myQuestions.forEach((question, index) => {
      if (question.pivot.attempted === 1) {
        const myAnswer = {
          type: question.type,
          questionId: question.id,
          sequenceIndex: index,
          answer: question.pivot.answer,
          correct: question.pivot.correct === 1 ? true : false
        }

        const answerExist = this.getCurrentQuestionAnswer(question.id);

        if (answerExist === undefined) {
          myTestAnswers.push({ ...myAnswer });
        } else {
          const answerIndex = myTestAnswers.indexOf(
            myTestAnswers.find(answer => answer.questionId === question.id)
          );
          myTestAnswers[answerIndex] = { ...myAnswer };
        }

        if (myTestAnswers.length) {
          myTestAnswers.sort(function (a, b) {
            return a.sequenceIndex - b.sequenceIndex;
          });
        }

        if (question.pivot.correct === 0) {
          if (!assigned) {
            activeIndex = index;
            activeQuestion = myQuestions[index];
            assigned = true;
          }
        }
      }
      else {
        if (!assigned) {
          activeIndex = index;
          activeQuestion = myQuestions[index];
          assigned = true;
        }
      }
    })
    this.setState({
      testAnswers: myTestAnswers,
      activeQuestionIndex: activeIndex,
      activeQuestion: activeQuestion,
      showReference: this.props.isTestComplete
    }, () => {
      localStorage.setItem("myTestAnswers", JSON.stringify(myTestAnswers));
      this.getAnswersStatus();
      // this.onClickNextQuestion();
    });
  }

  getAnswersStatus = () => {
    var processedAnswers = [];
    const myAnswers = [...this.state.testAnswers]
      ? [...this.state.testAnswers]
      : [];
    if (myAnswers.length) {
      processedAnswers = myAnswers.map((answer, index) => {

        if (answer.correct !== undefined) {
          if (answer.correct) {
            return (
              <li key={index} className="result">
                <div className="d-flex justify-content-between">
                  <div className="correct-answers">
                    <span>{answer.sequenceIndex + 1}</span>
                    <span className="pl-2">Correct</span>
                  </div>
                  <div className="correct-ans-icon">
                    <i className="fa fa-check-circle" aria-hidden="true" />
                  </div>
                </div>
              </li>
            );
          } else {
            return (
              <li key={index} className="result">
                <div className="d-flex justify-content-between">
                  <div className="correct-answers">
                    <span>{answer.sequenceIndex + 1}</span>
                    <span className="pl-2">Wrong</span>
                  </div>
                  <div className="wrong-ans-icon">
                    <i className="fa fa-times-circle" aria-hidden="true" />
                  </div>
                </div>
              </li>
            );
          }
        }
        else if (answer.correct === undefined && answer.type === 'VSAQ') {
          return (
            <li key={index} className="result">
              <div className="d-flex justify-content-between">
                <div className="correct-answers">
                  <span>{answer.sequenceIndex + 1}</span>
                  <span className="pl-2">{answer.text}</span>
                </div>
              </div>
            </li>
          );
        }
        else if (answer.correct === undefined && answer.type === 'EMQ') {
          return (
            <li key={index} className="result">
              <div className="d-flex justify-content-between">
                <div className="correct-answers">
                  <span>{answer.sequenceIndex + 1}</span>
                  <ul>
                    {answer.answer.map(scenario => (
                      <li>Scenario {parseInt(scenario.index) + 1} : {scenario.option}</li>
                    ))}
                  </ul>
                  <span className="pl-2">{answer.text}</span>
                </div>
              </div>
            </li>
          );
        }
        else if (answer.correct === 'unanswered') {
          return (
            <li key={index} className="result">
              <div className="d-flex justify-content-between">
                <div className="correct-answers">
                  <span>{answer.sequenceIndex + 1}</span>
                  <span className="pl-2">Left unanswered</span>
                </div>
                <div className="wrong-ans-icon">
                  <i className="fa fa-check-circle" aria-hidden="true" />
                </div>
              </div>
            </li>
          );
        }
        else {
          return (
            <li key={index} className="result">
              <div className="d-flex justify-content-between">
                <div className="correct-answers">
                  <span>{answer.sequenceIndex + 1}</span>
                  <span className="pl-2">Submission pending</span>
                </div>
                {/* <div className="correct-ans-icon">
                                    <i className="fa fa-check-circle" aria-hidden="true" />
                                </div> */}
              </div>
            </li>
          );
        }
      });
    }
    this.setState({ processedAnswers: processedAnswers });
  };

  onClickEndAndPreview = () => {
    const { courseId } = this.props.match.params;
    if (localStorage.getItem("testType") === "memorizer") {
      localStorage.removeItem("testType");
      this.props.history.push({
        pathname: `/courses/${courseId}/memorizers`,
      });

      return;
    }
    let that = this;
    // if (this.props.isTestComplete) {
    //   this.props.goBackToAllRevisions();
    //   return;
    // }
    const tempAnswers = [...this.state.testAnswers] ? [...this.state.testAnswers] : [];
    for (let index in tempAnswers) {
      if (tempAnswers[index].correct === undefined) {
        this.onShowToast("Please submit your pending answers before ending session.", 'warning');
        return;
      }
    }
    axios.post(`/courses/${this.props.courseId}/tests/${this.props.testId}/finish`)
      .then(res => {
        if (res.data.message) {
          that.props.testCompleted();
        }
      })
  }

  onShowToast = (content, type) => {
    if (type === 'success') {
      return toast.success(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
      });
    }
    else if (type === 'info') {
      return toast.info(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
      });
    }
    else if (type === 'warning') {
      return toast.warning(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
      });
    }
    else if (type === 'danger') {
      return toast.danger(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
      });
    }
  }

  applySpaceRepitition = () => {
    var randomIndex = -1;
    if (this.state.activeQuestionIndex === this.state.testQuestions.length - 1) {
      randomIndex = this.state.activeQuestionIndex + 1;
    }
    else {
      const activeIndex = this.state.activeQuestionIndex;
      if (activeIndex + 1 < this.state.testQuestions.length - 1) {
        randomIndex = random(activeIndex + 1, this.state.testQuestions.length - 1, false);
      }
      else {
        randomIndex = random(activeIndex, this.state.testQuestions.length - 1, false);
      }
    }
    var repeatedTestQuestions = [...this.state.testQuestions];
    // const found = repeatedTestQuestions.indexOf(question => (question.repeatedIndex && question.repeatedIndex === this.state.activeQuestionIndex));

    // if (found > -1) {
    repeatedTestQuestions.splice(randomIndex, 0,
      {
        title: this.state.activeQuestion.title,
        repeatedIndex: this.state.activeQuestionIndex
      });
    // }

    this.setState({ testQuestions: repeatedTestQuestions }, () => {
      // this.onClickNextQuestion();
    });
    // this.getUniqueArray();
  }

  getUniqueArray = () => {
    const tempArray = [...this.state.testQuestions];
    return (uniqBy(tempArray, 'title'));
  }

  onHandleKeyDown = (e) => {

    // arrow up/down button should select next/previous list element
    if (e.keyCode === 37) {
      this.onClickPreviousQuestion();
    } else if (e.keyCode === 39) {
      this.onClickNextQuestion();
    }
  }

  getMemorizers = () => {
    // const myMemos = this.props.memorizers;
    const myMemos = this.state.memorizers;
    if (myMemos && myMemos.length) {
      return myMemos.map((memo, index) => (
        <li key={index}>
          <label className="memorizer-container">{memo.title}
            <input type="checkbox" id={memo.id} onChange={this.onChangeAddToMemo} />
            <span className="memorizer-checkmark" />
          </label>
        </li>
      ))
    }
    return null;
  }
  
  getAllMemorizers = () => {
    if (localStorage.getItem('token')) {
      let that = this;
      axios.get('/memorizers')
        .then(res => {
          
          that.setState({
            memorizers: res.data
          }, () => {
            // that.populateMemorizers()
            that.getMemorizers();
            window.$("#myNewMemoModal").modal("hide");
            // that.props.setMemorizers(res.data)
          })
        })
    }
  }

  onChangeAddToMemo = (e) => {
    const { checked, id } = e.target;
    const selectedMemos = [...this.state.selectedMemorizers] ? [...this.state.selectedMemorizers] : [];
    const index = selectedMemos.indexOf(id);
    if (checked) {
      if (index < 0) {
        selectedMemos.push(id);
      }
    }
    else {
      if (index > -1) {
        selectedMemos.splice(index, 1);
      }
    }
    this.setState({ selectedMemorizers: selectedMemos });
  }

  onClickAddQuestionToMemorizers = () => {
    const toAddMemos = [...this.state.selectedMemorizers] ? [...this.state.selectedMemorizers] : [];
    toAddMemos.forEach(memo => {
      axios.post(`/memorizers/${memo}/questions`,
        {
          question_id: this.state.activeQuestion.id,
          custom: false
        })
        .then(res => {

          if (res.data) {
            this.onShowToast('Question has been added to memorizer(s).', 'success');
          }
          else {
            this.onShowToast(`Question couldn't be added to memorizer(s).`, 'warning');
          }
        })
    })
  }

  onClickSaveTest = () => {
    const courseId = localStorage.getItem("courseId")
    if (localStorage.getItem("testType") === "memorizer") {
      localStorage.removeItem("testType");
      this.props.history.push({
        pathname: `/courses/${courseId}/memorizers`,
      });
      window.location.reload();
      return;
    } else {
      this.props.goBackToAllRevisions();
    }
  }

  onSelectScenarioOption = (e) => {
    const [index, scenario, option_id, option] = e.target.value.split('-');
    this.setState({ answerErrorMessage: '' });
    this.updateLocalAnswers(
      {
        type: "EMQ",
        questionId: this.state.activeQuestion.id,
        sequenceIndex: this.state.activeQuestionIndex,
        answer: [{ index: index, scenario_id: parseInt(scenario), option_id: parseInt(option_id), option: option }],
        correct: undefined
      }
    )
  }

  componentDidMount() {
    // const ourLocalAnswers = JSON.parse(localStorage.getItem("myTestAnswers"));
    // if (ourLocalAnswers !== null) {

    //   this.setState({ testAnswers: ourLocalAnswers });
    // }
    const isNewTest = this.state.isNewTest;
    if (!isNewTest) {
      this.processCurrentTestStatus();
    }
  };

  onCreateMemo = (e) => {
    e.preventDefault();
    // document.getElementById('myNewMemoModal').click();
    axios.post('/memorizers', { title: this.state.newMemoName })
      .then(res => {
        if (res.data) {
          toast.success("Memorizer has been saved!");
          this.getAllMemorizers();
          // this.onShowToast('Memorizer has been saved!', 'success');
        }
        else {
          toast.warn(`Memorizer couldn't be saved!`);
          // this.onShowToast(`Memorizer couldn't be saved!`, 'warning');
        }
      })
      .catch(err => toast.warn(`Memorizer couldn't be saved!`));
  }

  // getMemorizers = () => {
  //   if (localStorage.getItem('token')) {
  //     let that = this;
  //     axios.get('/memorizers')
  //       .then(res => {

  //         that.setState({
  //           memorizers: res.data
  //         }, () => {
  //           // that.populateMemorizers()
  //           that.props.setMemorizers(res.data)
  //         })
  //       })
  //   }
  // }

  render() {
    return (
      <div>
        <div className="revise-mode">
          <div className="revise-question-sec">
            <div className="row">
              <div className="col-lg-8">
                <div className="row first-row-part my-4">
                  <div className="col-lg-6 col-md-6 col-6">
                    <button className="previous-button"
                      disabled={!this.state.isPreviousQuestionAvailable}
                      onClick={this.onClickPreviousQuestion}
                      style={{
                        cursor: !this.state.isPreviousQuestionAvailable
                          ? "not-allowed"
                          : "pointer"
                      }}>
                      <span> <i className="fa fa-angle-left" /></span>Previous
                      </button>
                  </div>
                  <div className="col-lg-6 col-md-6 col-6 text-right">
                    <button
                      className="next-button"
                      disabled={!this.state.isNextQuestionAvailable}
                      onClick={this.onClickNextQuestion}
                      style={{
                        cursor: !this.state.isNextQuestionAvailable
                          ? "not-allowed"
                          : "pointer"
                      }}
                    >
                      Next
                    <span className="pull-right">
                        <i className="fa fa-angle-right" />
                      </span>
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-lg-4" />
            </div>
            <div className="row sec-row-part">
              <div className="col-lg-8 border-bottom">
                <div className="row">
                  <div className="col-lg-6 col-md-6 my-3">
                    <h4>
                      Question <span>{this.state.activeQuestionIndex + 1}</span>{" "}
                      of <span>{this.getUniqueArray().length}</span>
                      {/* of <span>{this.state.testQuestions.length}</span> */}
                    </h4>
                  </div>
                  <div className="col-lg-6 col-md-6 text-right my-2">
                    <button
                      data-toggle="modal"
                      data-target="#addtomemorizerModal"
                      className="add-to-memorizer"
                    >
                      <span>
                        <i className="fa fa-plus-circle" aria-hidden="true" />
                      </span>Add to Memoriser</button>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="row my-2">
                  <div className="col-lg-6 col-md-6 col-6 text-left">
                    <button className="save-and-exit" onClick={this.onClickSaveTest}>
                      Save &amp; Exit
                      </button>
                  </div>
                  <div className="col-lg-6 col-md-6 col-6 text-right">
                    <button onClick={this.onClickEndAndPreview} className="end-preview">
                      End &amp; Preview
                      </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="question-sec">
          <div className="row"
            style={{
              opacity: `${this.state.isSubmitted || this.props.isTestComplete ? 0.6 : 1}`,
              PointerEvent: `${this.state.isSubmitted || this.props.isTestComplete ? 'none' : 'auto'
                }`
            }}>
            {this.state.activeQuestion.type === "MCQ" ? (
              <Fragment>
                <div className="col-lg-2 col-md-2">
                  <div className="image-sec-part">
                    <img src={`${process.env.PUBLIC_URL}/images/question-img.png`} alt="" className="w-100" />
                  </div>
                </div>
                <div className="col-lg-6 col-md-6 multiple-choice-sec">
                  <div className="questions-sec">
                    <p>{this.state.activeQuestion.title}</p>
                    <p>{this.state.activeQuestion.repeatedIndex ? 'Repeated Question.' : null}</p>
                  </div>
                  <div className="mcqs-sec">
                    <ul className="list-unstyled">
                      {this.state.activeQuestion.options.map(
                        (option, index) => {
                          return (
                            <li key={index} className="d-inline-block"
                            >
                              <label className="radio-container"
                                disabled={this.state.isSubmitted || this.props.isTestComplete}
                                style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? 'not-allowed' : 'pointer'}` }}
                              >
                                <span className="font-weight-bold">
                                  {option.scenario}
                                </span>
                                <input
                                  disabled={this.state.isSubmitted || this.props.isTestComplete}
                                  type="radio"
                                  name="radio"
                                  id={option.id}
                                  onClick={this.onClickMCQOption}
                                />
                                <span className="radio-checkmark" />
                                <span className="pull-right options mr-auto">
                                  {option.explanation
                                    ? option.explanation
                                    : "Explanation here"}
                                </span>
                              </label>
                            </li>
                          );
                        }
                      )}
                    </ul>
                  </div>
                  <div className="submit-button-sec">
                    <button id="mcq" onClick={this.onClickSubmitAnswer}
                      disabled={this.state.isSubmitted || this.props.isTestComplete}
                      style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? "not-allowed" : "pointer"}` }}>
                      Submit Answer</button>
                  </div>
                </div>
              </Fragment>
            ) : null
            }
            {
              this.state.activeQuestion.type === "VSAQ" && this.state.activeQuestion.preview ? (
                <div className="col-lg-8 col-md-2 vsaq-revise-mode">
                  <div className="fill-in-the-blank-que">
                    <p>
                      {/* {this.state.activeQuestion.preview} */}
                      {this.state.activeQuestion.preview.split('(')[0]}
                      <span className="ml-1 mr-1">
                        <input type="text" id="vsaqInput" onChange={this.onEnterVSAQAnswer}
                          disabled={this.state.isSubmitted || this.props.isTestComplete}
                          style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? "not-allowed" : "pointer"}` }}
                        />
                      </span>
                      {/* {this.state.activeQuestion.preview} */}
                      {/* {this.state.activeQuestion.preview.substring(this.state.activeQuestion.preview.indexOf(')') + 1)} */}
                    </p>
                  </div>
                  <div className="image-sec-part">
                    <img src={`${process.env.PUBLIC_URL}/images/question-img.png`} alt="" className="w-100" />
                  </div>
                  <div className="submit-button-sec">
                    <button id="vsaq"
                      disabled={this.state.isSubmitted || this.props.isTestComplete}
                      style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? "not-allowed" : "pointer"}` }}
                      onClick={this.onClickSubmitAnswer}
                      className="f-w-600">Submit Answer</button>
                  </div>
                </div>
              )
                :
                null
            }
            {
              this.state.activeQuestion.type === "EMQ" ? (
                <div className="col-xl-8 col-lg-8 col-md-8">
                  <div className="row">
                    <div className="col-lg-3 col-md-3">
                      <div className="image-sec-part">
                        <img src={`${process.env.PUBLIC_URL}/images/question-img.png`} alt="" className="w-100" />
                      </div>
                    </div>
                    <div className="col-lg-9 col-md-9 multiple-choice-sec">
                      <div className="questions-sec">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam Lorem ipsum dolor sit  sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam?</p>
                      </div>
                      {/* <div class="submit-button-sec">
                              <button type="">Submit Answer</button>
                            </div> */}
                    </div>
                    {/* col-lg-6 */}
                  </div>
                  {/* row */}
                  <h3>Options</h3>
                  <div className="row">
                    {
                      this.state.activeQuestion.options.map((option) =>
                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-3 col-6">
                          <div className="multiple-options">
                            <label>{option.scenario}</label>
                            {/* <label className="radio-container">Option 2
                            <input type="radio" defaultChecked="checked" name="radio" />
                            <span className="radio-checkmark" />
                          </label> */}
                          </div>
                        </div>
                      )
                    }
                  </div>
                  {/* row */}
                  {this.state.activeQuestion.scenarios.map((scenario, index) => (
                    <div>
                      <div className="scenario--first-sec-start">
                        <div className="scenario-first-head">
                          <h4>Scenario {index + 1}</h4>
                        </div>
                        <div className="scenario-sec-desc">
                          <p>{scenario.explanation}</p>
                        </div>
                        <div className="choose-ans">
                          <div className="row">
                            <div className="col-xl-3 col-lg-3 col-md-3">
                              <div className="choose-ans-label">
                                <label htmlFor="choose-ans">Choose Answer</label>
                              </div>
                            </div>
                            <div className="col-xl-9 col-lg-9 col-md-9">
                              <div className="selectt-ans">
                                <select className="form-control" id="sel1" onChange={this.onSelectScenarioOption}>
                                  {
                                    this.state.activeQuestion.options.map((option, i) =>
                                      <option
                                        value={index + '-' + scenario.id + '-' + i + '-' + option.scenario}
                                        disabled={this.state.isSubmitted || this.props.isTestComplete}
                                        style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? "not-allowed" : "pointer"}` }}
                                      >{option.scenario}</option>
                                    )
                                  }
                                  {/* <option>Select</option> */}
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr />
                    </div>
                  ))}
                  <div class="submit-button-sec">
                    <button
                      id={this.state.activeQuestion.type.toLowerCase()}
                      onClick={this.onClickSubmitAnswer}
                      disabled={this.state.isSubmitted || this.props.isTestComplete}
                      style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? "not-allowed" : "pointer"}` }}
                    >Submit Answer</button>
                  </div>
                </div>
              ) : null
            }
            <div className="col-lg-4 col-md-4">
              <div className="card correct-answers-card">
                <ul className="list-unstyled">
                  <div
                    style={{
                      margin: "5px auto",
                      display: "block",
                      textAlign: "center"
                    }}
                  >
                    <h4>Answers</h4>
                  </div>
                  {this.state.processedAnswers}
                </ul>
              </div>
            </div>
          </div >
          <p>{this.state.answerErrorMessage}</p>
          {
            this.state.showReference
              ?
              <div>
                <Reference activeQuestion={this.state.activeQuestion} courseId={this.props.courseId} questionId={this.state.activeQuestion.id} />
              </div>
              :
              null
          }
        </div >

        <div className="modal" id="addtomemorizerModal">
          <div className="modal-dialog">
            <div className="modal-content myselect-memorizerModal">
              {/* Modal Header */}
              <div className="modal-header pb-0 border-bottom-0">
                <button type="button" className="close pt-1 pb-0" data-dismiss="modal" style={{ cursor: 'pointer' }}>×</button>
              </div>
              {/* Modal body */}
              <div className="modal-body">
                <div className="myselect-memorizerModal-body">
                  <div className="select-memorizer-head text-center">
                    <h2>Select Memoriser</h2>
                    <p>Please select memory card in which you want to add this question.</p>
                  </div>
                  <div className="select-memorizer-wrapper">
                    <ul className="list-unstyled">
                      {this.state.memorizers &&  this.getMemorizers()}
                    </ul>
                  </div>
                  <div className="d-flex justify-content-between">
                    <div className="add-button">
                      <button data-dismiss="modal" onClick={this.onClickAddQuestionToMemorizers}>Add</button>
                    </div>
                    <div className="add-too-memorizer align-self-center">
                      <a data-toggle="modal" href="#myNewMemoModal" >
                        <i className="fa fa-plus-circle" aria-hidden="true" />Add New Memoriser
                      </a>
                    </div>
                  </div>
                </div>
              </div>

              {/* CREATE NEW MEMORISER MODAL START */}
              <div className="modal" id="myNewMemoModal">
                <div className="modal-dialog">
                  <div className="modal-content">
                    {/* Modal body */}
                    <form onSubmit={this.onCreateMemo}>
                      <div className="modal-body text-center new-memorizer">
                        <img src="/images/new-memorizer.png" alt="" />
                        <h2>New Memoriser</h2>
                        <input type="text" className="w-100 custom-input-box" placeholder="Enter Name" onChange={(e) => this.setState({ newMemoName: e.target.value })} required />
                        {/* <p>{this.state.memoNameError}</p> */}
                        <div className="submit-button-sec my-2">
                          <button type="submit" className="w-100 py-3 custom-theme-button-style text-white font-weight-bold">Create</button>
                          {/* onClick={this.onCreateMemo} */}
                        </div>
                      </div>
                    </form>
                    {/* Modal footer */}
                  </div>
                </div>
              </div>
              {/* CREATE NEW MEMORISER MODAL END */}
            </div>
          </div>
        </div>
      </div>
    );
  };

  isIterable(obj) {
    // checks for null and undefined
    if (obj == null) {
      return false;
    }
    return typeof obj[Symbol.iterator] === "function";
  };
}

export default withRouter(RevisionHandler);
