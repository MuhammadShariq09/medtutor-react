const auth = {
    login: '/login',
    register : '/register',
    logout : '/logout',
    password_reset_request : '/password/create',
    password_reset_token_validation : '/password/toke/validate/',
    password_reset : '/password/reset',
}

export const user = {
    auth,
    user_profile: '/users/profile',
}

export const news_letters = {
    subscribe: '/newsletters',
    unsubscribe: '/newsletters/'
}

export const memorizer = {
    index: '/memorizers',
    store: '/memorizers',
    show: '/memorizers/',
    update: '/memorizers/',
    add_question: '/memorizers/1/questions'
}