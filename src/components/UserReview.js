import React from 'react';
import Rating from 'react-rating';

const UserReview = (props) => {
    const { name, image, rating, comment } = props;
    return (
        <div className="media p-3">
            <img src={image} alt="User here" className="align-self-start mr-3 rounded-circle" style={{ width: '60px' }} />
            <div className="media-body">
                <h4>{name}</h4>
                <Rating
                    emptySymbol="fa fa-star-o fa-2x"
                    fullSymbol="fa fa-star fa-2x"
                    fractions={2}
                    initialRating={rating}
                    readonly={true}
                    style={{ color: "orange" }}
                />
                <p className="mt-0">{comment}</p>
            </div>
        </div>
    );
}

export default UserReview;