import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import $ from 'jquery';
import * as jQuery from 'jquery';
import 'bootstrap/dist/css/bootstrap.css';
import { Route, BrowserRouter as Router, Switch, HashRouter } from 'react-router-dom'
import Homepage from './components/Homepage';
import Login from './components/Login';
import Notfound from './components/Notfound';
import Register from './components/Register';
import RegisterCourse from './components/RegisterCourse';
import Profile from './components/Profile';
import ContactUs from './components/ContactUs';
import Navbar from './components/Navbar';
import axios from 'axios';
import Courses from './components/Courses';
import Footer from './components/Footer';
import LoadingOverlay from 'react-loading-overlay';
import AboutUs from './components/AboutUs';
import RevisePage from './components/RevisePage';


window.jQuery = jQuery;

class Loader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      ajaxCalls: 0
    }
  }

  checkAxiosPending = () => {
    let that = this;
    // Add a request interceptor
    axios.interceptors.request.use(function (config) {
      let ajax = that.state.ajaxCalls + 1;
      // show loader
      that.setState({ ajaxCalls: ajax, isLoading: true })
      return config;
    }, function (error) {
      return Promise.reject(error);
    });

    // Add a response interceptor
    axios.interceptors.response.use(function (response) {
      let ajax = that.state.ajaxCalls - 1;

      that.setState({ ajaxCalls: ajax })

      if (ajax == 0) {
        //hide loader
        that.setState({ isLoading: false })
      }
      return response;
    }, function (error) {
      let ajax = that.state.ajaxCalls - 1;

      that.setState({ ajaxCalls: ajax })

      if (ajax == 0) {
        //hide loader
        that.setState({ isLoading: false })
      }
      return Promise.reject(error);
    });


  }

  componentDidMount() {
    this.checkAxiosPending()
  }

  render() {
    return (
      <Router basename={process.env.NODE_ENV === "development" ? "" : "/medtutor_react/"}>
        <LoadingOverlay
          active={this.state.isLoading}
          spinner
          text='Loading'>
          <Switch>
            <Route exact path="/" component={Homepage} />
            <Switch>
              <Route exact path="/login">
                <div className="profile-page">
                  <Navbar />
                  <Login />
                </div>
              </Route>
              <Route exact path="/register">
                <div className="profile-page">
                  <Navbar />
                  <Register />
                </div>
              </Route>
              <Route exact path="/registerCourse">
                <div className="profile-page">
                  <Navbar />
                  <RegisterCourse />
                </div>
              </Route>
              <Route exact path="/courses/:courseId/:activeTab?/:testId?">
                <div className="profile-page">
                  <Navbar />
                  <Courses />
                </div>
              </Route>
              <Route exact path="/aboutus">
                <Navbar />
                <AboutUs />
              </Route>
              <Route exact path="/contact">
                <Navbar />
                <ContactUs />
              </Route>
              <Route exact path="/revise">
                <Navbar />
                <RevisePage />
              </Route>
              {/* <Route exact path="/profile" component={Profile} /> */}
              <Route exact path="/profile">
                <div className="profile-page">
                  <Navbar />
                  <Profile />
                </div>
              </Route>
              <Route>
                <div className="profile-page">
                  <Navbar />
                  <Notfound />
                </div>

              </Route>
            </Switch>
          </Switch>
          <Footer />
        </LoadingOverlay>
      </Router>
    );
  }
}


const token = localStorage.getItem('token');
if (token) {

  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
}

window.$ = $;
window.jQuery = jQuery;

require('popper.js');
require('bootstrap');

ReactDOM.render(<Loader />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

// new WOW().init();
