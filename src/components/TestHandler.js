import React, { Component } from "react";
import axios from "axios";
import SessionPerformance from "./SessionPerformance";
import { toast } from 'react-toastify';
import Reference from "./Reference";
import Countdown from 'react-countdown-now';



class TestHandler extends Component {
  constructor(props) {
    super(props);
    this.state = {
      testQuestions: props.testQuestions,
      isPreviousQuestionAvailable: false,
      isNextQuestionAvailable: true,
      activeQuestionIndex: 0,
      activeQuestion: props.testQuestions[0],
      testAnswers: [],
      selectedMemorizers: [],
      sessionEnded: false,
      answersProcessed: false,
      showReference: props.showReference,
      timeSetting: false,
      newMemoName: '',
      memorizers: this.props.memorizers,
      arr : []
    };
  }
    generateRandrom = ()=>{
      
  while(this.state.arr.length < 5){
    var r = Math.floor(Math.random() * this.state.activeQuestion.options.length) + 1;
    if(this.state.arr.indexOf(r) === -1) this.state.arr.push(r);
    console.log("random",this.state.arr)
}
    }
  getCurrentQuestionAnswer = thisQuestionId => {
    var currentAnswers = [...this.state.testAnswers]
      ? [...this.state.testAnswers]
      : [];
    const foundQuestion = currentAnswers.find(answer => answer.questionId === thisQuestionId)
    return foundQuestion;
  };

  isPreviousQuestionAvailable = (activeIndex) => {
    var previousQuestion = this.state.activeQuestion;
    while (activeIndex > 0) {
      if (this.state.testQuestions[activeIndex].repeatedIndex) {
        return this.state.testQuestions[found.repeatedIndex];
      }
      activeIndex--;
      const found = this.getCurrentQuestionAnswer(this.state.testQuestions[activeIndex].id);
      if ((found === undefined)) {
        previousQuestion = this.state.testQuestions[activeIndex];

        return previousQuestion;
      }
      else if (found.correct === undefined) {
        previousQuestion = this.state.testQuestions[activeIndex];

        return previousQuestion;
      }
    }
    return null;
  }

  isNextQuestionAvailable = (activeIndex) => {
    var nextQuestion = this.state.activeQuestion;
    while (activeIndex < this.state.testQuestions.length - 1) {
      activeIndex++;
      const found = this.getCurrentQuestionAnswer(this.state.testQuestions[activeIndex].id);
      if (this.state.testQuestions[activeIndex].repeatedIndex !== undefined) {

        return this.state.testQuestions[this.state.testQuestions[activeIndex].repeatedIndex];
      }
      else if ((found === undefined)) {
        nextQuestion = this.state.testQuestions[activeIndex];

        return nextQuestion;
      }
      else if (found.correct === undefined) {
        nextQuestion = this.state.testQuestions[activeIndex];

        return nextQuestion;
      }
    }
    return null;
  }

  onClickPreviousQuestion = () => {
    var activeIndex = this.state.activeQuestionIndex;
    var isPrevAvail = false;
    var isNextAvail = this.isNextQuestionAvailable(activeIndex);
    var previousQuestion = this.isPreviousQuestionAvailable(activeIndex);
    if (previousQuestion !== null) {
      var prevIndex = this.state.testQuestions.indexOf(previousQuestion);
      isPrevAvail = this.isPreviousQuestionAvailable(prevIndex) !== null ? true : false;
      isNextAvail = this.isNextQuestionAvailable(prevIndex) !== null ? true : false;
    }
    if (previousQuestion !== null) {
      activeIndex = this.state.testQuestions.indexOf(previousQuestion);
      this.setState({
        isPreviousQuestionAvailable: isPrevAvail,
        isNextQuestionAvailable: isNextAvail,
        activeQuestionIndex: activeIndex,
        activeQuestion: previousQuestion,
        showReference: false
      });
    }
    else {
      this.setState({
        isPreviousQuestionAvailable: false,
        isNextQuestionAvailable: isNextAvail,
        showReference: false,
      });
    }
    const el = document.getElementById('vsaqInput');
    if (el) {
      el.value = '';
    }
  }

  onClickNextQuestion = () => {
    var activeIndex = this.state.activeQuestionIndex;
    var isPrevAvail = false;
    isPrevAvail = this.isPreviousQuestionAvailable(activeIndex) !== null ? true : false;
    var isNextAvail = false;
    var nextQuestion = this.isNextQuestionAvailable(activeIndex);
    if (nextQuestion !== null) {
      var nextIndex = this.state.testQuestions.indexOf(nextQuestion);
      isNextAvail = this.isNextQuestionAvailable(nextIndex) !== null ? true : false;
      isPrevAvail = this.isPreviousQuestionAvailable(nextIndex) !== null ? true : false;
    }
    if (nextQuestion !== null) {
      activeIndex = this.state.testQuestions.indexOf(nextQuestion);
      this.setState({
        isPreviousQuestionAvailable: isPrevAvail,
        isNextQuestionAvailable: isNextAvail,
        activeQuestionIndex: activeIndex,
        activeQuestion: nextQuestion,
        showReference: false
      });
    }
    else {
      this.setState({
        isPreviousQuestionAvailable: isPrevAvail,
        isNextQuestionAvailable: false,
        showReference: false
      });
    }
    const el = document.getElementById('vsaqInput');
    if (el) {
      el.value = '';
    }
  }

  onClickMCQOption = e => {
    const { id } = e.target;
    this.updateLocalAnswers({
      type: "MCQ",
      questionId: this.state.activeQuestion.id,
      sequenceIndex: this.state.activeQuestionIndex,
      answer: id,
      text: this.state.activeQuestion.options.find(option => option.id === parseInt(id)).scenario,
      correct: undefined
    });
  };

  onEnterVSAQAnswer = (e) => {
    var { value } = e.target;
    if (value.length) {
      value = value.toLowerCase();
      this.updateLocalAnswers({
        type: "VSAQ",
        questionId: this.state.activeQuestion.id,
        sequenceIndex: this.state.activeQuestionIndex,
        answer: value,
        text: value,
        correct: undefined
      });
    }
  }

  onClickSubmitAnswer = () => {
    const myAnswer = this.getCurrentQuestionAnswer(this.state.activeQuestion.id);
    if (myAnswer !== undefined) {
      console.log(myAnswer)
      const answerIndex = myAnswer.questionId;
      axios.post(`/courses/${localStorage.getItem("courseId")}/question/${answerIndex}/check`, {
        answer: myAnswer.answer,
        test_id: this.props.testId
      })
        .then(res => {
          var updatedAnswer = { ...myAnswer, correct: res.data.answer };
          this.updateLocalAnswers({ ...updatedAnswer });

          // this.onClickNextQuestion();

        });
    } else {

    }
  };

  updateLocalAnswers = myAnswer => {
    const { type, questionId, sequenceIndex, answer, correct } = myAnswer;
    console.log(myAnswer)
    var myTestAnswers = [...this.state.testAnswers] ? [...this.state.testAnswers] : [];
    
    const answerExist = this.getCurrentQuestionAnswer(questionId);
    

    if (answerExist === undefined) {
      myTestAnswers.push({ ...myAnswer });
    } else {
      const answerIndex = myTestAnswers.indexOf( myTestAnswers.find(answer => answer.questionId === questionId) );
      if (type === 'EMQ') {
        var currentAnswer = myTestAnswers[answerIndex];
        var answeredScenarios = [...currentAnswer.answer] ? [...currentAnswer.answer] : [];
        if (correct !== undefined) {
          currentAnswer.correct = correct;
        }
        else if (answeredScenarios.length) {
          if ((answeredScenarios.find(myanswers => myanswers.scenario_id === answer[0].scenario_id)) !== undefined) {
            const scenarioIndex = answeredScenarios.indexOf( answeredScenarios.find(answers => answers.scenario_id === answer[0].scenario_id) );
            answeredScenarios[scenarioIndex] = { ...answer[0], correct: myAnswer.correct };
          }
          else {
            answeredScenarios.push({ ...answer[0], correct: myAnswer.correct });
          }
        }
        else {
          answeredScenarios.push({ ...answer[0], correct: myAnswer.correct });
        }
        currentAnswer.answer = answeredScenarios;
        myTestAnswers[answerIndex] = currentAnswer;
      }
      else {
        myTestAnswers[answerIndex] = { ...myAnswer };
      }
    }
    if (myTestAnswers.length) {
      myTestAnswers.sort(function (a, b) {
        return a.sequenceIndex - b.sequenceIndex;
      });
    }

    this.setState({ testAnswers: myTestAnswers }, () => {
      localStorage.setItem("myTestAnswers", JSON.stringify(myTestAnswers));
      this.getAnswersStatus();
    });
  };

  getAnswersStatus = () => {
    var processedAnswers = [];
    const myAnswers = [...this.state.testAnswers]
      ? [...this.state.testAnswers]
      : [];

    if (myAnswers.length) {
      processedAnswers = myAnswers.map((answer, index) => {

        if (answer.type === "EMQ") {
          return (
            <li key={index} className="result">
              <div className="d-flex justify-content-between">
                <div className="correct-answers">
                  <span>{answer.sequenceIndex + 1}</span>
                  <ul>
                    {answer.answer.map(scenario => (
                      <li>Stem {parseInt(scenario.index) + 1} : {scenario.option}</li>
                    ))}
                  </ul>
                  <span className="pl-2">{answer.text}</span>
                </div>
              </div>
            </li>
          );
        }
        else if (answer.type === "VSAQ") {
          return (
            <li key={index} className="result">
              <div className="d-flex justify-content-between">
                <div className="correct-answers">
                  <span>{answer.sequenceIndex + 1}</span>
                  <span className="pl-2">{answer.text}</span>
                </div>
              </div>
            </li>
          );
        }
        else if (answer.type === "MCQ") {
          return (
            <li key={index} className="result">
              <div className="d-flex justify-content-between">
                <div className="correct-answers">
                  <span>{answer.sequenceIndex + 1}</span>
                  <span className="pl-2">{answer.text}</span>
                </div>
              </div>
            </li>
          );
        }
      });
    }


    this.setState({ processedAnswers: processedAnswers });
  };

  onClickEndAndPreview = () => {
    const tempAnswers = [...this.state.testAnswers] ? [...this.state.testAnswers] : [];
    for (let index in tempAnswers) {
      if (tempAnswers[index].correct === undefined) {
        this.onShowToast("Please submit your pending answers before ending session.", 'warning');
        return;
      }
    }
    this.endTest();
  }

  endTest = () => {
    let that = this;
    axios.post(`/courses/${localStorage.getItem("courseId")}/tests/${this.props.testId}/finish`)
      .then(res => {
        if (res.data.message) {
          // that.setState({ sessionEnded: true });
          that.props.testCompleted();
        }
      })
  }

  onSelectScenarioOption = (e) => {
    console.log("e.target.value",e.target.value)
    const [index, scenario, option_id, option] = e.target.value.split('-');
    this.setState({
      answerErrorMessage: ''
    })
    this.updateLocalAnswers(
      {
        type: "EMQ",
        questionId: this.state.activeQuestion.id,
        sequenceIndex: this.state.activeQuestionIndex,
        answer: [{ index: index, scenario_id: parseInt(scenario), option_id:  parseInt(option_id), option: option }],
        correct: undefined
      }
    )
  }

  componentDidMount() {
    this.generateRandrom()
    const ourLocalAnswers = JSON.parse(localStorage.getItem("myTestAnswers"));
    this.setState({ timeSetting: this.props.expiryTime, memorizers: this.props.memorizers });


    if (ourLocalAnswers !== null) {

      this.setState({ testAnswers: ourLocalAnswers });
    }


  };

  getMemorizers = () => {
    const myMemos = this.state.memorizers;
    if (myMemos && myMemos.length) {
      return myMemos.map((memo, index) => (
        <li key={index}>
          <label className="memorizer-container">{memo.title}
            <input type="checkbox" id={memo.id} onChange={this.onChangeAddToMemo} />
            <span className="memorizer-checkmark" />
          </label>
        </li>
      ))
    }
    return null;
  }

  onChangeAddToMemo = (e) => {
    const { checked, id } = e.target;
    const selectedMemos = [...this.state.selectedMemorizers] ? [...this.state.selectedMemorizers] : [];
    const index = selectedMemos.indexOf(id);
    if (checked) {
      if (index < 0) {
        selectedMemos.push(id);
      }
    }
    else {
      if (index > -1) {
        selectedMemos.splice(index, 1);
      }
    }
    this.setState({ selectedMemorizers: selectedMemos });
  }

  onClickAddQuestionToMemorizers = () => {
    const toAddMemos = [...this.state.selectedMemorizers] ? [...this.state.selectedMemorizers] : [];
    const Checkmemorizers = [...this.state.memorizers] ? [...this.state.memorizers] : [];
    let flag = 0;
    Checkmemorizers.map(item => {
      item.questions && item.questions.map(question => {
        if (toAddMemos.includes(item.id.toString()) && flag === 0 && question.id === this.state.activeQuestion.id) {
          toast.warn(`Question is already exists in the ${item.title} memorizer.`);
          flag = 1;
        }
      })
    })
    if(this.state.activeQuestion.type !== "MCQ"){
      toast.warn("Please add Only MCQs in the memoriser mode.");
      return;
    }
    if (flag === 0) {
      toAddMemos.forEach(memo => {
        axios.post(`/memorizers/${memo}/questions`, { question_id: this.state.activeQuestion.id, custom: false })
          .then(res => {
            if (res.data) {
              axios.get('/memorizers').then(response => { this.setState({ memorizers: response.data }); })
              this.onShowToast('Question has been added to memorizer(s).', 'success');
            }
            else {
              this.onShowToast(`Question couldn't be added to memorizer(s).`, 'warning');
            }
          })
      })
    }
    return;
  }

  onShowToast = (content, type) => {
    if (type === 'success') {
      return toast.success(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
      });
    }
    else if (type === 'info') {
      return toast.info(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
      });
    }
    else if (type === 'warning') {
      return toast.warning(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
      });
    }
    else if (type === 'danger') {
      return toast.danger(content, {
        position: "top-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false
      });
    }
  }

  onHandleKeyDown = (e) => {

    // arrow up/down button should select next/previous list element
    if (e.keyCode === 37) {
      this.onClickPreviousQuestion();
    } else if (e.keyCode === 39) {
      this.onClickNextQuestion();
    }
  }
  getAllMemorizers = () => {
    if (localStorage.getItem('token')) {
      let that = this;
      axios.get('/memorizers')
        .then(res => {
          
          that.setState({
            memorizers: res.data
          }, () => {
            // that.populateMemorizers()
            that.getMemorizers();
            window.$("#myNewMemoModal").modal("hide");
            // that.props.setMemorizers(res.data)
          })
        })
    }
  }

  onCreateMemo = (e) => {
    e.preventDefault();
    if (!this.state.newMemoName.length) {
      this.setState({ memoNameError: 'Memo name cannot be empty!' })
      return;
    }
    // document.getElementById('myNewMemoModal').click();
    axios.post('/memorizers', { title: this.state.newMemoName })
      .then(res => {
        if (res.data) {
          this.onShowToast('Memorizer has been saved!', 'success');
          this.getAllMemorizers();
        }
        else {
          this.onShowToast(`Memorizer couldn't be saved!`, 'warning');
        }
      })
      .catch(err => this.onShowToast(`Memorizer couldn't be saved!`, 'warning'));
  }

  render() {
  console.log("this.activeQuestion",this.state.activeQuestion.options)
   
    return (
      <div className="revise-mode my-5">
        <div className="row">
          <div className="col-lg-8">
            <div className="row first-row-part">
              <div className="col-lg-6 col-md-6 col-6">
                <button
                  className="previous-button"
                  disabled={!this.state.isPreviousQuestionAvailable}
                  onClick={this.onClickPreviousQuestion}
                  style={{
                    cursor: this.state.isPreviousQuestionAvailable
                      ? "pointer"
                      : "not-allowed"
                  }}
                >
                  <span>
                    <i className="fa fa-angle-left" />
                  </span>
                  Previous
                       </button>
              </div>
              <div className="col-lg-6 col-md-6 col-6 text-right">
                <button
                  className="next-button"
                  disabled={!this.state.isNextQuestionAvailable}
                  onClick={this.onClickNextQuestion}
                  style={{ cursor: this.state.isNextQuestionAvailable ? "pointer" : "not-allowed" }}>
                  Next{" "}
                  <span className="pull-right">
                    <i className="fa fa-angle-right" />
                  </span>
                </button>
              </div>
            </div>
          </div>
          <div className="col-lg-4" />
        </div>
        <div className="revise-question-sec">
          <div className="row sec-row-part">
            <div className="col-lg-8 border-bottom">
              <div className="row">
                <div className="col-lg-6 col-md-6 my-3">
                  <h4>
                    Question <span>{this.state.activeQuestionIndex + 1}</span>{" "}
                    of <span>{this.state.testQuestions.length}</span>
                  </h4>
                </div>
                <div className="col-lg-6 col-md-6 text-right my-2">
                  <button
                    data-toggle="modal"
                    data-target="#addtoTestMemorizerModal"
                    className="add-to-memorizer">
                    <span> <i className="fa fa-plus-circle" aria-hidden="true" /> </span>{" "}
                      Add to Memorizer
                    </button>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="row my-2">
                {/* <div className="col-lg-6 col-md-6 col-6 text-left">
                        <button type className="save-and-exit">
                          Save &amp; Exit
                        </button>
                      </div> */}
                <div className="col-lg-6 col-md-6 col-6 text-right">
                  <button onClick={this.onClickEndAndPreview} className="end-preview">End &amp; Preview</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="question-sec">
          <div className="row">
            <div className="col-lg-8 col-md-8 multiple-choice-sec">
              {this.state.activeQuestion.type == "MCQ" ? (
                <div>
                  <div className="questions-sec">
                    <p>{this.state.activeQuestion.title}</p>
                    <p>{this.state.activeQuestion.repeatedIndex ? 'Repeated Question.' : null}</p>
                  </div>
                  <div className="mcqs-sec">
                    <ul className="list-unstyled">
                      {this.state.activeQuestion.options.map(
                        (option, index) => {
                          return (
                            <li key={index} className="d-inline-block">
                              <label className="radio-container"
                                disabled={this.state.isSubmitted || this.props.isTestComplete}
                                style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? 'not-allowed' : 'pointer'}` }}
                              >
                                <span className="font-weight-bold">
                                  {option.scenario}
                                </span>
                                <input
                                  disabled={this.state.isSubmitted || this.props.isTestComplete}
                                  type="radio"
                                  name="radio"
                                  id={option.id}
                                  onClick={this.onClickMCQOption}
                                />
                                <span className="radio-checkmark" />
                                <span className="pull-right options mr-auto">
                                  { option.explanation ? option.explanation : "Explanation here" }
                                </span>
                              </label>
                            </li>
                          );
                        }
                      )}
                    </ul>
                  </div>
                  <div className="submit-button-sec">
                    <button id="mcq" onClick={this.onClickSubmitAnswer}
                      disabled={this.state.isSubmitted || this.props.isTestComplete}
                      style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? "not-allowed" : "pointer"}` }}
                    >
                      Submit Answer</button>
                  </div>
                </div>
              ) : null}
              {
                this.state.activeQuestion.type == "VSAQ" && this.state.activeQuestion.preview ? (
                  <div className="col-lg-8 col-md-2 vsaq-revise-mode">
                    <div className="questions-sec fill-in-the-blank-que" style={{ marginTop: "10px" }}>
                      <p>
                      {/* {this.state.activeQuestion.preview} */}
                        {this.state.activeQuestion.preview.split('(')[0]}
                        <span className="ml-1 mr-1">
                          <input type="text" id="vsaqInput" onChange={this.onEnterVSAQAnswer}
                            disabled={this.state.isSubmitted || this.props.isTestComplete}
                            style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? "not-allowed" : "pointer"}` }}
                          />
                        </span>
                        {/* {this.state.activeQuestion.preview.substring(this.state.activeQuestion.preview.indexOf(')') + 1)} */}
                      </p>
                    </div>
                    <div className="image-sec-part">
                      <img
                        src="images/question-img.png"
                        alt=""
                        className="w-100"
                      />
                    </div>
                    <div className="submit-button-sec">
                      <button id="vsaq" onClick={this.onClickSubmitAnswer}
                        disabled={this.state.isSubmitted || this.props.isTestComplete}
                        style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? "not-allowed" : "pointer"}` }}
                      >
                        Submit Answer</button>
                    </div>
                  </div>
                )
                  :
                  null
              }
              {
                this.state.activeQuestion.type == "EMQ" ? (
                  <div className="question-sec">
                  <div className="row">
                    <div className="col-lg-12 col-md-12 multiple-choice-sec">
                      <div className="session-performance-head">
                        <h2>Causes of Collapse?</h2>
                      </div>
                      <div className="row">
                      {
                        this.state.activeQuestion.options.map((option, index) =>

                          <div  key={index} className="col-lg-6">
                          <ul className="list-unstyled my-list-style">
                            <li className={this.state.arr.includes(index)?"ble-tx":""}><span></span>{option.scenario.toUpperCase()} </li>
                            </ul>
                          </div>
                        )
                      }
                      {this.state.activeQuestion.scenarios.map((scenario, index) => (
                      <div>
                          
                      <div className="row align-items-baseline">
                        <div className="col-lg-12">
                          <div className="revise-now-head">
                            <h3>Match the above options to the following stems </h3>
                          </div>
                        </div>
                        <div className="col-lg-12">
                          <div className="steps1">
                            <h3>Stem {index + 1}</h3>
                          </div>
                        </div>
                        <div className="col-lg-12">
                          <p>{scenario.explanation}</p>
                        </div>
                        <div className="col-lg-4">
                          <h3>Choose Answer </h3>
                        </div>
                        <div className="col-lg-8">
                          <div className="form-group">
                          <select class="form-control" id="sel1"  onChange={this.onSelectScenarioOption}>
                          <option>Select</option>

                                  {
                                      this.state.activeQuestion.options.map((option, i) =>
                                        <option
                                          value={index + '-' + scenario.id + '-' + option.id + '-' + option.scenario}
                                        
                                          disabled={this.state.isSubmitted || this.props.isTestComplete}
                                          style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? "not-allowed" : "pointer"}` }}
                                        >{option.scenario}</option>
                                      )
                                    }
                                  </select>
                          </div>
                        </div>
                      </div>
                      <hr />
                      </div>
                    ))}
                    <div className="submit-button-sec">
                      <button
                        id={this.state.activeQuestion.type.toLowerCase()}
                        onClick={this.onClickSubmitAnswer}
                        disabled={this.state.isSubmitted || this.props.isTestComplete}
                        style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? "not-allowed" : "pointer"}` }}
                      >Submit Answer</button>
                    </div>
                  </div>
                       
                  
                        
                
                
                  </div>
             
                </div>
         
                  
                     </div>
                  //     {/* col-lg-6 */}
                  //   </div>
                  //   {/* row */}
                  //   <h3>Options</h3>
                  //   <div className="row">
                  //     {
                  //       this.state.activeQuestion.options.map((option, index) =>
                  //         <div key={index} className="col-xl-3 col-lg-3 col-md-4 col-sm-3 col-6">
                  //           <div className="multiple-options">
                  //             <label>{option.scenario}</label>
                  //             {/* <label className="radio-container">Option 2
                  //           <input type="radio" defaultChecked="checked" name="radio" />
                  //           <span className="radio-checkmark" />
                  //         </label> */}
                  //           </div>
                  //         </div>
                  //       )
                  //     }
                  //   </div>
                  //   {/* row */}
                  //   {this.state.activeQuestion.scenarios.map((scenario, index) => (
                  //     <div>
                  //       <div className="scenario--first-sec-start">
                  //         <div className="scenario-first-head">
                  //           <h4>Scenario {index + 1}</h4>
                  //         </div>
                  //         <div className="scenario-sec-desc">
                  //           <p>{scenario.explanation}</p>
                  //         </div>
                  //         <div className="choose-ans">
                  //           <div className="row">
                  //             <div className="col-xl-3 col-lg-3 col-md-3">
                  //               <div className="choose-ans-label">
                  //                 <label htmlFor="choose-ans">Choose Answer</label>
                  //               </div>
                  //             </div>
                  //             <div className="col-xl-9 col-lg-9 col-md-9">
                  //               <div className="selectt-ans">
                  //                 <select className="form-control" id="sel1" onChange={this.onSelectScenarioOption}>
                  //                   {
                  //                     this.state.activeQuestion.options.map((option, i) =>
                  //                       <option
                  //                         value={index + '-' + scenario.id + '-' + i + '-' + option.scenario}
                  //                         disabled={this.state.isSubmitted || this.props.isTestComplete}
                  //                         style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? "not-allowed" : "pointer"}` }}
                  //                       >{option.scenario}</option>
                  //                     )
                  //                   }
                  //                   {/* <option>Select</option> */}
                  //                 </select>
                  //               </div>
                  //             </div>
                  //           </div>
                  //         </div>
                  //       </div>
                  //       <hr />
                  //     </div>
                  //   ))}
                  //   <div className="submit-button-sec">
                  //     <button
                  //       id={this.state.activeQuestion.type.toLowerCase()}
                  //       onClick={this.onClickSubmitAnswer}
                  //       disabled={this.state.isSubmitted || this.props.isTestComplete}
                  //       style={{ cursor: `${this.state.isSubmitted || this.props.isTestComplete ? "not-allowed" : "pointer"}` }}
                  //     >Submit Answer</button>
                  //   </div>
                  // </div> */}
                ) : null
              }
              <p>{this.state.errorMessage}</p>
            </div>
            <div className="col-lg-4 col-md-4">
              <div className="card remaining-time-card" style={{ textAlign: "center" }}>
                <div className="minutes-remianing pull-left">
                  <p className="m-0">Time Remaining</p>
                </div>
                <Countdown
                  date={this.props.isTestComplete ? 0 : this.state.timeSetting}
                  zeroPadDays={0}
                  zeroPadTime={0}
                  className="pull-right"
                  onComplete={() => { this.props.testCompleted() }}
                />
              </div>
              {/* <div className="remaining-time-chart">
                        <div className={`c100 p70 pink`}>
                          <div className="slice">
                            <div className="bar" />
                            <div className="fill" />
                          </div>
                        </div>
                      </div> */}
              <div className="card correct-answers-card">
                <ul className="list-unstyled">
                  <div
                    style={{
                      margin: "5px auto",
                      display: "block",
                      textAlign: "center"
                    }}
                  >
                    <h4>Answers</h4>
                  </div>
                  {this.state.processedAnswers}
                </ul>
              </div>
            </div>
          </div>
        </div>
        {
          this.props.isTestComplete
            ?
            <div>
              <Reference courseId={localStorage.getItem("courseId")} questionId={this.state.activeQuestion.id} />
            </div>
            :
            null
        }

        <div className="modal" id="addtoTestMemorizerModal">
          <div className="modal-dialog">
            <div className="modal-content myselect-memorizerModal">
              {/* Modal Header */}
              <div className="modal-header pb-0 border-bottom-0">
                <button
                  type="button"
                  className="close pt-1 pb-0"
                  data-dismiss="modal"
                  style={{ cursor: 'pointer' }}
                >×</button>
              </div>
              {/* Modal body */}
              <div className="modal-body">
                <div className="myselect-memorizerModal-body">
                  <div className="select-memorizer-head text-center">
                    <h2>Select Memorizer</h2>
                    <p>Please select memory card in which you want to add this question.</p>
                  </div>
                  <div className="select-memorizer-wrapper">
                    <ul className="list-unstyled">
                      {this.getMemorizers()}
                    </ul>
                  </div>
                  <div className="d-flex justify-content-between">
                    <div className="add-button">
                      <button
                        data-dismiss="modal"
                        onClick={this.onClickAddQuestionToMemorizers}>Add</button>
                    </div>
                    <div className="add-too-memorizer align-self-center">
                      {/* data-toggle="modal"
                    data-target="#addtoTestMemorizerModal" */}
                      <a href="#/" data-toggle="modal" data-target="#myNewMemoModal" >
                        <i className="fa fa-plus-circle" aria-hidden="true" />Add New Memorizer
                        </a>
                    </div>
                  </div>
                </div>
              </div>

              <div className="modal" id="myNewMemoModal">
                <div className="modal-dialog">
                  <div className="modal-content">
                  <form onSubmit={this.onCreateMemo}>
                    <div className="modal-body text-center new-memorizer">
                      <img src="/images/new-memorizer.png" alt="" />
                      <h2>New Memorizer</h2>
                      <input type="text" className="w-100 custom-input-box" placeholder="Enter Name" onChange={(e) => this.setState({ memoNameError: '', newMemoName: e.target.value })} />
                      <p>{this.state.memoNameError}</p>
                      <div className="submit-button-sec my-2">
                        <button type="submit" className="w-100 py-3 custom-theme-button-style text-white font-weight-bold">Create</button>
                      </div>
                    </div>
                    </form>

                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    );
  };

  isIterable(obj) {
    // checks for null and undefined
    if (obj == null) {
      return false;
    }
    return typeof obj[Symbol.iterator] === "function";
  };
}

export default TestHandler;