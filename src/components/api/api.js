import axios from 'axios';

let token = localStorage.getItem('token');

export const API = axios.create({
    baseURL: 'http://dev74.onlinetestingserver.com/medtutor/api',
    headers : {
        'Authorization': 'Bearer ' + token ? token : null
    }
})

if(token)
{
    // API.headers['Authorization'] = 'Bearer ' + token;
}

export const setToken = (token) => {
    // API.headers['Authorization'] = 'Bearer ' + token;
}

// export const instance, setToken;